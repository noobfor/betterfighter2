package de.tud.gdi1.bf2.ui;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.player.Player;
import de.tud.gdi1.bf2.model.player.PlayerOne;
import de.tud.gdi1.bf2.model.player.PlayerTwo;
import eea.engine.entity.StateBasedEntityManager;

public class BetterFighter2 extends StateBasedGame {
	
	// Jeder State wird durch einen Integer-Wert gekennzeichnet :D
    public static final int MAINMENU_STATE 				= 0;
    public static final int CHARACTER_SELECTION_STATE 	= 1;
    public static final int GAMEPLAY_STATE 				= 2;
    
    public static boolean debug = false;
    
    static PlayerOne p1 = new PlayerOne("player1");
	static PlayerTwo p2 = new PlayerTwo("player2");
	
	Player[] players = {p1,p2};
    
    public BetterFighter2(boolean debug)
    {
        super("BetterFighter2");
        setDebug(debug);
    }
    
    public static void setDebug(boolean debuging) {
    	debug = debuging;
    }
 
    public static void main(String[] args) throws SlickException
    {
    	// Setze den library Pfad abhaengig vom Betriebssystem
    	if(System.getProperty("os.name").toLowerCase().contains("windows")) {
    		System.setProperty("org.lwjgl.librarypath",System.getProperty("user.dir") + "/native/windows");
    	} 
    	else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
    		System.setProperty("org.lwjgl.librarypath",System.getProperty("user.dir") + "/native/macosx");
    	}
    	else {
    		System.setProperty("org.lwjgl.librarypath",System.getProperty("user.dir") + "/native/" +System.getProperty("os.name").toLowerCase());
    	}
    	
    	System.out.println("set opengl");
    	System.setProperty("org.lwjgl.opengl.Display.allowSoftwareOpenGL", "true");
    	System.err.println(System.getProperty("os.name") + ": " +System.getProperty("org.lwjgl.librarypath"));
    	
    	// Setze dieses StateBasedGame in einen App Container (oder Fenster)
        AppGameContainer app = new AppGameContainer(new BetterFighter2(false));
        
        // Lege die Einstellungen des Fensters fest und starte das Fenster
        app.setDisplayMode(800, 600, false);
        app.setTargetFrameRate(60);
        app.start();
    }

	@Override
	public void initStatesList(GameContainer gameContainer) throws SlickException {		
		// Fuege dem StateBasedGame die States hinzu 
		// (der zuerst hinzugefuegte State wird als erster State gestartet)
		
		addState(new MainMenuState(MAINMENU_STATE));
		addState(new CharacterSelectionState(CHARACTER_SELECTION_STATE,players));
        addState(new GameplayState(GAMEPLAY_STATE,players));
        
        // Fuege dem StateBasedEntityManager die States hinzu
        StateBasedEntityManager.getInstance().addState(MAINMENU_STATE);
        StateBasedEntityManager.getInstance().addState(CHARACTER_SELECTION_STATE);
        StateBasedEntityManager.getInstance().addState(GAMEPLAY_STATE);
		
	}
	
	public Player[] getPlayers() {
		return players;
	}
}
