package de.tud.gdi1.bf2.ui;

import java.util.Comparator;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.HpAndManaDisplay;
import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.entities.map.MapRing;
import de.tud.gdi1.bf2.model.entities.spells.Iceball;
import de.tud.gdi1.bf2.model.player.Player;
import de.tud.gdi1.bf2.utils.EntityPositionComparator;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;

public class GameplayState extends BasicGameState {

	private int stateID;
	private StateBasedEntityManager entityManager;

	int manaRegenerationPerSecond = 3;
	int healthRegenerationPer5Second = 1;

	long startTime;
	long elapsedTime = 0;

	Player[] players;

	boolean allAlive = true;
	private int secondsAfterGameOver = 1;

	public GameplayState(int id, Player[] players) {
		stateID = id;
		entityManager = StateBasedEntityManager.getInstance();
		this.players = players;

	}

	@Override
	public void init(GameContainer arg0, StateBasedGame arg1) throws SlickException {
		setBackground();
		load_charachters();
	}

	private void load_charachters() {

		Character[] charalist = new Character[players.length];
		for (int i = 0; i < charalist.length; i++) {
			entityManager.addEntity(stateID, players[i].getChar());
			charalist[i] = players[i].getChar();
		}

		load_healthbars(charalist);

	}

	private void load_healthbars(Character[] charalist) {
		try {
			HpAndManaDisplay hpAndManaDisplay = new HpAndManaDisplay("healthbar", charalist[0], charalist[1]);

			entityManager.addEntity(this.stateID, hpAndManaDisplay);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void setBackground() throws SlickException {
		MapRing currentMap = new MapRing("currentMap", stateID);
		StateBasedEntityManager.getInstance().addEntity(stateID, currentMap);

	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		List<Entity> oldList = entityManager.getEntitiesByState(this.stateID);
		Comparator<Entity> comp = new EntityPositionComparator();
		oldList.sort(comp);
		entityManager.renderEntities(container, game, g);
		g.setBackground(Color.white);
		for (Player player : players) {

			g.setColor(Color.white);
			if (!allAlive && !player.getChar().isDead()) {
				g.drawString("PLAYER " + player.getName() + " WINS!", 300, 400);
			}
		}
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {

		entityManager.updateEntities(container, game, delta);
		if (elapsedTime == 0) {
			startTime = System.currentTimeMillis() - 1; // -1 is needed because otherwise the elapsedtime doesn't
														// increase consistently
		}

		if (elapsedTime >= 1000) {
			startTime = System.currentTimeMillis() - 1;
			for (Player player : players) {
				// perhaps if player is burning, dont regen health, or if player is frozen dont
				// regen mana
				if (!player.getChar().isDead()) { // only regen if character is not dead
					// player.getChar().addHealth(healthRegenerationPer5Second); // health is no
					// longer regenerated
					player.getChar().addMana(manaRegenerationPerSecond);
				}
			}
		}

		for (Player player : players) {

			if (player.getChar().getHealth() == 0 && allAlive) {
				System.out.println("RIP " + player.getName() + "...");
				player.getChar().Kill();
				allAlive = false;
				startTime = System.currentTimeMillis() - 1;
				elapsedTime = 0;

			} else if ((!allAlive) && (elapsedTime >= (secondsAfterGameOver * 1000))) { // counter in milliseconds for
																						// before changing game state
				game.enterState(0);
			}

			if (player.getChar().isDead()) {
				entityManager.removeEntity(game.getCurrentStateID(), player.getChar());
			}

		}

		elapsedTime = System.currentTimeMillis() - startTime;
	}

	@Override
	public int getID() {
		return stateID;
	}

}
