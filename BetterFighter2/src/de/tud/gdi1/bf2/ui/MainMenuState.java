package de.tud.gdi1.bf2.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.factory.ButtonFactory;
import eea.engine.action.Action;
import eea.engine.action.basicactions.ChangeStateInitAction;
import eea.engine.action.basicactions.QuitAction;
import eea.engine.component.render.ImageRenderComponent;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.ANDEvent;
import eea.engine.event.basicevents.MouseClickedEvent;
import eea.engine.event.basicevents.MouseEnteredEvent;

public class MainMenuState extends BasicGameState {

	private int stateID = 0; // Identifier von diesem BasicGameState
	private StateBasedEntityManager entityManager; // zugehoeriger entityManager

	// private final int distance = 100;
	// private final int start_Position = 180;

	MainMenuState(int sid) {
		stateID = sid;
		entityManager = StateBasedEntityManager.getInstance(); // braucht man immer um �nderungen anzugeben ;)
	}

	/**
	 * Wird vor dem (erstmaligen) Starten dieses State's ausgefuehrt
	 */
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		// Hintergrund laden
		setBackground();
		createVsButton();
		createQuitButton();
	}

	private void setBackground() throws SlickException {
		Entity background = new Entity("background");
		background.setPosition(new Vector2f(400, 300));
		if (!BetterFighter2.debug) {
			background.addComponent(new ImageRenderComponent(new Image("/assets/MainMenu.png")));
		}
		StateBasedEntityManager.getInstance().addEntity(this.stateID, background);
	}

	private void createVsButton() throws SlickException {
		Vector2f position = new Vector2f(135, 250);
		String path = "assets/vs.png";
		ANDEvent clickOnVsEvent = new ANDEvent(new MouseEnteredEvent(), new MouseClickedEvent());
		Action vsMode_Action = new ChangeStateInitAction(BetterFighter2.CHARACTER_SELECTION_STATE);

		ButtonFactory bf = new ButtonFactory("Vs. Mode", position, path, clickOnVsEvent, vsMode_Action);
		entityManager.addEntity(this.stateID, bf.createEntity());
	}

	private void createQuitButton() throws SlickException {
		Vector2f position = new Vector2f(135, 450);
		String path = "assets/close_button.png";
		ANDEvent clickOnQuitEvent = new ANDEvent(new MouseEnteredEvent(), new MouseClickedEvent());
		Action quit_Action = new QuitAction();

		ButtonFactory bf = new ButtonFactory("Quit", position, path, clickOnQuitEvent, quit_Action);
		entityManager.addEntity(this.stateID, bf.createEntity());
	}

	/**
	 * Wird vor dem Frame ausgefuehrt
	 */
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		entityManager.updateEntities(container, game, delta);
	}

	/**
	 * Wird mit dem Frame ausgefuehrt
	 */
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		entityManager.renderEntities(container, game, g);
		g.setBackground(Color.black);

	}

	@Override
	public int getID() {
		return stateID;
	}

}
