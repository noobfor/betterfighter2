package de.tud.gdi1.bf2.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.RoundedRectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.action.SelectCharacterAction;
import de.tud.gdi1.bf2.model.factory.ButtonFactory;
import de.tud.gdi1.bf2.model.player.Player;
import de.tud.gdi1.bf2.model.render.RenderShapeComponent;
import eea.engine.action.Action;
import eea.engine.action.basicactions.ChangeStateAction;
import eea.engine.action.basicactions.ChangeStateInitAction;
import eea.engine.component.Component;
import eea.engine.component.render.ImageRenderComponent;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.ANDEvent;
import eea.engine.event.OREvent;
import eea.engine.event.basicevents.KeyPressedEvent;
import eea.engine.event.basicevents.MouseClickedEvent;
import eea.engine.event.basicevents.MouseEnteredEvent;

public class CharacterSelectionState extends BasicGameState {

	private int stateID;
	private StateBasedEntityManager entityManager;
	Player[] players;

	GameContainer gc;
	private int numberOfCharacters = 4;
	private Vector2f[] entitiesPositions = new Vector2f[numberOfCharacters];

	private String path = "assets/characters/";

	private int numberOfImplementedCharacters = 2;

	CharacterSelectionState(int stateId, Player[] players) {
		this.stateID = stateId;
		entityManager = StateBasedEntityManager.getInstance();
		this.players = players;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		int screenWidth = container.getWidth();
		float sectionSize = screenWidth / numberOfCharacters;
		float squareSize = 160;
		for (int i = 0; i < numberOfCharacters; i++) {
			this.entitiesPositions[i] = new Vector2f(i * sectionSize + (sectionSize - squareSize) / 2 + 80, 160f);
		}
		setBackground();
		Entity[] squares = createCharacterDisplay();
		createStartButton(squares);
		addEscListener();
	}

	private Entity[] createCharacterDisplay() throws SlickException {
		createCharacterEntity("Bugi", 1);
		createCharacterEntity("Noobfor", 2);

		Entity square1 = drawSquare(entitiesPositions[0], 160, Color.red, "square1");
		Entity square2 = drawSquare(entitiesPositions[1], 160, Color.green, "square2");
		Entity[] squares = { square1, square2 };

		for (int i = 0; i < squares.length; i++) {
			setSelectionControls(players[i].getKeyConfig(), squares[i], players[i]);
		}

		return squares;
	}

	private void setSelectionControls(int[] keyConfig, Entity controlledSquare, Player p) throws SlickException {

		KeyPressedEvent keyRight = new KeyPressedEvent(keyConfig[0]);
		KeyPressedEvent keyLeft = new KeyPressedEvent(keyConfig[1]);

		keyRight.addAction(new Action() {

			@Override
			public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
				Vector2f currentPosition = controlledSquare.getPosition();
				int oldIndex = 0;
				if (currentPosition.getX() < entitiesPositions[numberOfImplementedCharacters - 1].getX()) {

					for (int i = 0; i < numberOfCharacters; i++) {
						if (currentPosition.getX() == entitiesPositions[i].getX()) {
							oldIndex = i;
						}
					}
					controlledSquare.setPosition(entitiesPositions[oldIndex + 1]);
				}
			}
		});
		controlledSquare.addComponent(keyRight);

		keyLeft.addAction(new Action() {

			@Override
			public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
				Vector2f currentPosition = controlledSquare.getPosition();
				int oldIndex = 0;
				if (currentPosition.getX() > entitiesPositions[0].getX()) {

					for (int i = 0; i < numberOfImplementedCharacters; i++) {
						if (currentPosition.getX() == entitiesPositions[i].getX()) {
							oldIndex = i;
						}
					}
					controlledSquare.setPosition(entitiesPositions[oldIndex - 1]);
				}
			}
		});
		controlledSquare.addComponent(keyLeft);

	}

	private Entity drawSquare(Vector2f pos, float width, Color color, String id) {
		Entity square = new Entity(id);
		square.setPosition(pos);
		square.addComponent(new RenderShapeComponent(new RoundedRectangle(0, 0, width, width, 20f), color, 4));
		entityManager.addEntity(stateID, square);
		return square;
	}

	private void addEscListener() {
		Entity esc_Listener = new Entity("ESC_Listener");
		KeyPressedEvent esc_pressed = new KeyPressedEvent(Input.KEY_ESCAPE);
		esc_pressed.addAction(new ChangeStateAction(BetterFighter2.MAINMENU_STATE));
		esc_Listener.addComponent(esc_pressed);
		entityManager.addEntity(stateID, esc_Listener);

	}

	private void createStartButton(Entity[] squares) throws SlickException {

		Vector2f position = new Vector2f(390, 450);
		String path = "assets/button_start.png";
		ANDEvent clickedStartButton = new ANDEvent(new MouseEnteredEvent(), new MouseClickedEvent());
		OREvent startGame = new OREvent(clickedStartButton, new KeyPressedEvent(Input.KEY_SPACE));
		for (int i = 0; i < squares.length; i++) {
			startGame.addAction(new SelectCharacterAction(players[i], squares[i], entitiesPositions));
		}

		Action startAction = new ChangeStateInitAction(BetterFighter2.GAMEPLAY_STATE);
		ButtonFactory bf = new ButtonFactory("VSStartButton", position, path, startGame, startAction);

		entityManager.addEntity(stateID, bf.createEntity());
	}

	private void createCharacterEntity(String player_name, int player_id) throws SlickException {

		Entity playerEntity = new Entity(player_name);
		playerEntity.setScale(2f);

		switch (player_id) {
		case 1:
			playerEntity.setPosition(entitiesPositions[0]);
			if (!BetterFighter2.debug) {
				playerEntity.addComponent(new ImageRenderComponent(new Image(path + "Budsy/idle/0.png")));
			}
			break;

		case 2:
			playerEntity.setPosition(entitiesPositions[1]);
			if (!BetterFighter2.debug) {
				playerEntity.addComponent(new ImageRenderComponent(new Image(path + "Noobfor/idle/0.png")));
			}
			break;

		default:
			break;
		}
		entityManager.addEntity(this.stateID, playerEntity);

	}

	private void setBackground() throws SlickException {
		Entity background = new Entity("character_selection_background");
		background.setPosition(new Vector2f(400, 300));
		//background.addComponent(new ImageRenderComponent(new
		//		Image("/assets/CharSelect.png")));
		entityManager.addEntity(this.stateID, background);
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		entityManager.renderEntities(container, game, g);
		g.setBackground(Color.gray);
		g.setColor(Color.white);
		g.drawString("SELECT YOUR CHARACTER", 295, 300);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		entityManager.updateEntities(container, game, delta);
	}

	@Override
	public int getID() {
		return stateID;
	}

}
