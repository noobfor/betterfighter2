package de.tud.gdi1.bf2.tests.tutors.suites;

import de.tud.gdi1.bf2.tests.tutors.testcases.AdvancedSpecialMovesTests;
import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.framework.TestSuite;

public class BetterFighter2TestsuiteExtended2 {
	
	public static Test suite() {
		TestSuite suite = new TestSuite("Tutor tests for BetterFighter2 - Extended 2");
		suite.addTest(new JUnit4TestAdapter(AdvancedSpecialMovesTests.class));
		return suite;
	}
	
}
