package de.tud.gdi1.bf2.tests.tutors.testcases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.newdawn.slick.SlickException;

import de.tud.gdi1.bf2.tests.adapter.BetterFighter2TestAdapterExtended1;
import de.tud.gdi1.bf2.ui.BetterFighter2;

public class ProjectileTest {

	BetterFighter2TestAdapterExtended1 adapter;
	int[] player1KeyConfig;
	int[] player2KeyConfig;

	@Before
	public void setUp() throws Exception {
		adapter = new BetterFighter2TestAdapterExtended1();
		adapter.initializeGame();
		player1KeyConfig = adapter.getPlayers().get(0).getKeyConfig();
		player2KeyConfig = adapter.getPlayers().get(1).getKeyConfig();

		adapter.handleMousePosition(0, adapter.getStartButtonXPosition(), adapter.getStartButtonYPosition());
		adapter.handleMouseClicked(0);
		
		// go to character selection state
		assertEquals(BetterFighter2.CHARACTER_SELECTION_STATE, adapter.getStateBasedGame().getCurrentStateID());
		adapter.playerSelectCharacter(0, 0);
        adapter.playerSelectCharacter(1, 1);
        adapter.handleSpaceKeyPressed(0);
        adapter.getOurTestInput().clearPressedKeys();

        // player 1 picks FireCharacter
        assertTrue(adapter.isPlayer1FireCharacter());
        // player 2 picks IceCharacter
        assertTrue(adapter.isPlayer2IceCharacter());
		// go to gameplay state
		
		assertEquals(BetterFighter2.GAMEPLAY_STATE, adapter.getStateBasedGame().getCurrentStateID());

		adapter.getOurTestInput().clearDownKeys();
	}

	@After
	public void finish() {
		adapter.stopGame();
	}
	
	@Test
	public void fireballSpeedTests() throws SlickException {
		int shieldOne = player1KeyConfig[7];
		int rightOne = player1KeyConfig[0];
		int attackOne = player1KeyConfig[5];

		adapter.setPositionofCharacter(0, 200, 300);
		adapter.setPositionofCharacter(1, 350, 500);
		
		adapter.handleKeyPressedPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, rightOne);
		adapter.handleKeyDownPlayer1(0, attackOne);
		
		//speed test
		float startX = adapter.getFireballXPosition(0, 0);
		adapter.updateGame(20);
		float endX = adapter.getFireballXPosition(0, 0);
		
		assertEquals(startX+8, endX, 1);
		
	}
	
	
	@Test
	public void iceballSpeedTests() throws SlickException {
		int shieldTwo = player2KeyConfig[7];
		int rightTwo = player2KeyConfig[0];
		int attackTwo = player2KeyConfig[5];

		adapter.setPositionofCharacter(1, 200, 300);
		adapter.setPositionofCharacter(0, 350, 500);
		
		adapter.handleKeyPressedPlayer1(0, shieldTwo);
		adapter.handleKeyDownPlayer1(0, shieldTwo);
		adapter.handleKeyDownPlayer1(0, rightTwo);
		adapter.handleKeyDownPlayer1(0, attackTwo);
		
		float startX = adapter.getIceballXPosition(1, 0);
		adapter.updateGame(50);
		float endX = adapter.getIceballXPosition(1, 0);
		
		assertEquals(startX+5, endX, 1);
	}
	
	
	
}
