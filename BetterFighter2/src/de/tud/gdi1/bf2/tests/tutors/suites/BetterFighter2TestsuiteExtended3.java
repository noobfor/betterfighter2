package de.tud.gdi1.bf2.tests.tutors.suites;

import de.tud.gdi1.bf2.tests.tutors.testcases.VeryAdvancedSpecialMoves;
import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.framework.TestSuite;

public class BetterFighter2TestsuiteExtended3 {
	
	public static Test suite() {
		TestSuite suite = new TestSuite("Tutor tests for BetterFighter2 - Extended 3");
		suite.addTest(new JUnit4TestAdapter(VeryAdvancedSpecialMoves.class));
		return suite;
	}
	
}
