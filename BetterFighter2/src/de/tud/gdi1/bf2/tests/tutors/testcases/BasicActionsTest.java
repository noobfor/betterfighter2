package de.tud.gdi1.bf2.tests.tutors.testcases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.tests.adapter.BetterFighter2TestAdapterMinimal;
import de.tud.gdi1.bf2.ui.BetterFighter2;

public class BasicActionsTest {

	BetterFighter2TestAdapterMinimal adapter;
	int[] player1KeyConfig;
	int[] player2KeyConfig;
	
	float jumpHeight = 80;
	float jumpTime = 1000;
	float castingTime = 200;
	float jumpLength = 160;

	@Before
	public void setUp() throws Exception {
		adapter = new BetterFighter2TestAdapterMinimal();
		adapter.initializeGame();
		player1KeyConfig = adapter.getPlayers().get(0).getKeyConfig();
		player2KeyConfig = adapter.getPlayers().get(1).getKeyConfig();

		adapter.handleMousePosition(0, adapter.getStartButtonXPosition(), adapter.getStartButtonYPosition());
		adapter.handleMouseClicked(0);
		// go to character selection state
		assertEquals(BetterFighter2.CHARACTER_SELECTION_STATE, adapter.getStateBasedGame().getCurrentStateID());
		adapter.handleSpaceKeyPressed(0);
		// go to gameplay state
		assertEquals(BetterFighter2.GAMEPLAY_STATE, adapter.getStateBasedGame().getCurrentStateID());

		adapter.getOurTestInput().clearDownKeys();
	}

	@After
	public void finish() {
		adapter.stopGame();
	}

	// test diagonal jumps

	@Test
	public void testJumpRightUp() throws SlickException {
		int rightOne = player1KeyConfig[0];
		int upOne = player1KeyConfig[2];
		int jumpOne = player1KeyConfig[6];

		float offset = 50;

		Vector2f characterPosition = adapter.getCharacter(0).getPosition();

		adapter.getCharacter(0).setPosition(new Vector2f(characterPosition.getX(), characterPosition.getY() + 50));
		characterPosition = adapter.getCharacter(0).getPosition();
		Vector2f startingPosition = characterPosition.copy();

		adapter.handleKeyDownPlayer1(0, jumpOne, upOne, rightOne);

		adapter.getOurTestInput().clearDownKeys();

		// jump positions tested for 20 frames
		for (int i = 0; i < 20; i++) {
			int delta = (i + 1) * 50;
			adapter.updateGame(50);
			characterPosition = adapter.getCharacter(0).getPosition();
			if (delta <= castingTime) { // during casting time, the position should not be modified
				assertEquals(startingPosition.y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);

			} else if (delta > castingTime && delta <= (jumpTime + castingTime)) {
				float x = delta - castingTime - 50 / 2;
				float y = adapter.jumpFunction(x, offset, jumpHeight, jumpTime);
				assertEquals(startingPosition.y - y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x + x * (jumpLength / jumpTime), characterPosition.x, 0.1);
			} else {
				assertEquals(startingPosition.y - offset, characterPosition.y, 0.1);
				assertEquals(startingPosition.x + jumpLength, characterPosition.x, 0.1);
			}
		}

	}

	@Test
	public void testJumpLeftUp() throws SlickException {
		int leftOne = player1KeyConfig[1];
		int upOne = player1KeyConfig[2];
		int jumpOne = player1KeyConfig[6];

		float offset = 50;

		Vector2f characterPosition = adapter.getCharacter(0).getPosition();

		adapter.getCharacter(0).setPosition(new Vector2f(characterPosition.getX(), characterPosition.getY() + 50));
		characterPosition = adapter.getCharacter(0).getPosition();
		Vector2f startingPosition = characterPosition.copy();

		adapter.handleKeyDownPlayer1(0, jumpOne, upOne, leftOne);

		adapter.getOurTestInput().clearDownKeys();

		// jump positions tested for 20 frames
		for (int i = 0; i < 20; i++) {
			int delta = (i + 1) * 50;
			adapter.updateGame(50);
			characterPosition = adapter.getCharacter(0).getPosition();
			if (delta <= castingTime) { // during casting time, the position should not be modified
				assertEquals(startingPosition.y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);

			} else if (delta > castingTime && delta <= (jumpTime + castingTime)) {
				float x = delta - castingTime - 50 / 2;
				float y = adapter.jumpFunction(x, offset, jumpHeight, jumpTime);
				
				assertEquals(startingPosition.y - y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x - x * (jumpLength / jumpTime), characterPosition.x, 0.1);
			} else {
				assertEquals(startingPosition.y - offset, characterPosition.y, 0.1);
				assertEquals(startingPosition.x - jumpLength, characterPosition.x, 0.1);
			}
		}
	}

	@Test
	public void testJumpRightDown() throws SlickException {
		int rightOne = player1KeyConfig[0];
		int downOne = player1KeyConfig[3];
		int jumpOne = player1KeyConfig[6];

		float offset = -50;

		Vector2f characterPosition = adapter.getCharacter(0).getPosition();

		adapter.getCharacter(0).setPosition(new Vector2f(characterPosition.getX(), characterPosition.getY() + offset));
		characterPosition = adapter.getCharacter(0).getPosition();
		Vector2f startingPosition = characterPosition.copy();

		adapter.handleKeyDownPlayer1(0, jumpOne, downOne, rightOne);

		adapter.getOurTestInput().clearDownKeys();

		// jump positions tested for 20 frames
		for (int i = 0; i < 20; i++) {
			int delta = (i + 1) * 50;
			adapter.updateGame(50);
			characterPosition = adapter.getCharacter(0).getPosition();
			if (delta <= castingTime) { // during casting time, the position should not be modified
				assertEquals(startingPosition.y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);

			} else if (delta > castingTime && delta <= (jumpTime + castingTime)) {
				float x = delta - castingTime - 50 / 2;
				float y = adapter.jumpFunction(x, offset, jumpHeight, jumpTime);
				assertEquals(startingPosition.y - y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x + x * (jumpLength / jumpTime), characterPosition.x, 0.1);
			} else {
				assertEquals(startingPosition.y - offset, characterPosition.y, 0.1);
				assertEquals(startingPosition.x + jumpLength, characterPosition.x, 0.1);
			}
		}
	}

	@Test
	public void testJumpLefttDown() throws SlickException {
		int leftOne = player1KeyConfig[1];
		int downOne = player1KeyConfig[3];
		int jumpOne = player1KeyConfig[6];

		float offset = -50;

		Vector2f characterPosition = adapter.getCharacter(0).getPosition();

		adapter.getCharacter(0).setPosition(new Vector2f(characterPosition.getX(), characterPosition.getY() + offset));
		characterPosition = adapter.getCharacter(0).getPosition();
		Vector2f startingPosition = characterPosition.copy();

		adapter.handleKeyDownPlayer1(0, jumpOne, downOne, leftOne);
		adapter.getOurTestInput().clearDownKeys();

		// jump positions tested for 20 frames
		for (int i = 0; i < 20; i++) {
			int delta = (i + 1) * 50;
			adapter.updateGame(50);
			characterPosition = adapter.getCharacter(0).getPosition();
			if (delta <= castingTime) { // during casting time, the position should not be modified
				assertEquals(startingPosition.y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);

			} else if (delta > castingTime && delta <= (jumpTime + castingTime)) {
				float x = delta - castingTime - 50 / 2;
				float y = adapter.jumpFunction(x, offset, jumpHeight, jumpTime);
				assertEquals(startingPosition.y - y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x - x * (jumpLength / jumpTime), characterPosition.x, 0.1);
			} else {
				assertEquals(startingPosition.y - offset, characterPosition.y, 0.1);
				assertEquals(startingPosition.x - jumpLength, characterPosition.x, 0.1);
			}
		}
	}
	
	
	@Test
	public void testPunch() throws SlickException {
		int attackKeyPlayer1 = player1KeyConfig[5];
//		int attackKeyPlayer2 = player2KeyConfig[5];
		
		// Test if punch is active for about 30 Frames
		adapter.handleKeyPressedPlayer1(1, attackKeyPlayer1);
		assertEquals(adapter.isPunching(0), true);
		adapter.updateFrames(14);
		assertEquals(adapter.isPunching(0), true);
		adapter.updateFrames(2);
		assertEquals(adapter.isPunching(0), false);
		
		// Test if punch hitbox is active for about 25 Frames 
		// and movement is disabled until 30 Frames
		adapter.setPositionofCharacter(0, 350, 350);
		adapter.setDirectionofCharacter(0, true);
		adapter.handleKeyPressedPlayer1(1, attackKeyPlayer1);
		
		adapter.updateFrames(13);
		
		adapter.setPositionofCharacter(1, 350, 350);
		adapter.handleKeyPressedPlayer2(1, player2KeyConfig[2]);
		assertTrue(adapter.getCharacterActualLife(1) == 100);
		
		// should not be able to move yet
		float xPositionBefore = adapter.getCharacterXPosition(0);
		adapter.handleKeyDownPlayer2(10, player1KeyConfig[1]);
		float xPositionAfter = adapter.getCharacterXPosition(0);
		assertEquals(xPositionBefore, xPositionAfter, 0);
		adapter.getOurTestInput().clearDownKeys();
		
		adapter.updateFrames(50);
		
		// new Position should be x in the punching direction (in this example to the right)
		xPositionAfter = adapter.getCharacterXPosition(0);
		assertEquals(355, xPositionAfter, 0);
		
		// Test if punch does exactly 10 damage
		adapter.setPositionofCharacter(0, 350, 350);
		adapter.setPositionofCharacter(1, 350, 350);
		adapter.handleKeyPressedPlayer1(1, attackKeyPlayer1);
		assertTrue(adapter.getCharacterActualLife(1) == 90);
		
	}
	
	@Test
	public void testShield() throws SlickException {
		int defendKeyPlayer1 = player1KeyConfig[7];
//		int defendKeyPlayer2 = player2KeyConfig[7];
//		int attackKeyPlayer1 = player1KeyConfig[5];
		
		// Test if shield is active for 50 frames
		adapter.handleKeyPressedPlayer1(1, defendKeyPlayer1);
		assertEquals(true, adapter.isShielding(0));
		
		adapter.updateFrames(24);
		assertTrue(adapter.isShielding(0));
		adapter.updateFrames(1);
		assertFalse(adapter.isShielding(0));

		// test if movement is disabled while shielding
		adapter.handleKeyPressedPlayer1(1, defendKeyPlayer1);
		float xPositionBefore = adapter.getCharacterXPosition(0);
		adapter.handleKeyDownPlayer1(10, player1KeyConfig[1]);
		float xPositionAfter = adapter.getCharacterXPosition(0);
		
		assertEquals(xPositionBefore, xPositionAfter, 0);
		
		adapter.getOurTestInput().clearDownKeys();
	}
	
	
}
