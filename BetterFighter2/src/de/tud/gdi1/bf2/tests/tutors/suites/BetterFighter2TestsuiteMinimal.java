package de.tud.gdi1.bf2.tests.tutors.suites;

import de.tud.gdi1.bf2.tests.tutors.testcases.BasicActionsTest;
import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.framework.TestSuite;

public class BetterFighter2TestsuiteMinimal {

	public static Test suite() {

		TestSuite suite = new TestSuite("Tutor tests for BetterFighter2 - Minimal");
		suite.addTest(new JUnit4TestAdapter(BasicActionsTest.class));
		return suite;
	}
}
