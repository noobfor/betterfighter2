package de.tud.gdi1.bf2.tests.tutors.testcases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.entities.characters.BudsyCharacter;
import de.tud.gdi1.bf2.model.entities.characters.NoobforCharacter;
import de.tud.gdi1.bf2.tests.adapter.BetterFighter2TestAdapterExtended2;
import de.tud.gdi1.bf2.ui.BetterFighter2;

public class AdvancedSpecialMovesTests {

	BetterFighter2TestAdapterExtended2 adapter;
	int[] player1KeyConfig;
	int[] player2KeyConfig;

	@Before
	public void setUp() throws Exception {
		adapter = new BetterFighter2TestAdapterExtended2();
		adapter.initializeGame();

		player1KeyConfig = adapter.getPlayers().get(0).getKeyConfig();
		player2KeyConfig = adapter.getPlayers().get(1).getKeyConfig();

		adapter.handleMousePosition(0, adapter.getStartButtonXPosition(), adapter.getStartButtonYPosition());
		adapter.handleMouseClicked(0);

		// go to character selection state
		assertEquals(BetterFighter2.CHARACTER_SELECTION_STATE, adapter.getStateBasedGame().getCurrentStateID());
		adapter.playerSelectCharacter(0, 0);
        adapter.playerSelectCharacter(1, 1);
        adapter.handleSpaceKeyPressed(0);
        adapter.getOurTestInput().clearPressedKeys();

        // player 1 picks FireCharacter
        assertTrue(adapter.isPlayer1FireCharacter());
        // player 2 picks IceCharacter
        assertTrue(adapter.isPlayer2IceCharacter());

		// go to gameplay state
		assertEquals(BetterFighter2.GAMEPLAY_STATE, adapter.getStateBasedGame().getCurrentStateID());

		adapter.getOurTestInput().clearDownKeys();
	}

	@After
	public void finish() {
		adapter.stopGame();
	}

	@Test
	public void testIcewall() throws SlickException {

		int shieldTwo = player2KeyConfig[7];
		int leftTwo = player2KeyConfig[1];
		int jumpTwo = player2KeyConfig[6];
		int attackTwo = player2KeyConfig[5];

		Vector2f rightToTheOther = adapter.getCharacterPosition(0).copy().add(new Vector2f(100, 0));

		adapter.setPositionofCharacter(1, rightToTheOther.x, rightToTheOther.y);

		adapter.setDirectionofCharacter(1, false);
		adapter.handleKeyPressedPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, leftTwo);
		adapter.handleKeyDownPlayer2(0, jumpTwo);
		adapter.getOurTestInput().clearDownKeys();

		for (int i = 0; i < 10; i++) {
			adapter.updateFrames(1);
			int iceWallCount = adapter.getIceWalls().size();
			assertTrue(iceWallCount == 1 && i < 4 || iceWallCount == 2 && i >= 4 && i < 9 || iceWallCount == 3);
		}

		List<Vector2f> iceWallPositions = adapter.getIceWallPositions();

		// all icewalls have a distance of 55 between each other
		assertTrue(iceWallPositions.get(0).x == iceWallPositions.get(1).x + 55
				&& iceWallPositions.get(1).x + 55 == iceWallPositions.get(2).x + 110);
		// wait for stopping shielding
		adapter.updateFrames(50);
		// attack the icewall
		adapter.handleKeyPressedPlayer2(0, attackTwo);
		adapter.updateFrames(1);
		// there should be only 2 icewalls left
		assertTrue(adapter.getIceWalls().size() == 2);

		// player 1 should be freezed and taken 20 hits of damage
		assertTrue(
				adapter.isPlayer1Freezed() && adapter.getCharacterActualLife(0) == adapter.getCharacterMaxLife(0) - 20);
		adapter.updateFrames(1);
		// after 1000 frames, the walls should be gone
		adapter.updateFrames(500);
		assertTrue(adapter.getIceWalls().size() == 0);
	}

	@Test
	public void testFirebreath() throws SlickException {
		int rightOne = player1KeyConfig[0];
		int leftOne = player1KeyConfig[1];
		int upOne = player1KeyConfig[2];
		int downOne = player1KeyConfig[3];
		int jumpOne = player1KeyConfig[6];
		int shieldOne = player1KeyConfig[7];

		Vector2f player2Position = adapter.getCharacterPosition(1);
		adapter.setPositionofCharacter(0, player2Position.x - 60, player2Position.y);

		adapter.handleKeyPressedPlayer2(0, shieldOne);
		adapter.handleKeyDownPlayer2(0, shieldOne);
		adapter.handleKeyDownPlayer2(0, downOne);
		adapter.handleKeyDownPlayer2(0, jumpOne);
		adapter.getOurTestInput().clearDownKeys();
		// test if character was knocked off
		assertTrue(adapter.isFireballKnockback(1));
		// test dmg
		assertEquals(adapter.getCharacterMaxLife(1) - 10, adapter.getCharacterActualLife(1));

		adapter.updateFrames(50);
		Vector2f lastPosition = adapter.getCharacterPosition(0);
		// cancellable with shield button
		adapter.handleKeyDownPlayer1(0, shieldOne);
		// movement should be again allowed
		adapter.handleKeyDownPlayer1(100, rightOne);
		adapter.getOurTestInput().clearDownKeys();
		Vector2f currentPosition = adapter.getCharacterPosition(0);
		assertEquals(lastPosition.x + 10, currentPosition.x, 0);
		adapter.handleKeyDownPlayer1(100, leftOne);
		currentPosition = adapter.getCharacterPosition(0);
		assertEquals(lastPosition.x, currentPosition.x, 0);
		adapter.handleKeyDownPlayer1(100, upOne);
		currentPosition = adapter.getCharacterPosition(0);
		assertEquals(lastPosition.y - 10, currentPosition.y, 0);
		adapter.handleKeyDownPlayer1(100, downOne);
		currentPosition = adapter.getCharacterPosition(0);
		assertEquals(lastPosition.y, currentPosition.y, 0);

		// Player 2 should be on the floor
		assertTrue(adapter.isOnTheGroundKnockback(1));
	}

	@Test
	public void testSprintAttack() {
		
		int leftTwo = player2KeyConfig[1];
		int runTwo = player2KeyConfig[4];
		int attackTwo = player2KeyConfig[5];

		Vector2f enemyPosition = adapter.getCharacterPosition(0);
		// set to the right of the enemy
		adapter.setPositionofCharacter(1, enemyPosition.x + 30, enemyPosition.y);
		
		adapter.handleKeyDownPlayer2(0, leftTwo, runTwo, attackTwo);
		adapter.getOurTestInput().clearDownKeys();

		// test if the dash moves the character 30 units towards the enemy
		assertEquals(enemyPosition.x, adapter.getCharacterPosition(1).x,0);
		// test if damage taken
		assertEquals(adapter.getCharacterMaxLife(0) - 15, adapter.getCharacterActualLife(0), 0);
		// and test strong knockback
		assertTrue(adapter.isFireballKnockback(0));
	}
}
