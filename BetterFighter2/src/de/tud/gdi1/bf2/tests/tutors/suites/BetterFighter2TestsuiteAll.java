package de.tud.gdi1.bf2.tests.tutors.suites;

import junit.framework.Test;
import junit.framework.TestSuite;

public class BetterFighter2TestsuiteAll {
	
	public static Test suite() {
		
		TestSuite suite = new TestSuite("All tutor tests for BetterFighter2");
		
		suite.addTest(BetterFighter2TestsuiteMinimal.suite());
		suite.addTest(BetterFighter2TestsuiteExtended1.suite());
		suite.addTest(BetterFighter2TestsuiteExtended2.suite());
		suite.addTest(BetterFighter2TestsuiteExtended3.suite());	
		return suite;
	}
}
