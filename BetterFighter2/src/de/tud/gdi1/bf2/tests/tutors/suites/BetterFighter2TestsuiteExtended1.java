package de.tud.gdi1.bf2.tests.tutors.suites;

import de.tud.gdi1.bf2.tests.tutors.testcases.ProjectileTest;
import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.framework.TestSuite;

public class BetterFighter2TestsuiteExtended1 {
	
	public static Test suite() {
		TestSuite suite = new TestSuite("Tutor tests for BetterFighter2 - Extended 1");
		suite.addTest(new JUnit4TestAdapter(ProjectileTest.class));
		return suite;
	}
	
}
