package de.tud.gdi1.bf2.tests.tutors.testcases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.tests.adapter.BetterFighter2TestAdapterExtended3;
import de.tud.gdi1.bf2.ui.BetterFighter2;

public class VeryAdvancedSpecialMoves {

	BetterFighter2TestAdapterExtended3 adapter;
	int[] player1KeyConfig;
	int[] player2KeyConfig;
	
	
	@Before
	public void setUp() throws Exception {
		adapter = new BetterFighter2TestAdapterExtended3();
		adapter.initializeGame();
		
		player1KeyConfig = adapter.getPlayers().get(0).getKeyConfig();
		player2KeyConfig = adapter.getPlayers().get(1).getKeyConfig();

		adapter.handleMousePosition(0, adapter.getStartButtonXPosition(), adapter.getStartButtonYPosition());
		adapter.handleMouseClicked(0);
		
		// go to character selection state
		assertEquals(BetterFighter2.CHARACTER_SELECTION_STATE, adapter.getStateBasedGame().getCurrentStateID());
		adapter.playerSelectCharacter(0, 0);
        adapter.playerSelectCharacter(1, 1);
        adapter.handleSpaceKeyPressed(0);
        adapter.getOurTestInput().clearPressedKeys();

        // player 1 picks FireCharacter
        assertTrue(adapter.isPlayer1FireCharacter());
        // player 2 picks IceCharacter
        assertTrue(adapter.isPlayer2IceCharacter());
		// go to gameplay state
		assertEquals(BetterFighter2.GAMEPLAY_STATE, adapter.getStateBasedGame().getCurrentStateID());

		adapter.getOurTestInput().clearDownKeys();
	}
	
	@After
	public void finish() {
		adapter.stopGame();
	}
	
	private float quadraticBezierCurve(float start, float mid, float end, float t) {
		return ((1-t) * ((1-t) * start + t * mid) + t * ((1-t) * mid + t * end));
	}
	
	private float knockbackFunction(float t, float startY, int directionOffset) {
		return (float) - ( 3 - Math.pow( t - directionOffset * 2/2, 2) + 0);
	}
	
	
	@Test
	public void iceRainCurveTest() throws SlickException {

		int shieldTwo = player2KeyConfig[7];
		int upTwo = player2KeyConfig[2];
		int attackTwo = player2KeyConfig[5];

		Vector2f point0 = new Vector2f();
		Vector2f point1 = new Vector2f();
		Vector2f point2 = new Vector2f();
		
		adapter.setPositionofCharacter(1, 300, 400);
		adapter.setPositionofCharacter(0, 400, 400);
		
		float characterXCoordinate = adapter.getCharacterXPosition(1);
		float characterYCoordinate = adapter.getCharacterYPosition(1);
		
		point0.x = characterXCoordinate;
		point0.y = characterYCoordinate - 60;
		
		point1.x = characterXCoordinate + 50;
		point1.y = characterYCoordinate - 310;
		
		point2.x = characterXCoordinate + 70;
		point2.y = characterYCoordinate - 240;
		
		//cast to the right
		adapter.setDirectionofCharacter(1, true);
		
		adapter.handleKeyPressedPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, upTwo);
		adapter.handleKeyDownPlayer2(0, attackTwo);
		
		adapter.getOurTestInput().clearDownKeys();
		adapter.getOurTestInput().clearPressedKeys();
		
		float t = 0.1f;
		adapter.updateFrames(5);
		float newXshouldBe = quadraticBezierCurve(point0.x, point1.x, point2.x, t);
		float newYshouldBe = quadraticBezierCurve(point0.y, point1.y, point2.y, t);
		assertEquals(newXshouldBe, adapter.getIcedropXPosition(0), 2);
		assertEquals(newYshouldBe, adapter.getIcedropYPosition(0), 2);
		
		
		t = 0.30f;
		adapter.updateFrames(10);
		newXshouldBe = quadraticBezierCurve(point0.x, point1.x, point2.x, t);
		newYshouldBe = quadraticBezierCurve(point0.y, point1.y, point2.y, t);
		assertEquals(newXshouldBe, adapter.getIcedropXPosition(0), 2);
		assertEquals(newYshouldBe, adapter.getIcedropYPosition(0), 2);
		
		
		t = 0.50f;
		adapter.updateFrames(10);
		newXshouldBe = quadraticBezierCurve(point0.x, point1.x, point2.x, t);
		newYshouldBe = quadraticBezierCurve(point0.y, point1.y, point2.y, t);
		assertEquals(newXshouldBe, adapter.getIcedropXPosition(0), 2);
		assertEquals(newYshouldBe, adapter.getIcedropYPosition(0), 2);
		
		t = 0.70f;
		adapter.updateFrames(10);
		newXshouldBe = quadraticBezierCurve(point0.x, point1.x, point2.x, t);
		newYshouldBe = quadraticBezierCurve(point0.y, point1.y, point2.y, t);
		assertEquals(newXshouldBe, adapter.getIcedropXPosition(0), 2);
		assertEquals(newYshouldBe, adapter.getIcedropYPosition(0), 2);
		
		t = 0.90f;
		adapter.updateFrames(10);
		newXshouldBe = quadraticBezierCurve(point0.x, point1.x, point2.x, t);
		newYshouldBe = quadraticBezierCurve(point0.y, point1.y, point2.y, t);
		assertEquals(newXshouldBe, adapter.getIcedropXPosition(0), 2);
		assertEquals(newYshouldBe, adapter.getIcedropYPosition(0), 2);
		
		t = 1.0f;
		adapter.updateFrames(5);
		newXshouldBe = quadraticBezierCurve(point0.x, point1.x, point2.x, t);
		newYshouldBe = quadraticBezierCurve(point0.y, point1.y, point2.y, t);
		assertEquals(newXshouldBe, adapter.getIcedropXPosition(0), 2);
		assertEquals(newYshouldBe, adapter.getIcedropYPosition(0), 2);
		
		//Now the icedrop should be following
		
		point0.x = characterXCoordinate + 70;
		point0.y = characterYCoordinate - 240;
		point2 = new Vector2f(
				adapter.getCharacterXPosition(0), 
				adapter.getCharacterYPosition(0));
		
		t = 0.1f;
		adapter.updateFrames(5);
		newXshouldBe = quadraticBezierCurve(point0.x, point1.x, point2.x, t);
		newYshouldBe = quadraticBezierCurve(point0.y, point1.y, point2.y, t);
		assertEquals(newXshouldBe, adapter.getIcedropXPosition(0), 2);
		assertEquals(newYshouldBe, adapter.getIcedropYPosition(0), 2);
		
		
		t = 0.30f;
		adapter.updateFrames(10);
		newXshouldBe = quadraticBezierCurve(point0.x, point1.x, point2.x, t);
		newYshouldBe = quadraticBezierCurve(point0.y, point1.y, point2.y, t);
		assertEquals(newXshouldBe, adapter.getIcedropXPosition(0), 2);
		assertEquals(newYshouldBe, adapter.getIcedropYPosition(0), 2);
		
		//TODO: same for casting to the left
		
	}
	
	@Test
	public void explosionKnockbackCurveTest() throws SlickException {
		int shieldOne = player1KeyConfig[7];
		int upOne = player1KeyConfig[2];
		int jumpOne = player1KeyConfig[6];
		
		adapter.setPositionofCharacter(0, 300, 400);
		adapter.setPositionofCharacter(1, 350, 400);
		adapter.setDirectionofCharacter(0, true);
		
		float startY = 400;
		
		adapter.handleKeyPressedPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, upOne);
		adapter.handleKeyDownPlayer1(0, jumpOne);
		
		adapter.getOurTestInput().clearDownKeys();
		adapter.getOurTestInput().clearPressedKeys();
		
		//knockback to the right
		
		float time = 0.0f;
		
		adapter.updateFrames(1);
		
		float newXshouldBe = adapter.getCharacterXPosition(1) + time;
		float y = knockbackFunction(time, startY, 1);
		float newYshouldBe = adapter.getCharacterYPosition(1) + y;
		
		assertEquals(newXshouldBe, adapter.getCharacterXPosition(1), 2);
		assertEquals(newYshouldBe, adapter.getCharacterYPosition(1), 2);

		//time = 0.1f;
		
		adapter.updateFrames(10);
		
		newXshouldBe = adapter.getCharacterXPosition(1) + time;
		y = knockbackFunction(time, startY, 1);
		newYshouldBe = adapter.getCharacterYPosition(1) + y;
		
		assertEquals(newXshouldBe, adapter.getCharacterXPosition(1), 2);
		assertEquals(newYshouldBe, adapter.getCharacterYPosition(1), 2);

		//time += 0.05;
		
		adapter.updateFrames(10);
		
		newXshouldBe = adapter.getCharacterXPosition(1) + time;
		y = knockbackFunction(time, startY, 1);
		newYshouldBe = adapter.getCharacterYPosition(1) + y;
		
		assertEquals(newXshouldBe, adapter.getCharacterXPosition(1), 2);
		assertEquals(newYshouldBe, adapter.getCharacterYPosition(1), 2);
		
	}
	
}
