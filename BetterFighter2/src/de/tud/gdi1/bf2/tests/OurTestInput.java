package de.tud.gdi1.bf2.tests;

import eea.engine.test.TestInput;

public class OurTestInput extends TestInput {

	private Integer[] downKeys;

	public OurTestInput(int height) {
		super(height);
		clearDownKeys();
	}

	@Override
	public boolean isKeyDown(int code) {
		for (Integer key : downKeys) {
			if (key.equals(code)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void setKeyDown(Integer... keys) {
		this.downKeys = keys;
	}

	@Override
	public void clearDownKeys() {
		downKeys = new Integer[0];
	}
}
