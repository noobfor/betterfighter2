package de.tud.gdi1.bf2.tests;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Game;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;


public class OurTestAppGameContainer extends AppGameContainer {

	public OurTestAppGameContainer(Game game) throws SlickException {
		super(game, 640, 480, false);
	}

	public OurTestAppGameContainer(Game game, int width, int height, boolean fullscreen) throws SlickException {
		super(game, width, height, fullscreen);
	}

	/**
	 * Analog zu AppGameContainer.start() wird das StateBasedGame im
	 * TestAppGameContainer gestartet, d.h. das Spiel wird ohne UI gestartet.
	 * 
	 * @param delta
	 *            Verzoegerung zwischen Frames
	 * 
	 * @throws SlickException
	 */
	public void start(int delta) throws SlickException {

		this.input = new OurTestInput(delta);
		game.update(this, delta);
		((StateBasedGame) game).initStatesList(this);
		game.init(this);

	}

	public OurTestInput getOurTestInput() {
		return (OurTestInput) input;
	}

	public void updateGame(int delta) throws SlickException {
		game.update(this, delta/2);
		game.update(this, delta/2);
		
	}

	@Override
	public void reinit() {
		try {
			game.init(this);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
