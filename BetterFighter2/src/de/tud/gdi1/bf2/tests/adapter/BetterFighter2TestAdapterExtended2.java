package de.tud.gdi1.bf2.tests.adapter;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.entities.spells.FireBreath;
import de.tud.gdi1.bf2.model.entities.spells.IceWall;
import eea.engine.entity.Entity;

public class BetterFighter2TestAdapterExtended2 extends BetterFighter2TestAdapterExtended1 {

	/**
	 * Use this constructor to set up everything you need.
	 */
	public BetterFighter2TestAdapterExtended2() {
		super();
	}

	public List<IceWall> getIceWalls() {
		List<IceWall> iceWalls = new ArrayList<IceWall>();

		for (Entity entity : getEntities()) {
			if (entity instanceof IceWall) {
				iceWalls.add((IceWall) entity);
			}
		}
		return iceWalls;
	}

	public List<Vector2f> getIceWallSizes() {
		List<Vector2f> sizes = new ArrayList<>();
		for (IceWall icewall : getIceWalls()) {
			sizes.add(icewall.getSize());
		}
		return sizes;
	}

	public List<Vector2f> getIceWallPositions() {
		List<Vector2f> positions = new ArrayList<>();
		for (IceWall icewall : getIceWalls()) {
			positions.add(icewall.getPosition());
		}
		return positions;
	}

	public boolean isPlayer1Freezed() {
		return getPlayers().get(0).getChar().isFreezed();
	}

	public FireBreath getFireBreathEntity() {
		for (Entity e : getEntities()) {
			if (e instanceof FireBreath) {
				return (FireBreath) e;
			}
		}
		return null;
	}

}
