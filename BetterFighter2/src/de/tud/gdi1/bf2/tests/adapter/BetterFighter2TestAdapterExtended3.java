package de.tud.gdi1.bf2.tests.adapter;

import java.util.ArrayList;
import java.util.List;

import de.tud.gdi1.bf2.model.entities.spells.IceDrop;
import de.tud.gdi1.bf2.model.player.Player;
import eea.engine.entity.Entity;

public class BetterFighter2TestAdapterExtended3 extends BetterFighter2TestAdapterExtended2 {

	/**
	 * Use this constructor to set up everything you need.
	 */
	public BetterFighter2TestAdapterExtended3() {
		super();
	}
	
	
	/*
	 * ***************************************************
	 * ********************** Spells *********************
	 * ***************************************************
	 */
	
	/**
	 * Get icedrop X position from the icerain move with the specific number
	 * @param number the specific number of the icedrop
	 */
	public float getIcedropXPosition(int number) {
		
		List<IceDrop> icedrops = new ArrayList<IceDrop>();

		for (Entity entity : getEntities()) {
			if (entity.getID().startsWith("IceDrop")) {
				icedrops.add((IceDrop) entity);
			}
		}
		
		return icedrops.get(number).getPosition().getX();
	}
	
	/**
	 * Get icedrop Y position from the icerain move with the specific number
	 * @param number the specific number of the icedrop
	 */
	public float getIcedropYPosition(int number) {
		
		List<IceDrop> icedrops = new ArrayList<IceDrop>();

		for (Entity entity : getEntities()) {
			if (entity.getID().startsWith("IceDrop")) {
				icedrops.add((IceDrop) entity);
			}
		}
		
		return icedrops.get(number).getPosition().getY();
	}
	
	/*
	 * ***************************************************
	 * ********************** Player *********************
	 * ***************************************************
	 */
	
	/**
	 * Returns if the character of the given playerposition is in explosion knockback
	 * @param position playerposition 0 or 1
	 * @return boolean for explosion knockback or not
	 */
	public boolean isExplosionKnockback(int position) {
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		return players.get(position).getChar().isInExplosionKnockback();
	}
	
	public boolean isUsingFireExplosion(int position) {
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		return players.get(position).getChar().isUsingSpecialMove();
	}
	
}
