package de.tud.gdi1.bf2.tests.adapter;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.entities.spells.Fireball;
import de.tud.gdi1.bf2.model.entities.spells.Iceball;
import de.tud.gdi1.bf2.model.player.Player;
import de.tud.gdi1.bf2.ui.BetterFighter2;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;

public class BetterFighter2TestAdapterExtended1 extends BetterFighter2TestAdapterMinimal {

	/**
	 * Use this constructor to set up everything you need.
	 */
	public BetterFighter2TestAdapterExtended1() {
		super();
	}

	
	/**
	 * get the entity list of the current gameplaystate
	 * @return list of entities
	 */
	public List<Entity> getEntities() {
		if (betterfighter2 != null) {
			return StateBasedEntityManager.getInstance().getEntitiesByState(BetterFighter2.GAMEPLAY_STATE);
		}
		
		return null;
	}

	
	/*
	 * ***************************************************
	 * ********************** Spells *********************
	 * ***************************************************
	 */
	
	/**
	 * Get iceball X position with the specific number
	 * @param playernumber number of player, either 0 or 1 for 2 players
	 * @param iceballnumber number of the ith iceball
	 */
	public float getIceballXPosition(int playernumber, int iceballnumber) {
		
		List<Iceball> iceballs = new ArrayList<Iceball>();
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		for (Entity entity : getEntities()) {
			if (entity.getID().startsWith(players.get(playernumber).getChar().getID())) {
				if(entity instanceof Iceball)
				iceballs.add((Iceball) entity);
			}
		}
		
		return iceballs.get(iceballnumber).getPosition().getX();
	}
	
	/**
	 * Get iceball X position with the specific number
	 * @param playernumber number of player, either 0 or 1 for 2 players
	 * @param iceballnumber number of the ith iceball
	 */
	public float getIceballYPosition(int playernumber, int iceballnumber) {
		
		List<Iceball> iceballs = new ArrayList<Iceball>();
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		for (Entity entity : getEntities()) {
			if (entity.getID().startsWith(players.get(playernumber).getChar().getID())) {
				if(entity instanceof Iceball)
				iceballs.add((Iceball) entity);
			}
		}
		
		return iceballs.get(iceballnumber).getPosition().getY();
	}
	
	/**
	 * Sets new Position for the specific projectile
	 * @param playernumber number of player, either 0 or 1 for 2 players
	 * @param iceballnumber number of the ith iceball
	 * @param x new x position
	 * @param y new y position
	 */
	public void setNewPositionofIceball(int playernumber, int iceballnumber, float x, float y) {
		List<Iceball> iceballs = new ArrayList<Iceball>();
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		for (Entity entity : getEntities()) {
			if (entity.getID().startsWith(players.get(playernumber).getChar().getID())) {
				if(entity instanceof Iceball)
				iceballs.add((Iceball) entity);
			}
		}
		
		iceballs.get(iceballnumber).setPosition(new Vector2f(x,y));
	}
	

	/**
	 * Get fireball X position with the specific number
	 * @param playernumber number of player, either 0 or 1 for 2 players
	 * @param fireballnumber number of the ith fireball
	 */
	public float getFireballXPosition(int playernumber, int fireballnumber) {
		
		List<Fireball> fireballs = new ArrayList<Fireball>();
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		for (Entity entity : getEntities()) {
			if (entity.getID().startsWith(players.get(playernumber).getChar().getID())) {
				if(entity instanceof Fireball)
				fireballs.add((Fireball) entity);
			}
		}
		
		return fireballs.get(fireballnumber).getPosition().getX();
	}
	
	/**
	 * Get fireball Y position with the specific number
	 * @param playernumber number of player, either 0 or 1 for 2 players
	 * @param fireballnumber number of the ith fireball
	 */
	public float getFireballYPosition(int playernumber, int fireballnumber) {
		
		List<Fireball> fireballs = new ArrayList<Fireball>();
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		for (Entity entity : getEntities()) {
			if (entity.getID().startsWith(players.get(playernumber).getChar().getID())) {
				if(entity instanceof Fireball)
				fireballs.add((Fireball) entity);
			}
		}
		
		return fireballs.get(fireballnumber).getPosition().getY();
	}
	
	/**
	 * Sets new Position for the specific projectile
	 * @param playernumber number of player, either 0 or 1 for 2 players
	 * @param fireballnumber number of the ith fireball
	 * @param x new x position
	 * @param y new y position
	 */
	public void setNewPositionofFireball(int playernumber, int fireballnumber, float x, float y) {
		List<Fireball> fireballs = new ArrayList<Fireball>();
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		for (Entity entity : getEntities()) {
			if (entity.getID().startsWith(players.get(playernumber).getChar().getID())) {
				if(entity instanceof Fireball)
				fireballs.add((Fireball) entity);
			}
		}
		
		fireballs.get(fireballnumber).setPosition(new Vector2f(x,y));
		
	}
	

	/*
	 * ***************************************************
	 * ********************** Player *********************
	 * ***************************************************
	 */
	
	/**
	 * Returns if the character of the given playerposition is in Fireball knockback
	 * @param position playerposition 0 or 1
	 * @return boolean for fireball knockback or not
	 */
	public boolean isFireballKnockback(int position) {
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		return players.get(position).getChar().isStrongKnockback();
	}

	/**
	 * Returns if the character of the given playerposition is freezed
	 * @param position playerposition 0 or 1
	 * @return boolean for freezed or not
	 */
	public boolean isFreezed(int position) {
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		return players.get(position).getChar().isFreezed();
	}
	
	/**
	 * Returns if the character of the given playerposition is on the ground
	 * @param position playerposition 0 or 1
	 * @return boolean for on the ground or not
	 */
	public boolean isOnTheGroundKnockback(int position) {
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		return players.get(position).getChar().isOntheGround();
	}
	
	
}
