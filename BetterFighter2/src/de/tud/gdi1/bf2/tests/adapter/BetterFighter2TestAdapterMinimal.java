package de.tud.gdi1.bf2.tests.adapter;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.BudsyCharacter;
import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.entities.characters.NoobforCharacter;
import de.tud.gdi1.bf2.model.entities.map.Wall;
import de.tud.gdi1.bf2.model.player.Player;
import de.tud.gdi1.bf2.tests.OurTestAppGameContainer;
import de.tud.gdi1.bf2.tests.OurTestInput;
import de.tud.gdi1.bf2.ui.BetterFighter2;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;

/**
 * This is the test adapter for the minimal stage of completion. You <b>must</b>
 * implement the method stubs and match them to your concrete implementation.
 * Please read all the Javadoc of a method before implementing it. <br>
 * <strong>Important:</strong> this class should not contain any real game
 * logic, you should rather only match the method stubs to your game. <br>
 * Example: in {@link #getStartButtonXPosition()} you may return the value
 * <i>startButton.getPosition().getX()</i>, after you get all Entities from the main menu
 * and you have a Entity <i>startButton</i> which
 * is located in the main menu. 
 * What you mustn't do is to implement the actual logic of the method in this class. <br>
 * <br>
 * If you have implemented the minimal stage of completion, you should be able
 * to implement all method stubs. The public and private JUnit tests for the
 * minimal stage of completion will be run on this test adapter. The other test
 * adapters will inherit from this class, because they need the basic methods
 * (like getting positions of a character), too. <br>
 * <br>
 * The methods of all test adapters need to function without any kind of user
 * interaction.</br>
 * 
 * <i>Note:</i> All other test adapters will inherit from this class.
 * 
 * @see BetterFighter2TestAdapterExtended1
 * @see BetterFighter2TestAdapterExtended2
 * @see BetterFighter2TestAdapterExtended3
 */
public class BetterFighter2TestAdapterMinimal {

	protected BetterFighter2 betterfighter2; // erbt von StateBasedGame
	OurTestAppGameContainer app; // spezielle Variante des AppGameContainer,
									// welche keine UI erzeugt (nur f�r Tests!)
	boolean syntaxException; // gibt es Syntax-Fehler
	boolean semanticException; // gibt es Semantik-Fehler


	/**
	 * Verwenden Sie diesen Konstruktor zur Initialisierung von allem, was sie
	 * benoetigen
	 * 
	 * Use this constructor to set up everything you need.
	 */
	public BetterFighter2TestAdapterMinimal() {
		super();
		betterfighter2 = null;
		syntaxException = true;
		semanticException = true;
	}

	/*
	 * *************************************************** 
	 * ********* initialize,run, stop the game ***********
	 * ***************************************************
	 */

	public StateBasedGame getStateBasedGame() {
		return betterfighter2;
	}

	/**
	 * Diese Methode initialisiert das Spiel im Debug-Modus, d.h. es wird ein
	 * AppGameContainer gestartet, der keine Fenster erzeugt und aktualisiert.
	 * 
	 * Sie müssen diese Methode erweitern
	 */
	public void initializeGame() {

		// Setze den library Pfad abhaengig vom Betriebssystem
		if (System.getProperty("os.name").toLowerCase().contains("windows")) {
			System.setProperty("org.lwjgl.librarypath", System.getProperty("user.dir") + "/native/windows");
		} else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
			System.setProperty("org.lwjgl.librarypath", System.getProperty("user.dir") + "/native/macosx");
		} else {
			System.setProperty("org.lwjgl.librarypath",
					System.getProperty("user.dir") + "/native/" + System.getProperty("os.name").toLowerCase());
		}
		// Initialisiere das Spiel BetterFighter2 im Debug-Modus (ohne UI-Ausgabe)

		betterfighter2 = new BetterFighter2(true);

		try {
			app = new OurTestAppGameContainer(betterfighter2);
			app.start(0);
		} catch (SlickException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Stoppe das im Hintergrund laufende Spiel
	 */
	public void stopGame() {
		if (app != null) {
			app.exit();
			app.destroy();
		}
		StateBasedEntityManager.getInstance().clearAllStates();
		betterfighter2 = null;
	}

	/**
	 * Run the game for a specified duration. Laesst das Spiel fuer eine angegebene
	 * Zeit laufen
	 * 
	 * @param ms
	 *            duration of runtime of the game
	 */
	public void runGame(int ms) {
		if (betterfighter2 != null && app != null) {
			try {
				app.updateGame(ms);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
	}
	

	/**
	 * updates the game the specific amount of time, each update counts as two update
	 * @param i the number of times the update method should be called
	 * @throws SlickException 
	 */
	public void updateFrames(int i) throws SlickException {
		for(int j = 0; j < i; j++) {
		app.updateGame(1);
		}
	}
	
	public void updateGame(int delta) throws SlickException {
		app.updateGame(delta);
	}
	
	public OurTestInput getOurTestInput() {
		return app.getOurTestInput();
	}
	
	/*
	 * *************************************************** 
	 * ******************** MainMenu *********************
	 * ***************************************************
	 */

	/**
	 * start button to get into character select
	 * 
	 * @return x coordinate of the start button
	 * @throws Exception
	 */
	public int getStartButtonXPosition() throws Exception {

		List<Entity> entities = new ArrayList<Entity>();

		if (betterfighter2 != null)
			entities = StateBasedEntityManager.getInstance().getEntitiesByState(BetterFighter2.MAINMENU_STATE);

		Entity startButton = null;

		for (Entity entity : entities) {
			if (entity.getID().startsWith("Vs. Mode")) {
				startButton = entity;
			}

		}
		if (startButton == null) {
			throw new Exception("Start button not found");
		}

		return (int) startButton.getPosition().getX();
	}

	/**
	 * start button to get into character select
	 * 
	 * @return y coordinate of the start button
	 * @throws Exception
	 */
	public int getStartButtonYPosition() throws Exception {

		List<Entity> entities = new ArrayList<Entity>();

		if (betterfighter2 != null)
			entities = StateBasedEntityManager.getInstance().getEntitiesByState(BetterFighter2.MAINMENU_STATE);

		Entity startButton = null;

		for (Entity entity : entities) {
			if (entity.getID().startsWith("Vs. Mode")) {
				startButton = entity;
			}
		}
		if (startButton == null) {
			throw new Exception("Start button not found");
		}

		return (int) startButton.getPosition().getY();
	}

	/*
	 * *************************************************** 
	 * *************** Characterselection ****************
	 * ***************************************************
	 */


	/**
	 * start button to get into gameplay state
	 * 
	 * @return x coordinate of the start button
	 */
	public int getStartGameButtonXPosition() {

		List<Entity> entities = new ArrayList<Entity>();

		if (betterfighter2 != null)
			entities = StateBasedEntityManager.getInstance().getEntitiesByState(BetterFighter2.MAINMENU_STATE);

		List<Entity> startButton = new ArrayList<Entity>();

		for (Entity entity : entities) {
			if (entity.toString().startsWith("VSStart")) {
				startButton.add(entity);
			}
		}

		return (int) startButton.get(0).getPosition().getX();
	}

	/**
	 * start button to get into character select
	 * 
	 * @return y coordinate of the start button
	 */
	public int getStartGameButtonYPosition() {

		List<Entity> entities = new ArrayList<Entity>();

		if (betterfighter2 != null)
			entities = StateBasedEntityManager.getInstance().getEntitiesByState(BetterFighter2.MAINMENU_STATE);

		List<Entity> startButton = new ArrayList<Entity>();

		for (Entity entity : entities) {
			if (entity.toString().startsWith("VSStart")) {
				startButton.add(entity);
			}
		}

		return (int) startButton.get(0).getPosition().getY();
	}
	
	

	/*
	 * ***************************************************
	 * ********************** Player *********************
	 * ***************************************************
	 */

	/**
	 * 
	 * @return Liste von Spielern
	 */
	public List<Player> getPlayers() {

		if (betterfighter2 == null)
			System.err.println("The game betterfighter2 has not been initialized, tanks is null!");

		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}

		return players;
	}

	/**
	 * get the character entity with the specific number
	 * TODO: remove this before endversion
	 * @return character von player mit uebergebener nummer
	 */
	public Character getCharacter(int number) {

		if (betterfighter2 == null)
			System.err.println("The game betterfighter2 has not been initialized, betterfighter2 is null!");

		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}

		return players.get(number).getChar();
	}

	/**
	 * @param position
	 *            : Spielernummer
	 * @return Maximale Lebenspunkte des Spielers mit der uebergebenen Spielernummer
	 */
	public int getCharacterMaxLife(int position) {

		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}

		return players.get(position).getChar().getMaxHealthpoints();
	}

	/**
	 * @param position
	 *            : Spielernummer
	 * @return Aktuelle Lebenspunkte des Spielers mit der uebergebenen Spielernummer
	 */
	public int getCharacterActualLife(int position) {

		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}

		return players.get(position).getChar().getHealth();
	}
	
	/**
	 * reduziert die Lebenspunkte eines Charakters
	 * @param position Spielernummer
	 * @param health Anzahl an Lebenspunkten die reduziert werden soll
	 */
	public void decreaseCharacterLife(int position, int health) {

		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}

		players.get(position).getChar().decreaseHealth(health);
		
	}

	/**
	 * @param position
	 *            : Spielernummer
	 * @return Maximale Manapunkte des Spielers mit der uebergebenen Spielernummer
	 */
	public int getCharacterMaxMana(int position) {

		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}

		return players.get(position).getChar().getMaxManapoints();
	}

	/**
	 * @param position
	 *            : Spielernummer
	 * @return Aktuelle Lebenspunkte des Spielers mit der uebergebenen Spielernummer
	 */
	public int getCharacterActualMana(int position) {

		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}

		return players.get(position).getChar().getHealth();
	}

	/**
	 * @param position
	 *            : Spielernummer
	 * @return Aktuelle X Koordinate des Spielers mit der uebergebenen Spielernummer
	 */
	public int getCharacterXPosition(int position) {

		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}

		return (int) players.get(position).getChar().getPosition().getX();
	}

	/**
	 * @param position
	 *            : Spielernummer
	 * @return Aktuelle X Koordinate des Spielers mit der uebergebenen Spielernummer
	 */
	public int getCharacterYPosition(int position) {

		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}

		return (int) players.get(position).getChar().getPosition().getY();
	}
	
	/**
	 * get the character position as a vector2f
	 * @param index number of player, 0 or 1
	 * @return position of the character as vector2f
	 */
	public Vector2f getCharacterPosition(int index) {

		if (betterfighter2 == null)
			System.err.println("The game betterfighter2 has not been initialized, betterfighter2 is null!");

		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}

		return players.get(index).getChar().getPosition();
	}
	
	/**
	 * 
	 * @param position
	 * @param newDirection
	 */
	public void setDirectionofCharacter(int position, boolean newDirection) {
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		players.get(position).getChar().setDirection(newDirection);
	}
	
	/**
	 * returns the direction of the charakter with the given player position
	 * @param position the player position, 0 or 1
	 */
	public boolean getDirectionofCharacter(int position) {
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		return players.get(position).getChar().getDirection();
	}
	
	/**
	 * Bewegt Charakter mit der �bergebenen Spielernummer 
	 * zur neuen Position mit den angegebenen x und y Werten
	 * @param position Spielernummer, entweder 0 oder 1
	 * @param x Neue x Position des charakters
	 * @param y Neue y Position des Charakters
	 */
	public void setPositionofCharacter(int position, float x, float y) {
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		players.get(position).getChar().setPosition(new Vector2f(x,y));
		
	}

	public boolean isPunching(int position) {
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		return players.get(position).getChar().isPunching();
	}
	
	public boolean isShielding(int position) {
		List<Player> players = new ArrayList<Player>();

		for (Player getPlayer : betterfighter2.getPlayers()) {
			players.add(getPlayer);
		}
		
		return players.get(position).getChar().isShielding();
	}
	

	public float jumpFunction(float x, float verticalOffset, float jumpHeight, float jumpTime) {

		float result = (float) (x * (4 * jumpHeight * (jumpTime - x) + jumpTime * verticalOffset)
				/ Math.pow(jumpTime, 2));
		return result;
	}
	
	/**
	 * Returns if the player 1 has selected the fire character
	 * @return boolean whether the player 1 is the fire character
	 */
	public boolean isPlayer1FireCharacter() {
		return getPlayers().get(0).getChar() instanceof BudsyCharacter;
	}
	
	/**
	 * Return if the player 2 has selected the ice character
	 * @return boolean whether the player 2 is the ice character
	 */
	public boolean isPlayer2IceCharacter() {
		return getPlayers().get(1).getChar() instanceof NoobforCharacter;
	}
	
	/**
	 * This method controls the selection of each player for each character
	 * @param playerIndex int the index of the player that selects the character
	 * @param characterIndex int the index of the character to be selected
	 */
	public void playerSelectCharacter(int playerIndex, int characterIndex) {

		int[] keyConfig = getPlayers().get(playerIndex).getKeyConfig();

		if (characterIndex == 0) {
			handleKeyDownPlayer1(0, keyConfig[1]);
		} else {
			handleKeyDownPlayer1(0, keyConfig[0]);
		}

	}
	
	/*
	 * ***************************************************
	 * ************************ Wall ********************* 
	 * ***************************************************
	 */

	/**
	 * Wall ist die Bezeichnung eine unzerst�rbaren Wand, die die Arena begrenzt
	 * 
	 * @param Wall-Nummer
	 *            der Wall, dabei ist 0 = Norden, 1 = Ost, 2 = S�den, 3 = Westen
	 * @return Wall mit der �bergebenen Nummer
	 */
	public Wall getWall(int position) {

		List<Entity> entities = new ArrayList<Entity>();

		if (betterfighter2 != null)
			entities = StateBasedEntityManager.getInstance().getEntitiesByState(BetterFighter2.GAMEPLAY_STATE);

		List<Wall> walls = new ArrayList<Wall>();

		for (Entity entity : entities) {
			if (entity.getID().startsWith("wall")) {
				walls.add((Wall) entity);
			}
		}

		if (position < 0 || position > walls.size() - 1)
			return null;

		return walls.get(position);
	}

	public Wall[] getWalls() {

		List<Entity> entities = new ArrayList<Entity>();

		if (betterfighter2 != null)
			entities = StateBasedEntityManager.getInstance().getEntitiesByState(BetterFighter2.GAMEPLAY_STATE);

		List<Wall> walls = new ArrayList<Wall>();

		for (Entity entity : entities) {
			if (entity instanceof Wall) {
				walls.add((Wall) entity);
			}
		}

		return walls.toArray(new Wall[walls.size()]);
	}

	/**
	 * Wall ist die Bezeichnung eine unzerst�rbaren Wand, die die Arena begrenzt
	 * 
	 * @param Wall-Nummer
	 *            der Wall, dabei ist 0 = Norden, 1 = Ost, 2 = S�den, 3 = Westen
	 * @return Aktuelle Rotation in Grad der Wall mit der uebergebenen Wall-Nummer
	 */
	public int getWallRotation(int position) {
		List<Entity> entities = new ArrayList<Entity>();

		if (betterfighter2 != null)
			entities = StateBasedEntityManager.getInstance().getEntitiesByState(BetterFighter2.GAMEPLAY_STATE);

		List<Wall> walls = new ArrayList<Wall>();

		for (Entity entity : entities) {
			if (entity.toString().startsWith("Wall")) {
				walls.add((Wall) entity);
			}
		}

		if (position < 0 || position > walls.size() - 1)
			return -1;

		return (int) walls.get(position).getRotation();
	}

	/**
	 * Wall ist die Bezeichnung eine unzerst�rbaren Wand, die die Arena begrenzt
	 * 
	 * @param Wall-Nummer
	 *            der Wall, dabei ist 0 = Norden, 1 = Ost, 2 = S�den, 3 = Westen
	 * @return Aktuelle x-Koordinate der Wall mit der uebergebenen Wall-Nummer
	 */
	public int getWallXPosition(int position) {

		List<Entity> entities = new ArrayList<Entity>();

		if (betterfighter2 != null)
			entities = StateBasedEntityManager.getInstance().getEntitiesByState(BetterFighter2.GAMEPLAY_STATE);

		List<Wall> walls = new ArrayList<Wall>();

		for (Entity entity : entities) {
			if (entity.toString().startsWith("Wall")) {
				walls.add((Wall) entity);
			}
		}

		if (position < 0 || position > walls.size() - 1)
			return -1;

		return (int) walls.get(position).getPosition().getX();

	}

	/**
	 * Wall ist die Bezeichnung eine unzerst�rbaren Wand, die die Arena begrenzt
	 * 
	 * @param Wall-Nummer
	 *            der Wall, dabei ist 0 = Norden, 1 = Ost, 2 = S�den, 3 = Westen
	 * @return Aktuelle x-Koordinate der Wall mit der uebergebenen Wall-Nummer
	 */
	public int getWallYPosition(int position) {

		List<Entity> entities = new ArrayList<Entity>();

		if (betterfighter2 != null)
			entities = StateBasedEntityManager.getInstance().getEntitiesByState(BetterFighter2.GAMEPLAY_STATE);

		List<Wall> walls = new ArrayList<Wall>();

		for (Entity entity : entities) {
			if (entity.toString().startsWith("Wall")) {
				walls.add((Wall) entity);
			}
		}

		if (position < 0 || position > walls.size() - 1)
			return -1;

		return (int) walls.get(position).getPosition().getY();
	}

	/**
	 * @return Anzahl an Walls im aktuellen Spiel
	 */
	public int getWallCount() {

		List<Entity> entities = new ArrayList<Entity>();

		if (betterfighter2 != null)
			entities = StateBasedEntityManager.getInstance().getEntitiesByState(BetterFighter2.GAMEPLAY_STATE);

		int count = 0;
		for (Entity entity : entities) {
			if (entity.toString().startsWith("Wall")) {
				count++;
			}
		}
		return count;
	}

	/*
	 * *************************************************** 
	 * ********************** Input **********************
	 * ***************************************************
	 */

	/**
	 * This Method should emulate the key down event. This should make the Player1
	 * do various kinds of moves.
	 * 
	 * Diese Methode emuliert das Druecken beliebiger Tasten. (Dies soll es
	 * ermoeglichen, das Angreifen, etc. des Charakters zu testen)
	 * 
	 * @param updatetime
	 *            : Zeitdauer bis update-Aufruf
	 */
	public void handleKeyDownPlayer1(int updatetime, Integer... keys) {
		int key = 0;
		//TODO: maybe change input to use this for easier key selection in tests
		Integer keydown = betterfighter2.getPlayers()[0].getKeyConfig()[key];
		
		if (betterfighter2 != null && app != null) {
			app.getOurTestInput().setKeyDown(keys);
			try {
				app.updateGame(updatetime);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * This Method should emulate the key down event. This should make the Player2
	 * do various kinds of moves.. Diese Methode emuliert das Druecken der
	 * Angriffstaste von Spieler 2
	 * 
	 * @param updatetime
	 *            : Zeitdauer bis update-Aufruf
	 * @param keys
	 *            : z.B. Pfeiltasten, M, N, B oder V
	 */
	public void handleKeyDownPlayer2(int updatetime, Integer... keys) {
		if (betterfighter2 != null && app != null) {
			app.getOurTestInput().setKeyDown(keys);
			try {
				app.updateGame(updatetime);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * This Method should emulate the key pressed event. This should make the
	 * Player1 attack. Diese Methode emuliert das Druecken der Angriffstaste von
	 * Spieler 1
	 * 
	 * @param updatetime
	 *            : Zeitdauer bis update-Aufruf
	 * @param key
	 *            : z.B. W, A, S, D, F, G, H, Shift
	 */
	public void handleKeyPressedPlayer1(int updatetime, Integer... keys) {
		if (betterfighter2 != null && app != null) {
			app.getOurTestInput().setKeyPressed(keys);
			try {
				app.updateGame(updatetime);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * This Method should emulate the key pressed event. This should make the
	 * Player2 attack. Diese Methode emuliert das Druecken der Angriffstaste von
	 * Spieler 2
	 * 
	 * @param updatetime
	 *            : Zeitdauer bis update-Aufruf
	 * @param key
	 *            : z.B. Pfeiltasten, M, N, B oder V
	 */
	public void handleKeyPressedPlayer2(int updatetime, int key) {
		if (betterfighter2 != null && app != null) {
			app.getOurTestInput().setKeyPressed(key);
			try {
				app.updateGame(updatetime);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
	}

	public void handleMouseClicked(int updatetime) {
		if (betterfighter2 != null && app != null) {
			app.getOurTestInput().setMouseButtonPressed(Input.MOUSE_LEFT_BUTTON);
			try {
				app.updateGame(updatetime);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
	}

	public void handleMousePosition(int updatetime, int x, int y) {
		if (betterfighter2 != null && app != null) {
			app.getOurTestInput().setMouseX(x);
			app.getOurTestInput().setMouseY(y);
			try {
				app.updateGame(updatetime);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
	}

	public void handleSpaceKeyPressed(int updatetime) {
		if (betterfighter2 != null && app != null) {
			app.getOurTestInput().setKeyPressed(Input.KEY_SPACE);
			;
			try {
				app.updateGame(updatetime);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
	}

}
