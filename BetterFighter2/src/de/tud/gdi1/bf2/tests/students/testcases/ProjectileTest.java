package de.tud.gdi1.bf2.tests.students.testcases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.entities.spells.Fireball;
import de.tud.gdi1.bf2.model.entities.spells.Iceball;
import de.tud.gdi1.bf2.model.entities.spells.Spell;
import de.tud.gdi1.bf2.tests.adapter.BetterFighter2TestAdapterExtended1;
import de.tud.gdi1.bf2.ui.BetterFighter2;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;

public class ProjectileTest {

	BetterFighter2TestAdapterExtended1 adapter;
	int[] player1KeyConfig;
	int[] player2KeyConfig;

	@Before
	public void setUp() throws Exception {
		adapter = new BetterFighter2TestAdapterExtended1();
		adapter.initializeGame();
		player1KeyConfig = adapter.getPlayers().get(0).getKeyConfig();
		player2KeyConfig = adapter.getPlayers().get(1).getKeyConfig();

		adapter.handleMousePosition(0, adapter.getStartButtonXPosition(), adapter.getStartButtonYPosition());
		adapter.handleMouseClicked(0);
		
		// go to character selection state
		assertEquals(BetterFighter2.CHARACTER_SELECTION_STATE, adapter.getStateBasedGame().getCurrentStateID());
		adapter.playerSelectCharacter(0, 0);
        adapter.playerSelectCharacter(1, 1);
        adapter.handleSpaceKeyPressed(0);
        adapter.getOurTestInput().clearPressedKeys();

        // player 1 picks FireCharacter
        assertTrue(adapter.isPlayer1FireCharacter());
        // player 2 picks IceCharacter
        assertTrue(adapter.isPlayer2IceCharacter());
		// go to gameplay state
		
		assertEquals(BetterFighter2.GAMEPLAY_STATE, adapter.getStateBasedGame().getCurrentStateID());

		adapter.getOurTestInput().clearDownKeys();
	}

	@After
	public void finish() {
		adapter.stopGame();
	}

	@Test
	public void fireballLeftTest() throws SlickException {
		int shieldOne = player1KeyConfig[7];
		int leftOne = player1KeyConfig[1];
		int attackOne = player1KeyConfig[5];

		adapter.handleKeyPressedPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, leftOne);
		adapter.handleKeyDownPlayer1(0, attackOne);
		
		
		// test if projectile is flying to the left
		Vector2f lastPosition = (new Vector2f(adapter.getFireballXPosition(0, 0), adapter.getFireballYPosition(0, 0)));
		adapter.updateGame(100);
		Vector2f newPosition = (new Vector2f(adapter.getFireballXPosition(0, 0), adapter.getFireballYPosition(0, 0)));
		assertTrue(lastPosition.x > newPosition.x);

	}

	@Test
	public void fireballRightTest() throws SlickException {
		int shieldOne = player1KeyConfig[7];
		int rightOne = player1KeyConfig[0];
		int attackOne = player1KeyConfig[5];

		adapter.handleKeyPressedPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, rightOne);
		adapter.handleKeyDownPlayer1(0, attackOne);

		
		// test if projectile is flying to the right
		Vector2f lastPosition = (new Vector2f(adapter.getFireballXPosition(0, 0), adapter.getFireballYPosition(0, 0)));
		adapter.updateGame(100);
		Vector2f newPosition = (new Vector2f(adapter.getFireballXPosition(0, 0), adapter.getFireballYPosition(0, 0)));
		assertTrue(lastPosition.x < newPosition.x);
	}

	@Test
	public void IceballLeftTest() throws SlickException {
		int shieldTwo = player2KeyConfig[7];
		int leftTwo = player2KeyConfig[1];
		int attackTwo = player2KeyConfig[5];


		adapter.handleKeyPressedPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, leftTwo);
		adapter.handleKeyDownPlayer2(0, attackTwo);


		Vector2f lastPosition = (new Vector2f(adapter.getIceballXPosition(1, 0), adapter.getIceballYPosition(1, 0)));
		adapter.updateGame(100);
		Vector2f newPosition = (new Vector2f(adapter.getIceballXPosition(1, 0), adapter.getIceballYPosition(1, 0)));
		assertTrue(lastPosition.x > newPosition.x);
	}

	@Test
	public void IceballRightTest() throws SlickException {
		int shieldTwo = player2KeyConfig[7];
		int rightTwo = player2KeyConfig[0];
		int attackTwo = player2KeyConfig[5];

		adapter.handleKeyPressedPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, rightTwo);
		adapter.handleKeyDownPlayer2(0, attackTwo);


		Vector2f lastPosition = (new Vector2f(adapter.getIceballXPosition(1, 0), adapter.getIceballYPosition(1, 0)));
		adapter.updateGame(100);
		Vector2f newPosition = (new Vector2f(adapter.getIceballXPosition(1, 0), adapter.getIceballYPosition(1, 0)));
		assertTrue(lastPosition.x < newPosition.x);
	}
	
	@Test
	public void testFireballHit() throws SlickException {
		int shieldOne = player1KeyConfig[7];
		int rightOne = player1KeyConfig[0];
		int attackOne = player1KeyConfig[5];

		adapter.setPositionofCharacter(0, 300, 300);
		adapter.setPositionofCharacter(1, 350, 300);
		
		adapter.handleKeyPressedPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, rightOne);
		adapter.handleKeyDownPlayer1(0, attackOne);
		
		adapter.updateGame(130);
		
		assertTrue(adapter.getCharacterActualLife(1) < 100);
		assertTrue(adapter.isFireballKnockback(1));
		
		adapter.updateFrames(15);
		
		assertTrue(adapter.isOnTheGroundKnockback(1));
		
	}
	
	@Test
	public void testIceballHit() throws SlickException {
		int shieldTwo = player2KeyConfig[7];
		int rightTwo = player2KeyConfig[0];
		int attackTwo = player2KeyConfig[5];

		adapter.setPositionofCharacter(1, 300, 300);
		adapter.setPositionofCharacter(0, 340, 300);
		
		adapter.handleKeyPressedPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, rightTwo);
		adapter.handleKeyDownPlayer2(0, attackTwo);
		
		adapter.getOurTestInput().clearDownKeys();
		adapter.getOurTestInput().clearPressedKeys();
		
		adapter.updateGame(300);
		
		assertTrue(adapter.getCharacterActualLife(0) < 100);
		assertTrue(adapter.isFreezed(0));
		
		// Wait for shield to finish, then attack again
		adapter.updateFrames(25);
		
		adapter.setPositionofCharacter(1, adapter.getCharacterXPosition(0), adapter.getCharacterYPosition(0));
		adapter.handleKeyPressedPlayer2(0, attackTwo);
		
		assertTrue(adapter.isOnTheGroundKnockback(0));
		
	}
	
	@Test
	public void testFireballShield() throws SlickException {
		int shieldOne = player1KeyConfig[7];
		int rightOne = player1KeyConfig[0];
		int attackOne = player1KeyConfig[5];

		adapter.setPositionofCharacter(0, 300, 300);
		adapter.setPositionofCharacter(1, 350, 300);
		
		adapter.handleKeyPressedPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, rightOne);
		adapter.handleKeyDownPlayer1(0, attackOne);
		
		adapter.getOurTestInput().clearDownKeys();
		adapter.getOurTestInput().clearPressedKeys();
		
		adapter.handleKeyPressedPlayer2(0, player1KeyConfig[7]);
		
		adapter.updateGame(130);
		
		assertTrue(adapter.getCharacterActualLife(1) < 100);
		assertTrue(adapter.isFireballKnockback(1));
		
		adapter.updateFrames(15);
		
		assertTrue(adapter.isOnTheGroundKnockback(1));
		
	}
	
	@Test
	public void testIceballShield() throws SlickException {
		int shieldTwo = player2KeyConfig[7];
		int rightTwo = player2KeyConfig[0];
		int attackTwo = player2KeyConfig[5];

		adapter.setPositionofCharacter(1, 300, 300);
		adapter.setPositionofCharacter(0, 340, 300);
		
		adapter.handleKeyPressedPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, rightTwo);
		adapter.handleKeyDownPlayer2(0, attackTwo);
		
		adapter.getOurTestInput().clearDownKeys();
		adapter.getOurTestInput().clearPressedKeys();
		
		adapter.handleKeyPressedPlayer1(0, player1KeyConfig[7]);
		
		adapter.updateGame(300);
		
		assertTrue(adapter.getCharacterActualLife(0) == 100);
		
	}
	
	@Test
	public void testReflect() throws SlickException {

		int shieldTwo = player2KeyConfig[7];
		int rightTwo = player2KeyConfig[0];
		int attackTwo = player2KeyConfig[5];
		int attackOne = player1KeyConfig[5];

		adapter.setPositionofCharacter(1, 250, 400);
		adapter.setPositionofCharacter(0, 400, 400);
		adapter.setDirectionofCharacter(0, false);
		
		adapter.handleKeyPressedPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, rightTwo);
		adapter.handleKeyDownPlayer2(0, attackTwo);
		
		adapter.getOurTestInput().clearDownKeys();
		adapter.getOurTestInput().clearPressedKeys();
		
		adapter.setNewPositionofIceball(1, 0, 360, 400);

		adapter.handleKeyPressedPlayer1(0, attackOne);
		
		adapter.updateFrames(1);
		adapter.updateGame(100);
		
		assertTrue(adapter.getIceballXPosition(0, 0) < 395);
		
		
	}
	
	
}
