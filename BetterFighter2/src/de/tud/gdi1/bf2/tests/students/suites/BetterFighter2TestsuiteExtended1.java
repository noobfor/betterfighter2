package de.tud.gdi1.bf2.tests.students.suites;

import de.tud.gdi1.bf2.tests.students.testcases.ProjectileTest;
import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.framework.TestSuite;

public class BetterFighter2TestsuiteExtended1 {
	
	public static Test suite() {
		
		TestSuite suite = new TestSuite("Student tests for BetterFighter2 - Extended 1");
		suite.addTest(new JUnit4TestAdapter(ProjectileTest.class));
		return suite;
	}
	
}
