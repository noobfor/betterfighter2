package de.tud.gdi1.bf2.tests.students.testcases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.entities.map.Wall;
import de.tud.gdi1.bf2.tests.adapter.BetterFighter2TestAdapterMinimal;
import de.tud.gdi1.bf2.ui.BetterFighter2;
import de.tud.gdi1.bf2.model.entities.characters.Character;

public class BasicActionsTest {

	BetterFighter2TestAdapterMinimal adapter;
	int[] player1KeyConfig;
	int[] player2KeyConfig;

	@Before
	public void setUp() throws Exception {
		adapter = new BetterFighter2TestAdapterMinimal();
		adapter.initializeGame();
		
		player1KeyConfig = adapter.getPlayers().get(0).getKeyConfig();
		player2KeyConfig = adapter.getPlayers().get(1).getKeyConfig();

		adapter.handleMousePosition(0, adapter.getStartButtonXPosition(), adapter.getStartButtonYPosition());
		adapter.handleMouseClicked(0);
		
		// go to character selection state
		assertEquals(BetterFighter2.CHARACTER_SELECTION_STATE, adapter.getStateBasedGame().getCurrentStateID());
		adapter.playerSelectCharacter(0, 0);
        adapter.playerSelectCharacter(1, 1);
        adapter.handleSpaceKeyPressed(0);
        adapter.getOurTestInput().clearPressedKeys();

        // player 1 picks FireCharacter
        assertTrue(adapter.isPlayer1FireCharacter());
        // player 2 picks IceCharacter
        assertTrue(adapter.isPlayer2IceCharacter());
		// go to gameplay state
		assertEquals(BetterFighter2.GAMEPLAY_STATE, adapter.getStateBasedGame().getCurrentStateID());

		adapter.getOurTestInput().clearDownKeys();
	}

	@After
	public void finish() {
		adapter.stopGame();
	}

	@Test
	public void testWalkRight() {

		int rightOne = player1KeyConfig[0];
		int rightTwo = player2KeyConfig[0];
		
		Vector2f lastPosition = adapter.getCharacterPosition(0);
		adapter.handleKeyDownPlayer1(100, rightOne);
		Vector2f newPosition = adapter.getCharacterPosition(0);

		assertEquals(lastPosition.x + 10, newPosition.x, 0.1);

		lastPosition = adapter.getCharacterPosition(1);
		adapter.handleKeyDownPlayer2(100, rightTwo);
		newPosition = adapter.getCharacterPosition(1);

		assertEquals(lastPosition.x + 10, newPosition.x, 0.1);

	}

	@Test
	public void testWalkLeft() {

		int leftOne = player1KeyConfig[1];
		int leftTwo = player2KeyConfig[1];

		Vector2f lastPosition = adapter.getCharacterPosition(0);
		adapter.handleKeyDownPlayer1(100, leftOne);
		Vector2f newPosition = adapter.getCharacterPosition(0);

		assertEquals(lastPosition.x - 10, newPosition.x, 0.1);

		lastPosition = adapter.getCharacterPosition(1);
		adapter.handleKeyDownPlayer2(100, leftTwo);
		newPosition = adapter.getCharacterPosition(1);

		assertEquals(lastPosition.x - 10, newPosition.x, 0.1);

	}

	@Test
	public void testWalkUp() {

		int upOne = player1KeyConfig[2];
		int upTwo = player2KeyConfig[2];
		
		Vector2f lastPosition = adapter.getCharacterPosition(0);
		adapter.handleKeyDownPlayer1(100, upOne);
		Vector2f newPosition = adapter.getCharacterPosition(0);

		assertEquals(lastPosition.y - 10, newPosition.y, 0.1);

		lastPosition = adapter.getCharacterPosition(1);
		adapter.handleKeyDownPlayer2(100, upTwo);
		newPosition = adapter.getCharacterPosition(1);

		assertEquals(lastPosition.y - 10, newPosition.y, 0.1);


	}

	@Test
	public void testWalkDown() {

		int downOne = player1KeyConfig[3];
		int downTwo = player2KeyConfig[3];

		Vector2f lastPosition = adapter.getCharacterPosition(0);
		adapter.handleKeyDownPlayer1(100, downOne);
		Vector2f newPosition = adapter.getCharacterPosition(0);

		assertEquals(lastPosition.y + 10, newPosition.y, 0.1);

		lastPosition = adapter.getCharacterPosition(1);
		adapter.handleKeyDownPlayer2(100, downTwo);
		newPosition = adapter.getCharacterPosition(1);

		assertEquals(lastPosition.y + 10, newPosition.y, 0.1);

	}

	@Test
	public void testRunRight() {

		int rightOne = player1KeyConfig[0];
		int runOne = player1KeyConfig[4];

		int rightTwo = player2KeyConfig[0];
		int runTwo = player2KeyConfig[4];

		Vector2f lastPosition = adapter.getCharacterPosition(0);
		adapter.handleKeyDownPlayer1(100, rightOne, runOne);
		Vector2f newPosition = adapter.getCharacterPosition(0);

		assertEquals(lastPosition.x + 15, newPosition.x, 0.1);

		lastPosition = adapter.getCharacterPosition(1);
		adapter.handleKeyDownPlayer1(100, rightTwo, runTwo);
		newPosition = adapter.getCharacterPosition(1);

		assertEquals(lastPosition.x + 15, newPosition.x, 0.1);


	}

	@Test
	public void testRunLeft() {

		int leftOne = player1KeyConfig[1];
		int runOne = player1KeyConfig[4];

		int leftTwo = player2KeyConfig[1];
		int runTwo = player2KeyConfig[4];
		
		Vector2f lastPosition = adapter.getCharacterPosition(0);
		adapter.handleKeyDownPlayer1(100, leftOne, runOne);
		Vector2f newPosition = adapter.getCharacterPosition(0);

		assertEquals(lastPosition.x - 15, newPosition.x, 0.1);

		lastPosition = adapter.getCharacterPosition(1);
		adapter.handleKeyDownPlayer1(100, leftTwo, runTwo);
		newPosition = adapter.getCharacterPosition(1);

		assertEquals(lastPosition.x - 15, newPosition.x, 0.1);


	}

	@Test
	public void testRunUp() {
		int upOne = player1KeyConfig[2];
		int runOne = player1KeyConfig[4];

		int upTwo = player2KeyConfig[2];
		int runTwo = player2KeyConfig[4];

		Vector2f lastPosition = adapter.getCharacterPosition(0);
		adapter.handleKeyDownPlayer1(100, upOne, runOne);
		Vector2f newPosition = adapter.getCharacterPosition(0);

		assertEquals(lastPosition.y - 15, newPosition.y, 0.1);

		lastPosition = adapter.getCharacterPosition(1);
		adapter.handleKeyDownPlayer1(100, upTwo, runTwo);
		newPosition = adapter.getCharacterPosition(1);

		assertEquals(lastPosition.y - 15, newPosition.y, 0.1);

	}

	@Test
	public void testRunDown() {
		int downOne = player1KeyConfig[3];
		int runOne = player1KeyConfig[4];

		int downTwo = player2KeyConfig[3];
		int runTwo = player2KeyConfig[4];

		Vector2f lastPosition = adapter.getCharacterPosition(0);
		adapter.handleKeyDownPlayer1(100, downOne, runOne);
		Vector2f newPosition = adapter.getCharacterPosition(0);

		assertEquals(lastPosition.y + 15, newPosition.y, 0.1);

		lastPosition = adapter.getCharacterPosition(1);
		adapter.handleKeyDownPlayer1(100, downTwo, runTwo);
		newPosition = adapter.getCharacterPosition(1);

		assertEquals(lastPosition.y + 15, newPosition.y, 0.1);

	}

	@Test
	public void testWallCollisions() {
		int rightOne = player1KeyConfig[0];
		int leftOne = player1KeyConfig[1];
		int upOne = player1KeyConfig[2];
		int downOne = player1KeyConfig[3];
		int runOne = player1KeyConfig[4];

		int rightTwo = player2KeyConfig[0];
		int leftTwo = player2KeyConfig[1];
		int upTwo = player2KeyConfig[2];
		int downTwo = player2KeyConfig[3];
		int runTwo = player2KeyConfig[4];

		Wall wallNorth = adapter.getWall(0);
		Wall wallSouth = adapter.getWall(2);
		Wall wallEast = adapter.getWall(1);
		Wall wallWest = adapter.getWall(3);
		// ---------------------------------------------------------------------------------------
		float eastThreshold = wallEast.getPosition().getX() - wallEast.getSize().getX() / 2;

		Vector2f startingPositionOne = adapter.getCharacterPosition(0);
		Vector2f startingPositionTwo = adapter.getCharacterPosition(1);

		float expectedPosition = eastThreshold - adapter.getCharacter(0).getSize().getX() / 2;

		// move character next to the east wall
		adapter.getCharacter(0).setPosition(new Vector2f(expectedPosition, startingPositionOne.getY()));
		adapter.getCharacter(1).setPosition(new Vector2f(expectedPosition, startingPositionTwo.getY()));

		// try to move beyond the wall
		for (int i = 0; i < 10; i++) {
			adapter.handleKeyDownPlayer1(1, rightOne);
			adapter.handleKeyDownPlayer1(1, rightOne, runOne);

			adapter.handleKeyDownPlayer2(1, rightTwo);
			adapter.handleKeyDownPlayer2(1, rightTwo, runTwo);
		}

		Vector2f newPositionOne = adapter.getCharacterPosition(0);
		Vector2f newPositionTwo = adapter.getCharacterPosition(1);

		assertEquals(expectedPosition, newPositionOne.getX(), 0.1);
		assertEquals(expectedPosition, newPositionTwo.getX(), 0.1);

		// ---------------------------------------------------------------------------------------
		float westThreshold = wallWest.getPosition().getX() + wallWest.getSize().getX() / 2;

		expectedPosition = westThreshold + adapter.getCharacter(0).getSize().getX() / 2;

		// move character next to the west wall
		adapter.getCharacter(0).setPosition(new Vector2f(expectedPosition, startingPositionOne.getY()));
		adapter.getCharacter(1).setPosition(new Vector2f(expectedPosition, startingPositionTwo.getY()));

		// try to move beyond the wall
		for (int i = 0; i < 10; i++) {
			adapter.handleKeyDownPlayer1(1, leftOne);
			adapter.handleKeyDownPlayer1(1, leftOne, runOne);

			adapter.handleKeyDownPlayer2(1, leftTwo);
			adapter.handleKeyDownPlayer2(1, leftTwo, runTwo);
		}

		newPositionOne = adapter.getCharacterPosition(0);
		newPositionTwo = adapter.getCharacterPosition(1);

		assertEquals(expectedPosition, newPositionOne.getX(), 0.1);
		assertEquals(expectedPosition, newPositionTwo.getX(), 0.1);

		// ---------------------------------------------------------------------------------------
		float northThreshold = wallNorth.getPosition().getY() + wallNorth.getSize().getY() / 2;

		expectedPosition = northThreshold + adapter.getCharacter(0).getSize().getY() / 2;

		// move character next to north wall
		adapter.getCharacter(0).setPosition(new Vector2f(startingPositionOne.getX(), expectedPosition));
		adapter.getCharacter(1).setPosition(new Vector2f(startingPositionTwo.getX(), expectedPosition));

		// try to move beyond the wall
		for (int i = 0; i < 10; i++) {
			adapter.handleKeyDownPlayer1(1, upOne);
			adapter.handleKeyDownPlayer1(1, upOne, runOne);

			adapter.handleKeyDownPlayer2(1, upTwo);
			adapter.handleKeyDownPlayer2(1, upTwo, runTwo);
		}

		newPositionOne = adapter.getCharacterPosition(0);
		newPositionTwo = adapter.getCharacterPosition(1);

		assertEquals(expectedPosition, newPositionOne.getY(), 0.1);
		assertEquals(expectedPosition, newPositionTwo.getY(), 0.1);

		// ---------------------------------------------------------------------------------------
		float southThreshold = wallSouth.getPosition().getY() - 1 - wallSouth.getSize().getY() / 2;

		expectedPosition = southThreshold - adapter.getCharacter(0).getSize().getY() / 2;
		// move character next to north wall
		adapter.getCharacter(0).setPosition(new Vector2f(startingPositionOne.getX(), expectedPosition));
		adapter.getCharacter(1).setPosition(new Vector2f(startingPositionTwo.getX(), expectedPosition));

		// try to move beyond the wall
		for (int i = 0; i < 10; i++) {
			adapter.handleKeyDownPlayer1(1, downOne);
			adapter.handleKeyDownPlayer1(1, downOne, runOne);

			adapter.handleKeyDownPlayer2(1, downTwo);
			adapter.handleKeyDownPlayer2(1, downTwo, runTwo);
		}

		newPositionOne = adapter.getCharacterPosition(0);
		newPositionTwo = adapter.getCharacterPosition(1);

		assertEquals(expectedPosition, newPositionOne.getY(), 0.1);
		assertEquals(expectedPosition, newPositionTwo.getY(), 0.1);

	}

	@Test
	public void testBasicJump() throws SlickException {
		int jumpOne = player1KeyConfig[6];

		float jumpHeight = 80;
		float jumpTime = 1000;
		float castingTime = 200;
		float jumpLength = 160;

		Vector2f characterPosition = adapter.getCharacterPosition(0);
		Vector2f startingPosition = characterPosition.copy();

		adapter.handleKeyDownPlayer1(0, jumpOne);

		for (int i = 0; i < 20; i++) {
			int delta = (i + 1) * 50;
			adapter.updateGame(50);
			characterPosition = adapter.getCharacterPosition(0);
			if (delta <= castingTime || delta > (castingTime + jumpTime)) {
				assertEquals(startingPosition.y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);
			} else {
				float y = adapter.jumpFunction(delta - 225, 0, jumpHeight, jumpTime);
				assertEquals(startingPosition.y - y, characterPosition.y, 20); // movement in the y axis
				assertEquals(startingPosition.x, characterPosition.x, 0.1); // movement in the x axis
			}
		}
	}

	@Test
	public void testRightJump() throws SlickException {
		int rightOne = player1KeyConfig[0];
		int jumpOne = player1KeyConfig[6];
		float jumpHeight = 80;
		float jumpTime = 1000;
		float castingTime = 200;
		float jumpLength = 160;

		Vector2f characterPosition = adapter.getCharacterPosition(0);
		Vector2f startingPosition = characterPosition.copy();

		adapter.handleKeyDownPlayer1(0, jumpOne, rightOne);
		adapter.getOurTestInput().clearDownKeys();

		// jump positions tested for 20 frames
		for (int i = 0; i < 20; i++) {
			int delta = (i + 1) * 50;
			adapter.updateGame(50);
			characterPosition = adapter.getCharacterPosition(0);
			if (delta <= castingTime) { // during casting time, the position should not be modified
				assertEquals(startingPosition.y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);

			} else if (delta > castingTime && delta <= (jumpTime + castingTime)) {
				float x = delta - castingTime - 50 / 2;
				float y = adapter.jumpFunction(x, 0, jumpHeight, jumpTime);
				assertEquals(startingPosition.y - y, characterPosition.y, 10);
				assertEquals(startingPosition.x + x * (jumpLength / jumpTime), characterPosition.x, 20);
			} else {
				assertEquals(startingPosition.y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x + jumpLength, characterPosition.x, 0.1);
			}
		}
	}

	@Test
	public void testLefttJump() throws SlickException {
		int leftOne = player1KeyConfig[1];
		int jumpOne = player1KeyConfig[6];
		float jumpHeight = 80;
		float jumpTime = 1000;
		float castingTime = 200;
		float jumpLength = 160;

		Vector2f characterPosition = adapter.getCharacterPosition(0);
		Vector2f startingPosition = characterPosition.copy();

		adapter.handleKeyDownPlayer1(0, jumpOne, leftOne);
		adapter.getOurTestInput().clearDownKeys();

		// jump positions tested for 20 frames
		for (int i = 0; i < 20; i++) {
			int delta = (i + 1) * 50;
			adapter.updateGame(50);
			characterPosition = adapter.getCharacterPosition(0);
			if (delta <= castingTime) { // during casting time, the position should not be modified
				assertEquals(startingPosition.y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);

			} else if (delta > castingTime && delta <= (jumpTime + castingTime)) {
				float x = delta - castingTime - 50 / 2;
				float y = adapter.jumpFunction(x, 0, jumpHeight, jumpTime);
				assertEquals(startingPosition.y - y, characterPosition.y, 10);
				assertEquals(startingPosition.x - x * (jumpLength / jumpTime), characterPosition.x, 20);
			} else {
				assertEquals(startingPosition.y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x - jumpLength, characterPosition.x, 0.1);
			}
		}
	}

	@Test
	public void testUpJump() throws SlickException {
		int upOne = player1KeyConfig[2];
		int jumpOne = player1KeyConfig[6];
		float jumpHeight = 80;
		float jumpTime = 1000;
		float castingTime = 200;
		float jumpLength = 160;
		float offset = 50;

		Vector2f characterPosition = adapter.getCharacterPosition(0);

		adapter.getCharacter(0).setPosition(new Vector2f(characterPosition.getX(), characterPosition.getY() + 50));

		characterPosition = adapter.getCharacterPosition(0);
		Vector2f startingPosition = characterPosition.copy();

		adapter.handleKeyDownPlayer1(0, jumpOne, upOne);
		adapter.getOurTestInput().clearDownKeys();

		// jump positions tested for 20 frames
		for (int i = 0; i < 20; i++) {
			int delta = (i + 1) * 50;
			adapter.updateGame(50);
			characterPosition = adapter.getCharacterPosition(0);
			if (delta <= castingTime) { // during casting time, the position should not be modified
				assertEquals(startingPosition.y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);
			} else if (delta > castingTime && delta <= (jumpTime + castingTime)) {
				float x = delta - castingTime - 50 / 2;
				float y = adapter.jumpFunction(x, offset, jumpHeight, jumpTime);
				assertEquals(startingPosition.y - y, characterPosition.y, 20);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);
			} else {
				assertEquals(startingPosition.y - offset, characterPosition.y, 0.1);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);
			}
		}
	}

	@Test
	public void testDownJump() throws SlickException {
		int downOne = player1KeyConfig[3];
		int jumpOne = player1KeyConfig[6];
		float jumpHeight = 80;
		float jumpTime = 1000;
		float castingTime = 200;
		float jumpLength = 160;
		float offset = -50;
		
		Vector2f characterPosition = adapter.getCharacterPosition(0);

		adapter.getCharacter(0).setPosition(new Vector2f(characterPosition.getX(), characterPosition.getY() - 50));

		characterPosition = adapter.getCharacterPosition(0);
		Vector2f startingPosition = characterPosition.copy();

		adapter.handleKeyDownPlayer1(0, jumpOne, downOne);
		adapter.getOurTestInput().clearDownKeys();

		// jump positions tested for 20 frames
		for (int i = 0; i < 20; i++) {
			int delta = (i + 1) * 50;
			adapter.updateGame(50);
			characterPosition = adapter.getCharacterPosition(0);
			if (delta <= castingTime) { // during casting time, the position should not be modified
				assertEquals(startingPosition.y, characterPosition.y, 0.1);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);
			} else if (delta > castingTime && delta <= (jumpTime + castingTime)) {
				float x = delta - castingTime - 50 / 2;
				float y = adapter.jumpFunction(x, offset, jumpHeight, jumpTime);
				assertEquals(startingPosition.y - y, characterPosition.y, 20);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);
			} else {
				assertEquals(startingPosition.y - offset, characterPosition.y, 0.1);
				assertEquals(startingPosition.x, characterPosition.x, 0.1);
			}
		}
	}

	@Test
	public void testPunch() throws SlickException {
		int attackKeyPlayer1 = player1KeyConfig[5];
		int attackKeyPlayer2 = player2KeyConfig[5];
		
		// Test regular punching
		adapter.handleKeyPressedPlayer1(1, attackKeyPlayer1);
		assertEquals(true, adapter.isPunching(0));
		adapter.handleKeyPressedPlayer2(1, attackKeyPlayer2);
		assertEquals(true, adapter.isPunching(1));
		adapter.updateFrames(50);
		
		//test if hp decreases after punching
		
		adapter.setPositionofCharacter(0, 350, 350);
		adapter.setPositionofCharacter(1, 350, 350);
		adapter.handleKeyPressedPlayer2(1, attackKeyPlayer2);
		assertTrue(adapter.getCharacterActualLife(0) < 100);
		
		adapter.updateFrames(40);
		
		adapter.handleKeyPressedPlayer1(0, attackKeyPlayer1);
		assertTrue(adapter.getCharacterActualLife(1) < 100);
		
	}
	
	@Test
	public void testShield() throws SlickException {
		
	
		
		int defendKeyPlayer1 = player1KeyConfig[7];
		int defendKeyPlayer2 = player2KeyConfig[7];
		int attackKeyPlayer1 = player1KeyConfig[5];
		
		// Test regular shielding
		adapter.handleKeyPressedPlayer1(1, defendKeyPlayer1);
		assertEquals(true, adapter.isShielding(0));
		adapter.handleKeyPressedPlayer2(1, defendKeyPlayer2);
		assertEquals(true, adapter.isShielding(1));
		adapter.updateFrames(50);
		
		//Test if shield blocks attacks
		adapter.setPositionofCharacter(0, 350, 350);
		adapter.setPositionofCharacter(1, 350, 350);
		adapter.handleKeyPressedPlayer2(1, defendKeyPlayer2);
		adapter.handleKeyPressedPlayer1(0, attackKeyPlayer1);
		assertEquals(adapter.getCharacterActualLife(1), 100);
		
		
	}
	
	@Test
	public void testNewHitDetection() throws SlickException {
		int attackKeyPlayer1 = player1KeyConfig[5];
		
		adapter.setPositionofCharacter(0, 350, 329);
		adapter.setPositionofCharacter(1, 350, 350);
		
		adapter.handleKeyPressedPlayer1(1, attackKeyPlayer1);
		assertEquals(adapter.getCharacterActualLife(1), 100);
		
		adapter.setPositionofCharacter(0, 350, 371);
		
		adapter.handleKeyPressedPlayer1(1, attackKeyPlayer1);
		assertEquals(adapter.getCharacterActualLife(1), 100);
		
		adapter.setPositionofCharacter(0, 350, 370);
		adapter.handleKeyPressedPlayer1(1, attackKeyPlayer1);
		assertEquals(90, adapter.getCharacterActualLife(1));
	
		adapter.updateFrames(50);
		
		adapter.setPositionofCharacter(0, 350, 330);
		adapter.handleKeyPressedPlayer1(1, attackKeyPlayer1);
		assertEquals(80, adapter.getCharacterActualLife(1));
		
	}
	
	@Test
	public void testGameEnding() throws SlickException {
		
		adapter.decreaseCharacterLife(0, 1000);
		
		adapter.updateFrames(100000);
		
		assertEquals(0, adapter.getStateBasedGame().getCurrentStateID());
	}

}
