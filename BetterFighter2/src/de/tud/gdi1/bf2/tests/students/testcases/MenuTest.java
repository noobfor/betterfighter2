package de.tud.gdi1.bf2.tests.students.testcases;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.newdawn.slick.Input;

import de.tud.gdi1.bf2.model.entities.characters.BudsyCharacter;
import de.tud.gdi1.bf2.model.entities.characters.NoobforCharacter;
import de.tud.gdi1.bf2.tests.adapter.BetterFighter2TestAdapterMinimal;
import de.tud.gdi1.bf2.ui.BetterFighter2;

public class MenuTest {

	BetterFighter2TestAdapterMinimal adapter;
	
	@Before
	public void setUp() {
		adapter = new BetterFighter2TestAdapterMinimal();
	}
	
	@After
	public void finish() {
		adapter.stopGame();
	}
	
	@Test
	public void testChangeIntoCharacterSelect() throws Exception {
		adapter.initializeGame();
		assertTrue(adapter.getStateBasedGame().getCurrentStateID()==BetterFighter2.MAINMENU_STATE);

		adapter.handleMousePosition(0, adapter.getStartButtonXPosition(), adapter.getStartButtonYPosition());
		adapter.handleMouseClicked(0);
		assertTrue(adapter.getStateBasedGame().getCurrentStateID()==BetterFighter2.CHARACTER_SELECTION_STATE);
		//adapter.handleKeyPressed(0, Input.KEY_ESCAPE);
	}
	
	@Test
	public void testCharacterSelect() throws Exception {
		adapter.initializeGame();
		adapter.handleMousePosition(0, adapter.getStartButtonXPosition(), adapter.getStartButtonYPosition());
		adapter.handleMouseClicked(0);
		adapter.handleKeyPressedPlayer1(0, Input.KEY_D);
		adapter.handleKeyPressedPlayer2(0, Input.KEY_LEFT);
		
		adapter.handleSpaceKeyPressed(0);
		
		assertTrue(adapter.getStateBasedGame().getCurrentStateID()==BetterFighter2.GAMEPLAY_STATE);
		
		// TODO: change to characternames of your implementation, 
		// for example 1 or 2 instead of noobfor and budsy to test for right characterselection
		assertTrue(adapter.getCharacter(0) instanceof NoobforCharacter);
		assertTrue(adapter.getCharacter(1) instanceof BudsyCharacter);
		
	}
	

	

	
}
