package de.tud.gdi1.bf2.tests.students.suites;

import de.tud.gdi1.bf2.tests.students.testcases.BasicActionsTest;
import de.tud.gdi1.bf2.tests.students.testcases.MenuTest;
import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.framework.TestSuite;

public class BetterFighter2TestsuiteMinimal {

public static Test suite() {
		
		TestSuite suite = new TestSuite("Student tests for Betterfighter2 - Minimal");
		suite.addTest(new JUnit4TestAdapter(MenuTest.class));
		suite.addTest(new JUnit4TestAdapter(BasicActionsTest.class));
		return suite;
	}
}
