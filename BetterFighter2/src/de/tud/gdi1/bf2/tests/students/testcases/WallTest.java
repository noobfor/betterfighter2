package de.tud.gdi1.bf2.tests.students.testcases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.entities.map.Wall;
import de.tud.gdi1.bf2.tests.adapter.BetterFighter2TestAdapterMinimal;
import de.tud.gdi1.bf2.ui.BetterFighter2;

public class WallTest {

	BetterFighter2TestAdapterMinimal adapter;

	@Before
	public void setUp() throws Exception {
		adapter = new BetterFighter2TestAdapterMinimal();
		adapter.initializeGame();
		adapter.handleMousePosition(0, adapter.getStartButtonXPosition(), adapter.getStartButtonYPosition());
		adapter.handleMouseClicked(0);
		assertEquals(BetterFighter2.CHARACTER_SELECTION_STATE, adapter.getStateBasedGame().getCurrentStateID());
		adapter.handleSpaceKeyPressed(0);
		assertEquals(BetterFighter2.GAMEPLAY_STATE, adapter.getStateBasedGame().getCurrentStateID());
	}

	@After
	public void finish() {
		adapter.stopGame();
	}

	@Test
	public void testWallExistence() {
		Wall[] walls = adapter.getWalls();
		assertEquals(4, walls.length);
	}

	@Test
	public void testWallPositions() {
		// walls should not be places outside of the screen
		Wall[] walls = adapter.getWalls();
		int height = 600;
		int width = 800;

		int[][] bounds = new int[4][4];

		int i = 0;
		for (Wall wall : walls) {
			Vector2f wallPosition = wall.getPosition();
			Vector2f wallSize = wall.getSize();

			float leftWallBound = wallPosition.x - wallSize.x / 2;
			float rightWallBound = wallPosition.x + wallSize.x / 2;
			float northWallBound = wallPosition.y - wallSize.y / 2;
			float southWallBound = wallPosition.y + wallSize.y / 2;

			bounds[i][0] = (int) leftWallBound;
			bounds[i][1] = (int) rightWallBound;
			bounds[i][2] = (int) northWallBound;
			bounds[i][3] = (int) southWallBound;

			// System.out.println(leftWallBound + ", " + rightWallBound + ", " +
			// northWallBound + ", " + southWallBound);

			assertTrue(0 <= leftWallBound && leftWallBound <= width && 0 <= rightWallBound && rightWallBound <= width);
			assertTrue(leftWallBound < rightWallBound);

			assertTrue(
					0 <= northWallBound && northWallBound <= height && 0 <= southWallBound && southWallBound <= height);
			assertTrue(northWallBound < southWallBound);
			i++;
		}

		// find the north and south wall (both have the same width)
		int northWallSouthBound = 0;
		int southWallNorthBound = 0;

		boolean southFound = false;
		for (int j = 0; j < bounds.length; j++) {
			int left = bounds[j][0];
			int right = bounds[j][1];
			northWallSouthBound = bounds[j][3];
			int tmp = bounds[j][2];
			for (int k = 0; k < bounds.length; k++) {
				if (k == j) {
					continue;
				} else if (left == bounds[k][0] && right == bounds[k][1]) {
					southWallNorthBound = bounds[k][2];
					southFound = tmp <= southWallNorthBound;
					break;

				}
			}
			if (southFound) {
				break;
			}
		}
		// Test if the southern bound of the northern wall is above the northern bound
		// of the southern wall
		assertTrue(northWallSouthBound < southWallNorthBound);

		// same for east and west walls
		int eastWallWestBound = 0;
		int westWallEastBound = 0;

		boolean westFound = false;
		for (int j = 0; j < bounds.length; j++) {
			int up = bounds[j][2];
			int down = bounds[j][3];
			eastWallWestBound = bounds[j][0];
			for (int k = 0; k < bounds.length; k++) {
				if (k == j) {
					continue;
				} else if (up == bounds[k][2] && down == bounds[j][3]) {
					westWallEastBound = bounds[k][1];

					westFound = bounds[k][0] < eastWallWestBound;
					break;
				}
			}
			if (westFound) {
				break;
			}
		}
		assertTrue(westWallEastBound < eastWallWestBound);

		// the characters should be within this space
		for (int j = 0; j < 2; j++) {
			Vector2f characterPos = adapter.getCharacter(j).getPosition();
			assertTrue(westWallEastBound < characterPos.x && characterPos.x < eastWallWestBound);
			assertTrue(northWallSouthBound < characterPos.y && characterPos.y < southWallNorthBound);
		}

	}
}
