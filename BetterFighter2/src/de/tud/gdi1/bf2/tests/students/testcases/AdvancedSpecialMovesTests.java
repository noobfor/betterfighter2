package de.tud.gdi1.bf2.tests.students.testcases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.entities.characters.BudsyCharacter;
import de.tud.gdi1.bf2.model.entities.characters.NoobforCharacter;
import de.tud.gdi1.bf2.tests.adapter.BetterFighter2TestAdapterExtended2;
import de.tud.gdi1.bf2.ui.BetterFighter2;

public class AdvancedSpecialMovesTests {

	BetterFighter2TestAdapterExtended2 adapter;
	int[] player1KeyConfig;
	int[] player2KeyConfig;

	@Before
	public void setUp() throws Exception {
		adapter = new BetterFighter2TestAdapterExtended2();
		adapter.initializeGame();

		player1KeyConfig = adapter.getPlayers().get(0).getKeyConfig();
		player2KeyConfig = adapter.getPlayers().get(1).getKeyConfig();

		adapter.handleMousePosition(0, adapter.getStartButtonXPosition(), adapter.getStartButtonYPosition());
		adapter.handleMouseClicked(0);

		// go to character selection state
		assertEquals(BetterFighter2.CHARACTER_SELECTION_STATE, adapter.getStateBasedGame().getCurrentStateID());
		adapter.playerSelectCharacter(0, 0);
        adapter.playerSelectCharacter(1, 1);
        adapter.handleSpaceKeyPressed(0);
        adapter.getOurTestInput().clearPressedKeys();

        // player 1 picks FireCharacter
        assertTrue(adapter.isPlayer1FireCharacter());
        // player 2 picks IceCharacter
        assertTrue(adapter.isPlayer2IceCharacter());
		// go to gameplay state
		assertEquals(BetterFighter2.GAMEPLAY_STATE, adapter.getStateBasedGame().getCurrentStateID());

		adapter.getOurTestInput().clearDownKeys();
	}

	@After
	public void finish() {
		adapter.stopGame();
	}

	@Test
	public void testIcewall() throws SlickException {

		int shieldTwo = player2KeyConfig[7];
		int righTwo = player2KeyConfig[0];
		int jumpTwo = player2KeyConfig[6];

		adapter.handleKeyPressedPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, righTwo);
		adapter.handleKeyDownPlayer2(0, jumpTwo);
		adapter.getOurTestInput().clearDownKeys();

		for (int i = 0; i < 10; i++) {
			adapter.updateFrames(1);
			int iceWallCount = adapter.getIceWalls().size();
			assertTrue(iceWallCount == 1 && i < 4 || iceWallCount == 2 && i >= 4 && i < 9 || iceWallCount == 3);
		}

		List<Vector2f> iceWallPositions = adapter.getIceWallPositions();
		List<Vector2f> iceWallSizes = adapter.getIceWallSizes();

		// all icewalls have the same size
		for (Vector2f vector2f : iceWallSizes) {
			assertEquals(50, vector2f.x, 0.1);
			assertEquals(70, vector2f.y, 0.1);
		}
		// all icewalls have a distance of 55 between each other
		assertTrue(iceWallPositions.get(0).x == iceWallPositions.get(1).x - 55
				&& iceWallPositions.get(1).x - 55 == iceWallPositions.get(2).x - 110);

	}

	@Test
	public void testFirebreath() throws SlickException {
		int rightOne = player1KeyConfig[0];
		int leftOne = player1KeyConfig[1];
		int upOne = player1KeyConfig[2];
		int downOne = player1KeyConfig[3];
		int jumpOne = player1KeyConfig[6];
		int shieldOne = player1KeyConfig[7];

		Vector2f player2Position = adapter.getCharacterPosition(1);
		adapter.setPositionofCharacter(0, player2Position.x - 60, player2Position.y);

		adapter.handleKeyPressedPlayer2(0, shieldOne);
		adapter.handleKeyDownPlayer2(0, shieldOne);
		adapter.handleKeyDownPlayer2(0, downOne);
		adapter.handleKeyDownPlayer2(0, jumpOne);
		adapter.getOurTestInput().clearDownKeys();

		// test if character was knocked off
		assertTrue(adapter.isFireballKnockback(1));
		// test dmg
		assertEquals(adapter.getCharacterMaxLife(1) - 10, adapter.getCharacterActualLife(1));

		adapter.updateFrames(50);
		// test if movement disabled while using firebreath
		Vector2f lastPosition = adapter.getCharacterPosition(0);
		adapter.handleKeyDownPlayer1(100, rightOne);
		Vector2f currentPosition = adapter.getCharacterPosition(0);
		assertEquals(lastPosition.x, currentPosition.x, 0);
		assertEquals(lastPosition.y, currentPosition.y, 0);
		adapter.handleKeyDownPlayer1(100, leftOne);
		currentPosition = adapter.getCharacterPosition(0);
		assertEquals(lastPosition.x, currentPosition.x, 0);
		assertEquals(lastPosition.y, currentPosition.y, 0);
		adapter.handleKeyDownPlayer1(100, upOne);
		currentPosition = adapter.getCharacterPosition(0);
		assertEquals(lastPosition.x, currentPosition.x, 0);
		assertEquals(lastPosition.y, currentPosition.y, 0);
		adapter.handleKeyDownPlayer1(100, downOne);
		currentPosition = adapter.getCharacterPosition(0);
		assertEquals(lastPosition.x, currentPosition.x, 0);
		assertEquals(lastPosition.y, currentPosition.y, 0);

	}

	@Test
	public void testSprintattack() {

		int rightOne = player1KeyConfig[0];
		int runOne = player1KeyConfig[4];
		int attackOne = player1KeyConfig[5];

		Vector2f enemyPosition = adapter.getCharacterPosition(1);
		// set to the left of the enemy
		adapter.setPositionofCharacter(0, enemyPosition.x - 30, enemyPosition.y);
		adapter.handleKeyDownPlayer1(0, rightOne, runOne, attackOne);
		adapter.getOurTestInput().clearDownKeys();

		// test if damage taken
		assertEquals(adapter.getCharacterMaxLife(1) - 15, adapter.getCharacterActualLife(1), 0);
		// and test strong knockback
		assertTrue(adapter.isFireballKnockback(1));
	}

}
