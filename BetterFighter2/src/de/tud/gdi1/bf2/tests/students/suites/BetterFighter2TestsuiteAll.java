package de.tud.gdi1.bf2.tests.students.suites;

import de.tud.gdi1.bf2.tests.tutors.suites.BetterFighter2TestsuiteExtended2;
import de.tud.gdi1.bf2.tests.tutors.suites.BetterFighter2TestsuiteExtended3;
import junit.framework.Test;
import junit.framework.TestSuite;

public class BetterFighter2TestsuiteAll {
	
	public static Test suite() {
		
		TestSuite suite = new TestSuite("All student tests for Tanks");
		
		suite.addTest(BetterFighter2TestsuiteMinimal.suite());
		suite.addTest(BetterFighter2TestsuiteExtended1.suite());
		suite.addTest(BetterFighter2TestsuiteExtended2.suite());
		suite.addTest(BetterFighter2TestsuiteExtended3.suite());
		return suite;
	}
}
