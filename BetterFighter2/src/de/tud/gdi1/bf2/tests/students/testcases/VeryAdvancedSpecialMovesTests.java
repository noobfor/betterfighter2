package de.tud.gdi1.bf2.tests.students.testcases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.newdawn.slick.SlickException;

import de.tud.gdi1.bf2.tests.adapter.BetterFighter2TestAdapterExtended2;
import de.tud.gdi1.bf2.tests.adapter.BetterFighter2TestAdapterExtended3;
import de.tud.gdi1.bf2.ui.BetterFighter2;

public class VeryAdvancedSpecialMovesTests {

	BetterFighter2TestAdapterExtended3 adapter;
	int[] player1KeyConfig;
	int[] player2KeyConfig;
	
	
	@Before
	public void setUp() throws Exception {
		adapter = new BetterFighter2TestAdapterExtended3();
		adapter.initializeGame();
		
		player1KeyConfig = adapter.getPlayers().get(0).getKeyConfig();
		player2KeyConfig = adapter.getPlayers().get(1).getKeyConfig();

		adapter.handleMousePosition(0, adapter.getStartButtonXPosition(), adapter.getStartButtonYPosition());
		adapter.handleMouseClicked(0);
		
		// go to character selection state
		assertEquals(BetterFighter2.CHARACTER_SELECTION_STATE, adapter.getStateBasedGame().getCurrentStateID());
		adapter.playerSelectCharacter(0, 0);
        adapter.playerSelectCharacter(1, 1);
        adapter.handleSpaceKeyPressed(0);
        adapter.getOurTestInput().clearPressedKeys();

        // player 1 picks FireCharacter
        assertTrue(adapter.isPlayer1FireCharacter());
        // player 2 picks IceCharacter
        assertTrue(adapter.isPlayer2IceCharacter());
		// go to gameplay state
		assertEquals(BetterFighter2.GAMEPLAY_STATE, adapter.getStateBasedGame().getCurrentStateID());

		adapter.getOurTestInput().clearDownKeys();
	}
	
	@After
	public void finish() {
		adapter.stopGame();
	}
	
	@Test
	public void testIcerain() throws SlickException {
		
		int shieldTwo = player2KeyConfig[7];
		int upTwo = player2KeyConfig[2];
		int attackTwo = player2KeyConfig[5];

		adapter.setPositionofCharacter(1, 300, 300);
		adapter.setPositionofCharacter(0, 340, 300);
		
		adapter.handleKeyPressedPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, shieldTwo);
		adapter.handleKeyDownPlayer2(0, upTwo);
		adapter.handleKeyDownPlayer2(0, attackTwo);
		
		adapter.getOurTestInput().clearDownKeys();
		adapter.getOurTestInput().clearPressedKeys();
		
		
		assertEquals(adapter.getCharacterYPosition(1) - 60,adapter.getIcedropYPosition(0), 5);
		
		// should hit even if player 1 moves
		adapter.handleKeyDownPlayer1(0, player1KeyConfig[0]);
		
		adapter.updateFrames(150);
		
		assertTrue(adapter.getCharacterActualLife(0) < 100);
		assertTrue(adapter.isFreezed(0));
		

		assertTrue(adapter.getCharacterActualMana(0) < 100);
		
		}
	
	@Test
	public void testFireexplosion() throws SlickException {
		
		int shieldOne = player1KeyConfig[7];
		int upOne = player1KeyConfig[2];
		int jumpOne = player1KeyConfig[6];
		
		adapter.setPositionofCharacter(0, 300, 400);
		adapter.setPositionofCharacter(1, 350, 400);
		adapter.setDirectionofCharacter(0, true);
		
		adapter.handleKeyPressedPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, shieldOne);
		adapter.handleKeyDownPlayer1(0, upOne);
		adapter.handleKeyDownPlayer1(0, jumpOne);
		
		adapter.getOurTestInput().clearDownKeys();
		adapter.getOurTestInput().clearPressedKeys();
		
		float xPositionBefore = adapter.getCharacterXPosition(0);
		adapter.handleKeyDownPlayer1(10, player1KeyConfig[1]);
		float xPositionAfter = adapter.getCharacterXPosition(0);
		
		assertEquals(xPositionBefore, xPositionAfter, 0);

		assertTrue(adapter.getCharacterActualMana(1) < 100);
		assertTrue(adapter.getCharacterActualLife(1) < 100);
		assertTrue(adapter.isExplosionKnockback(1));
		
		
	}
	
}
