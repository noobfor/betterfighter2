package de.tud.gdi1.bf2.tests.students.suites;

import de.tud.gdi1.bf2.tests.students.testcases.VeryAdvancedSpecialMovesTests;
import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.framework.TestSuite;

public class Betterfighter2TestsuiteExtended3 {
	
	public static Test suite() {
		
		TestSuite suite = new TestSuite("Student tests for Betterfighter2 - Extended 3");
		suite.addTest(new JUnit4TestAdapter(VeryAdvancedSpecialMovesTests.class));
		return suite;
	}
	
}
