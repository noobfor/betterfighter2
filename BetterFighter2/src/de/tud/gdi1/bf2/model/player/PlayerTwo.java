package de.tud.gdi1.bf2.model.player;

import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Vector2f;

public class PlayerTwo extends Player {

	/**
	 * custom key configurations for player two can be customized here
	 * @param name player name
	 */
	public PlayerTwo(String name) {
		super(name);
		int[] keyConfig = { Input.KEY_RIGHT, Input.KEY_LEFT, Input.KEY_UP, Input.KEY_DOWN, Input.KEY_V, Input.KEY_B,
				Input.KEY_N, Input.KEY_M };
		this.setKeyConfig(keyConfig);
		this.setPosition(new Vector2f(500, 350));
	}

}
