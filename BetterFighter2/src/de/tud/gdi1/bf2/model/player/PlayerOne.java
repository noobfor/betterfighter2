package de.tud.gdi1.bf2.model.player;

import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Vector2f;

public class PlayerOne extends Player {

	/**
	 * custom key configurations for player one can be customized here
	 * @param name player name
	 */
	public PlayerOne(String name) {
		super(name);
		int[] keyConfig = { Input.KEY_D, Input.KEY_A, Input.KEY_W, Input.KEY_S, Input.KEY_LSHIFT, Input.KEY_F,
				Input.KEY_G, Input.KEY_H };
		this.setKeyConfig(keyConfig);
		this.setPosition(new Vector2f(300, 350));
	}
}
