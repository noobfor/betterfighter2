package de.tud.gdi1.bf2.model.player;

import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.entities.characters.Character;

/**
 * Represents a player which includes a specific key configuration and his chosen character
 * from the character selection menu. Use this class to get the character from the specific player
 * or the specific key configuration.
 * @author Tuan Kiet Tran
 *
 */
public class Player {

	private String name;
	/**
	 * LEFT, RIGHT, UP, DOWN, RUN, ATTACK, JUMP, DEFEND
	 */
	private int[] keyConfig;
	private Vector2f position;
	/**
	 * the chosen character of player
	 */
	private Character character;

	/**
	 * player constructor to create one player
	 * @param name custom name of the player
	 */
	public Player(String name) {
		this.name = name;
	}

	public void setChar(Character c) {
		this.character = c;
		this.character.setPosition(position);
	}

	public String getName() {
		return name;
	}

	/**
	 * the keyconfig of the specific player
	 * @return int array with each button
	 */
	public int[] getKeyConfig() {
		return keyConfig;
	}

	public void setKeyConfig(int[] keyConfig) {
		this.keyConfig = keyConfig;

	}

	public Vector2f getPosition() {
		return this.position;
	}

	public void setPosition(Vector2f newPosition) {
		this.position = newPosition;
	}

	public Character getChar() {
		return this.character;
	}

}
