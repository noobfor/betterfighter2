package de.tud.gdi1.bf2.model.entities.spells;

import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.entity.Entity;

public abstract class Spell extends Entity{

	protected boolean dir;
	private Character currentCharacter;
	private int manaCost;
	
	public Spell(String entityID, Character usingCharacter, Vector2f size, Vector2f position, int manaCost) {
		super(entityID);
		this.currentCharacter = usingCharacter;
		this.manaCost = manaCost;
		
		setPassable(false);
		setSize(size);
		setPosition(position);
		
	}

	abstract void setEvents();

	public int getManaCost() {
		return manaCost;
	}

	public void setManaCost(int manaCost) {
		this.manaCost = manaCost;
	}
	
	public Character getCharacter() {
		return currentCharacter;
	}

	public void setCharacter(Character c) {
		this.currentCharacter = c;
	}
	
}
