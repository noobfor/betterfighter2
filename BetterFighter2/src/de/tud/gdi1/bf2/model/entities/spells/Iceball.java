package de.tud.gdi1.bf2.model.entities.spells;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.action.DecreaseHPAction;
import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.events.CharacterCollisionEvent;
import de.tud.gdi1.bf2.model.events.IceWallCollisionEvent;
import de.tud.gdi1.bf2.model.events.WallCollisionEvent;
import de.tud.gdi1.bf2.model.render.RenderShapeComponent;
import eea.engine.action.basicactions.DestroyEntityAction;
import eea.engine.action.basicactions.MoveLeftAction;
import eea.engine.action.basicactions.MoveRightAction;
import eea.engine.event.basicevents.LoopEvent;

public class Iceball extends Spell{

	private static Vector2f size = new Vector2f(20, 20);
	private static int manaCost = 20;
	private int damage = 5;
	private int knockback = 4;
	private float projectileSpeed = 0.1f;
	private boolean direction;
	
	public Iceball(String entityID, Character usingCharacter, Vector2f position, boolean direction) {
		super(entityID, usingCharacter, size, position, manaCost);
		this.direction = direction;
		
		addComponent(new RenderShapeComponent(new Rectangle(0, 0, size.getX(), size.getY()), Color.blue, 1));
		setEvents();
	}

	@Override
	void setEvents() {
		CharacterCollisionEvent characterCollided = new CharacterCollisionEvent(1);
		characterCollided.addAction(new DecreaseHPAction(damage, knockback, this, this.getCharacter()));
		characterCollided.addAction(new DestroyEntityAction());
		addComponent(characterCollided);
		
		IceWallCollisionEvent iceWallCollided = new IceWallCollisionEvent(1);
		iceWallCollided.addAction(new DecreaseHPAction(0, 0));
		iceWallCollided.addAction(new DestroyEntityAction());
		addComponent(iceWallCollided);
		
		WallCollisionEvent wallCollided = new WallCollisionEvent();
		wallCollided.addAction(new DestroyEntityAction());
		addComponent(wallCollided);
		
		LoopEvent loop = new LoopEvent();
		if (direction)
		loop.addAction(new MoveRightAction(projectileSpeed));
		else loop.addAction(new MoveLeftAction(projectileSpeed));
		addComponent(loop);
	}

}
