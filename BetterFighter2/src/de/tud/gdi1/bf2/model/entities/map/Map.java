package de.tud.gdi1.bf2.model.entities.map;

import org.newdawn.slick.SlickException;

import eea.engine.component.render.ImageRenderComponent;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;

public class Map extends Entity {

	Wall wallnorth;
	Wall wallsouth;
	Wall walleast;
	Wall wallwest;
	ImageRenderComponent mapPicture;
	int currentstate;

	public Map(String entityID, int stateid, Wall north, Wall south, Wall east, Wall west,
			ImageRenderComponent background) throws SlickException {
		super(entityID);
		wallnorth = north;
		wallsouth = south;
		walleast = east;
		wallwest = west;
		mapPicture = background;
		currentstate = stateid;
		setMap();
	}

	public void setMap() throws SlickException {
		// setPosition(new Vector2f(400,330)); // Startposition des Hintergrunds
		// addComponent(mapPicture); // Bildkomponente

		// set Walls
		StateBasedEntityManager.getInstance().addEntity(currentstate, wallnorth);
		StateBasedEntityManager.getInstance().addEntity(currentstate, walleast);
		StateBasedEntityManager.getInstance().addEntity(currentstate, wallsouth);
		StateBasedEntityManager.getInstance().addEntity(currentstate, wallwest);

	}

}
