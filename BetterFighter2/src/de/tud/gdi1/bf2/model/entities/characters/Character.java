package de.tud.gdi1.bf2.model.entities.characters;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.action.RemoveGivenEventAction;
import de.tud.gdi1.bf2.model.action.animation.CharacterIdle;
import de.tud.gdi1.bf2.model.action.basicmoves.Run;
import de.tud.gdi1.bf2.model.action.basicmoves.Shield;
import de.tud.gdi1.bf2.model.action.basicmoves.StartBasicPunch;
import de.tud.gdi1.bf2.model.action.basicmoves.StartDashAttack;
import de.tud.gdi1.bf2.model.action.basicmoves.StartJumpAction;
import de.tud.gdi1.bf2.model.action.basicmoves.Walk;
import de.tud.gdi1.bf2.model.action.crowdcontrol.ExplosionKnockback;
import de.tud.gdi1.bf2.model.action.crowdcontrol.FreezeAction;
import de.tud.gdi1.bf2.model.action.crowdcontrol.OnTheGroundKnockback;
import de.tud.gdi1.bf2.model.action.crowdcontrol.StrongKnockback;
import de.tud.gdi1.bf2.model.action.crowdcontrol.WeakKnockback;
import de.tud.gdi1.bf2.model.events.CharacterCollisionEvent;
import de.tud.gdi1.bf2.model.events.WallCollisionEvent;
import de.tud.gdi1.bf2.model.player.Player;
import de.tud.gdi1.bf2.model.render.OurAnimationRenderComponent;
import de.tud.gdi1.bf2.model.render.RenderShapeComponent;
import de.tud.gdi1.bf2.ui.BetterFighter2;
import eea.engine.action.basicactions.MoveLeftAction;
import eea.engine.action.basicactions.MoveRightAction;
import eea.engine.entity.Entity;
import eea.engine.event.ANDEvent;
import eea.engine.event.Event;
import eea.engine.event.NOTEvent;
import eea.engine.event.OREvent;
import eea.engine.event.basicevents.KeyDownEvent;
import eea.engine.event.basicevents.KeyPressedEvent;
import eea.engine.event.basicevents.LoopEvent;

public abstract class Character extends Entity {
	private static int MAXHEALTH = 100;
	private static int MAXMANA = 100;

	private Vector2f size = new Vector2f(50, 75);
	private int health, mana;
	private float speed = 0.1f;
	private float runningSpeed = 0.15f;
	private Player p;

	private boolean iceWallCooldown = false;

	// false = left, true = right
	private boolean direction;
	private boolean dead = false;
	private boolean superArmor = false;

	//
	/**
	 * 0 = jumping, 1 = attacking, 2 = shielding, 3 = special move
	 */
	private boolean[] actions = { false, false, false, false };

	/**
	 * 0 = weak, 1 = strong, 2 = freezed, 3 = ontheground, 4 = explosion
	 */
	private boolean[] crowdControl = { false, false, false, false, false };

	// 0 = idle, 1 = walking, 2 = running, 3 = jumping, 4 = throwing projecile, 5 =
	// attacking, 6 = shielding
	private boolean[] animations = { false, false, false, false, false, false, false };

	public int[] keyConfig;

	OurAnimationRenderComponent RenderComponent;

	Image[] idle;
	Image[] walking;
	Image[] running;
	Image[] throwing;
	Image[] shielding;
	Image[] jumping;

	boolean directionChange = false;

	public Character(String entityID, Player p) {
		super(entityID);
		health = MAXHEALTH;
		mana = MAXMANA;
		setSize(size);
		setPassable(true);
		setPosition(p.getPosition());
		addComponent(new RenderShapeComponent(new Rectangle(0, 0, size.getX(), size.getY()), Color.red, 1));
		keyConfig = p.getKeyConfig();
		this.p = p;
		direction = p.getName().equals("player1");
		setMoves();
	}

	abstract protected void setSpecialMoves();
	
	public OurAnimationRenderComponent getRenderComponent() {
		return RenderComponent;
	}

	public void setAnimations(Image[] idle, Image[] walking, Image[] running, Image[] throwing, Image[] shielding,
			Image[] jumping) {
		setIdleAnimation(idle);
		setWalkingAnimation(walking);
		setRunningAnimation(running);
		setThrowingAnimation(throwing);
		setShieldingAnimation(shielding);
		setJumpingAnimation(jumping);
	}

	public void setIdleAnimation(Image[] animation) {
		this.idle = animation;
	}

	public void setWalkingAnimation(Image[] animation) {
		this.walking = animation;
	}

	public void setRunningAnimation(Image[] animation) {
		this.running = animation;
	}

	public void setThrowingAnimation(Image[] animation) {
		this.throwing = animation;
	}

	public void setShieldingAnimation(Image[] animation) {
		this.shielding = animation;
	}

	public void setJumpingAnimation(Image[] animation) {
		this.jumping = animation;
	}

	public Image[] getMirroreddAnimation(Image[] animation) {
		if (!BetterFighter2.debug) {
			Image[] result = new Image[animation.length];
			for (int i = 0; i < result.length; i++) {
				result[i] = animation[i].getFlippedCopy(true, false);
			}
			return result;
		} else {
			return null;
		}

	}

	public void startAnimation(Image[] currentAnimation, float speed, boolean looping) {
		OurAnimationRenderComponent currentAnimationRender;
		if (direction) {
			currentAnimationRender = new OurAnimationRenderComponent(currentAnimation, speed, size, looping);
		} else {
			currentAnimationRender = new OurAnimationRenderComponent(getMirroreddAnimation(currentAnimation), speed,
					size, looping);
		}
		RenderComponent = currentAnimationRender;
		addComponent(currentAnimationRender);
	}

	public void setDirectionChange(boolean b) {
		directionChange = b;
	}

	public boolean getDirectionChange() {
		return directionChange;
	}

	private void setMoves() {
		int right = keyConfig[0];
		int left = keyConfig[1];
		int up = keyConfig[2];
		int down = keyConfig[3];
		int run = keyConfig[4];
		int attack = keyConfig[5];
		int jump = keyConfig[6];
		int shield = keyConfig[7];
		// set up wall collision events

		KeyDownEvent keyRun = new KeyDownEvent(run);
		NOTEvent notKeyRun = new NOTEvent(keyRun);

		KeyDownEvent keyLeft = new KeyDownEvent(left);
		KeyDownEvent keyDown = new KeyDownEvent(down);
		KeyDownEvent keyRight = new KeyDownEvent(right);
		KeyDownEvent keyUp = new KeyDownEvent(up);

		ANDEvent walkLeft = new ANDEvent(keyLeft, notKeyRun);
		walkLeft.addAction(new Walk(0, this, speed, true));
		addComponent(walkLeft);

		ANDEvent walkRight = new ANDEvent(keyRight, notKeyRun);
		walkRight.addAction(new Walk(1, this, speed, true));
		addComponent(walkRight);

		ANDEvent walkUp = new ANDEvent(keyUp, notKeyRun);
		walkUp.addAction(new Walk(2, this, speed, true));
		addComponent(walkUp);

		ANDEvent walkDown = new ANDEvent(keyDown, notKeyRun);
		walkDown.addAction(new Walk(3, this, speed, true));
		addComponent(walkDown);

		ANDEvent runLeft = new ANDEvent(keyRun, keyLeft);
		runLeft.addAction(new Run(0, this, runningSpeed, true));
		addComponent(runLeft);

		ANDEvent runRight = new ANDEvent(keyRun, keyRight);
		runRight.addAction(new Run(1, this, runningSpeed, true));
		addComponent(runRight);

		ANDEvent runUp = new ANDEvent(keyRun, keyUp);
		runUp.addAction(new Run(2, this, runningSpeed, true));
		addComponent(runUp);

		ANDEvent runDown = new ANDEvent(keyRun, keyDown);
		runDown.addAction(new Run(3, this, runningSpeed, true));
		addComponent(runDown);

		// run
		// links
		WallCollisionEvent wallCollided = new WallCollisionEvent();
		ANDEvent runleftWall = new ANDEvent(wallCollided, runLeft);
		runleftWall.addAction(new Run(1, this, runningSpeed, false));
		addComponent(runleftWall);

		// rechts
		ANDEvent runrightWall = new ANDEvent(wallCollided, runRight);
		runrightWall.addAction(new Run(0, this, runningSpeed, false));
		addComponent(runrightWall);

		// up
		ANDEvent runUpWall = new ANDEvent(wallCollided, runUp);
		runUpWall.addAction(new Run(3, this, runningSpeed, false));
		addComponent(runUpWall);

		// down
		ANDEvent runDownWall = new ANDEvent(wallCollided, runDown);
		runDownWall.addAction(new Run(2, this, runningSpeed, false));
		addComponent(runDownWall);

		ANDEvent leftWall = new ANDEvent(wallCollided, keyLeft);
		leftWall.addAction(new Walk(1, this, speed, false));
		addComponent(leftWall);

		ANDEvent rightWall = new ANDEvent(wallCollided, keyRight);
		rightWall.addAction(new Walk(0, this, speed, false));
		addComponent(rightWall);

		ANDEvent upWall = new ANDEvent(wallCollided, keyUp);
		upWall.addAction(new Walk(3, this, speed, false));
		addComponent(upWall);

		ANDEvent downWall = new ANDEvent(wallCollided, keyDown);
		downWall.addAction(new Walk(2, this, speed, false));
		addComponent(downWall);

		KeyDownEvent keyJump = new KeyDownEvent(jump);

		NOTEvent notLeft = new NOTEvent(keyLeft);
		NOTEvent notRight = new NOTEvent(keyRight);
		NOTEvent notUp = new NOTEvent(keyUp);
		NOTEvent notDown = new NOTEvent(keyDown);

		// idle Animation
		ANDEvent idle = new ANDEvent(notRight, notLeft, notUp, notDown);
		idle.addAction(new CharacterIdle());
		addComponent(idle);

		// no direction
		addComponent(createJumpEvent(-1, keyJump, notLeft, notRight, notUp, notDown));

		// left
		addComponent(createJumpEvent(0, keyJump, keyLeft, notRight, notUp, notDown));

		// right
		addComponent(createJumpEvent(1, keyJump, notLeft, keyRight, notUp, notDown));

		// left up
		addComponent(createJumpEvent(2, keyJump, keyLeft, notRight, keyUp, notDown));

		// left down
		addComponent(createJumpEvent(3, keyJump, keyLeft, notRight, notUp, keyDown));

		// right up
		addComponent(createJumpEvent(4, keyJump, notLeft, keyRight, keyUp, notDown));

		// right down
		addComponent(createJumpEvent(5, keyJump, notLeft, keyRight, notUp, keyDown));

		// up
		addComponent(createJumpEvent(6, keyJump, notLeft, notRight, keyUp, notDown));

		// down
		addComponent(createJumpEvent(7, keyJump, notLeft, notRight, notUp, keyDown));

		// dash attack
		ANDEvent keyDashAttack = new ANDEvent(new OREvent(keyLeft, keyRight), keyRun, new KeyDownEvent(attack));
		keyDashAttack.addAction(new StartDashAttack(this));
		addComponent(keyDashAttack);

		// basicpunch
		KeyPressedEvent keyAttack = new KeyPressedEvent(attack);
		keyAttack.addAction(new StartBasicPunch(this));
		addComponent(keyAttack);

		// shielding

		KeyPressedEvent keyShield = new KeyPressedEvent(shield);
		keyShield.addAction(new Shield(this));
		addComponent(keyShield);

	}

	public void setDirection(boolean direction) {
		this.direction = direction;
		this.removeComponent(RenderComponent);

		setIdleAnimation(idle);
		setWalkingAnimation(walking);
	}

	public boolean getDirection() {
		return direction;
	}

	/**
	 * 
	 * @param knockback
	 *            what kind of knockback is happening. 1 = weak knockback, 2 =
	 *            medium knockback, 3 = strong knockback, 4 = freeze, 5 = lying on
	 *            the floor, 6 = explosion knockback
	 * @param enemyDirection
	 *            direction of the enemyenitty which is triggering this hitstun on
	 *            this entity
	 * @throws SlickException
	 */
	public void hitstunAnimation(int knockback, boolean enemyDirection) throws SlickException {

		// weak knockback
		if (knockback == 1) {

			if(this.isWeakKnockback()) {
				this.hitstunAnimation(3, enemyDirection);
			}
			else {
			this.setWeakKnockback(true);
			LoopEvent loop = new LoopEvent();
			Event[] eventsToRemove = { loop };

			loop.addAction(new WeakKnockback(this, System.currentTimeMillis(), eventsToRemove, this.getPosition()));

			this.addComponent(loop);
			}

		}

		// strong knockback
		else if (knockback == 3) {

			this.setStrongKnockback(true);

			ANDEvent rightWall = null;
			ANDEvent leftWall = null;
			MoveRightAction knockRight;
			MoveLeftAction knockLeft;
			WallCollisionEvent wallknockback = new WallCollisionEvent();

			LoopEvent fall = new LoopEvent();

			if (enemyDirection) {
				knockRight = new MoveRightAction(0.5f);
				fall.addAction(knockRight);
				this.addComponent(fall);
				rightWall = new ANDEvent(wallknockback, fall);
				rightWall.addAction(new MoveLeftAction(0.5f));
				addComponent(rightWall);

			} else {
				knockLeft = new MoveLeftAction(0.5f);
				fall.addAction(knockLeft);
				addComponent(fall);
				leftWall = new ANDEvent(wallknockback, fall);
				leftWall.addAction(new MoveRightAction(0.5f));
				addComponent(leftWall);
			}

			Event[] eventsToRemove = { fall, wallknockback, rightWall, leftWall };

			fall.addAction(new StrongKnockback(this, eventsToRemove));

		}

		// freezed
		else if (knockback == 4) {

			this.setFreezed(true);

			LoopEvent loop = new LoopEvent();

			Event[] eventsToRemove = { loop };

			loop.addAction(new FreezeAction(this, eventsToRemove, this.getPosition()));

			this.addComponent(loop);

		}

		// lying on the floor
		else if (knockback == 5) {

			LoopEvent loop = new LoopEvent();
			Event[] eventsToRemove = { loop };

			this.setOnTheGround(true);
			loop.addAction(new OnTheGroundKnockback(this, eventsToRemove, this.getPosition()));

			this.addComponent(loop);

		}

		// explosion knockback
		else if (knockback == 6) {

			this.setExplosionKnockback(true);

			LoopEvent loop = new LoopEvent();

			CharacterCollisionEvent collision = new CharacterCollisionEvent();
			collision.addAction(new RemoveGivenEventAction(loop));
			this.addComponent(collision);

			Event[] eventsToRemove = { loop, collision };

			// add lying on the floor action
			loop.addAction(new ExplosionKnockback(this, eventsToRemove, this.getPosition(),
					enemyDirection));
			this.addComponent(loop);

		}

	}

	private ANDEvent createJumpEvent(int direction, Event... events) {
		ANDEvent jumpEvent = new ANDEvent(events);
		jumpEvent.addAction(new StartJumpAction(direction));
		return jumpEvent;
	}

	public void setJumping(boolean b) {
		this.actions[0] = b;
		if (!animations[3] || directionChange) {
			// System.out.println("starting jumping animation");
			resetAnimations();
			animations[3] = true;
			this.removeComponent(RenderComponent);
			startAnimation(jumping, 0.0035f, false);
			if (directionChange) {
				directionChange = false;
			}
		}
	}

	public boolean isJumping() {
		return actions[0];
	}

	public void setpunching(boolean b) {
		this.actions[1] = b;
		this.animations[5] = b;
	}

	public boolean isPunching() {
		return actions[1];
	}

	public void setShielding(boolean b) {
		this.actions[2] = b;
		if (!animations[6] || directionChange) {
			resetAnimations();
			animations[6] = true;
			this.removeComponent(RenderComponent);
			startAnimation(shielding, 0.015f, false);
			if (directionChange) {
				directionChange = false;
			}
		}
	}

	public boolean isShielding() {
		return actions[2];
	}

	/**
	 * 
	 * actions: 0 = jumping, 1 = attacking, 2 = shielding, 3 = special move
	 * 
	 * @param b
	 *            the boolean in which all the actions should be set
	 */
	public void setAllActions(boolean b) {
		for (int i = 0; i < actions.length; i++) {
			actions[i] = b;
		}
	}

	public boolean isDoingAnyAction() {
		for (int i = 0; i < actions.length; i++) {
			if (actions[i])
				return true;
		}
		return false;
	}

	public void setThrowing(boolean b) {
		this.actions[3] = b;
		if (!animations[4] || directionChange) {
			// System.out.println("starting throwing animation");
			resetAnimations();
			animations[4] = true;
			this.removeComponent(RenderComponent);
			startAnimation(throwing, 0.005f, false);
			if (directionChange) {
				directionChange = false;
			}
		}
	}

	public boolean isThrowing() {
		return actions[3];
	}

	public int getHealth() {
		return health;
	}

	public void addHealth(int i) {
		if (MAXHEALTH - health < i)
			health = MAXHEALTH;

		else
			health += i;
	}

	public void decreaseHealth(int i) {
		if (health < i)
			health = 0;
		else
			health -= i;
	}

	public void decreaseMana(int i) {
		if (mana < i)
			mana = 0;
		else
			mana -= i;
	}

	public int getMaxHealthpoints() {
		return MAXHEALTH;
	}

	public int getMana() {
		return mana;
	}

	public int getMaxManapoints() {
		return MAXMANA;
	}

	public Player getPlayer() {
		return p;
	}

	public void addMana(int i) {
		if (MAXMANA - mana < i)
			mana = MAXMANA;

		else
			mana += i;
	}

	public void Kill() {
		dead = true;
	}

	public boolean isDead() {
		return dead;
	}

	public void setSpecialMove(boolean b) {
		actions[3] = b;
	}

	public boolean isUsingSpecialMove() {
		return actions[3];
	}

	public void setFireBreathing(boolean b) {
		actions[3] = b;
	}

	public boolean isFireBreathing() {
		return actions[3];
	}

	public void setIceRain(boolean b) {
		actions[3] = b;
	}

	public boolean isIceRaining() {
		return actions[3];
	}

	/**
	 * 
	 * @return actions: 0 = jumping, 1 = attacking, 2 = shielding, 3 = special move
	 */
	public boolean[] getActions() {
		return actions;
	}

	// Crowd controls

	/**
	 * 
	 * actions: 0 = jumping, 1 = attacking, 2 = shielding, 3 = special move
	 * 
	 * @param b
	 *            the boolean in which all the actions should be set
	 */
	public void setAllCrowdControl(boolean b) {
		for (int i = 0; i < crowdControl.length; i++) {
			crowdControl[i] = b;
		}
	}

	public boolean isInAnyCrowdControl() {
		for (int i = 0; i < crowdControl.length; i++) {
			if (crowdControl[i])
				return true;
		}
		return false;
	}

	public boolean[] getCrowdControl() {
		return crowdControl;
	}

	public void setWeakKnockback(boolean b) {
		crowdControl[0] = b;
	}

	public boolean isWeakKnockback() {
		return crowdControl[0];
	}

	public void setStrongKnockback(boolean b) {
		crowdControl[1] = b;
	}

	public boolean isStrongKnockback() {
		return crowdControl[1];
	}

	public void setFreezed(boolean b) {
		if (!BetterFighter2.debug) {
		if (b) {
			RenderComponent.stopAnimation();
		} else {
			RenderComponent.continueAnimation();
		}
		}

		crowdControl[2] = b;
	}

	public boolean isFreezed() {
		return crowdControl[2];
	}

	public void setOnTheGround(boolean b) {
		crowdControl[3] = b;
	}

	public boolean isOntheGround() {
		return crowdControl[3];
	}

	public void setExplosionKnockback(boolean b) {
		crowdControl[4] = b;
	}

	public boolean isInExplosionKnockback() {
		return crowdControl[4];
	}

	public void resetAnimations() {
		for (int i = 0; i < animations.length; i++) {
			animations[i] = false;
		}
	}

	public void setIdle() {
		if (!animations[0]) {
			// System.out.println("starting idle animation");
			resetAnimations();
			animations[0] = true;
			this.removeComponent(RenderComponent);
			startAnimation(idle, 0.005f, true);
		}

	}

	public void setWalking() {
		if (!animations[1] || directionChange) { // if the direction has changed, the animation has to restart
			// System.out.println("starting walking animation");
			resetAnimations();
			animations[1] = true;
			this.removeComponent(RenderComponent);
			startAnimation(walking, 0.01f, true);
			if (directionChange) {
				directionChange = false;
			}
		}
	}

	public void setRunning() {
		if (!animations[2] || directionChange) {

			// System.out.println("starting running animation");
			resetAnimations();
			animations[2] = true;
			this.removeComponent(RenderComponent);
			startAnimation(running, 0.02f, true);
			if (directionChange) {
				directionChange = false;
			}
		}
	}

	public boolean[] getAnimations() {
		return animations;
	}

	public boolean getSuperArmor() {
		return superArmor;
	}

	public void setSuperArmor(boolean b) {
		superArmor = b;
	}

	public boolean getIceWallCooldown() {
		return iceWallCooldown;
	}

	public void setIceWallCooldown(boolean b) {
		iceWallCooldown = b;
	}

}
