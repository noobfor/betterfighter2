package de.tud.gdi1.bf2.model.entities.spells;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.action.DecreaseHPAction;
import de.tud.gdi1.bf2.model.action.casts.IceRainAction;
import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.events.CharacterCollisionEvent;
import de.tud.gdi1.bf2.model.events.IceWallCollisionEvent;
import de.tud.gdi1.bf2.model.render.RenderShapeComponent;
import eea.engine.action.basicactions.DestroyEntityAction;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.basicevents.LoopEvent;

public class IceDrop extends Spell{

	private static Vector2f size = new Vector2f(10,20);
	private static int manaCost = 40;
	StateBasedEntityManager entityManager = StateBasedEntityManager.getInstance();
	

	/**
	 * 
	 * @param entityID
	 * @param usingCharacter
	 * @param position
	 */
	public IceDrop(String entityID, Character usingCharacter, Vector2f position) {
		super("IceDrop" + entityID, usingCharacter, size, position, manaCost);
		
		addComponent(new RenderShapeComponent(new Rectangle(0, 0, size.getX(), size.getY()), Color.blue, 1));
		setEvents();
	}

	@Override
	void setEvents() {

		CharacterCollisionEvent characterCollision = new CharacterCollisionEvent(1);
		characterCollision.addAction(new DecreaseHPAction(2, 4, this, this.getCharacter()));
		characterCollision.addAction(new DestroyEntityAction());
		this.addComponent(characterCollision);
		
		IceWallCollisionEvent iceWallCollision = new IceWallCollisionEvent(1);
		iceWallCollision.addAction(new DecreaseHPAction(0, 0));
		this.addComponent(iceWallCollision);
	
		LoopEvent loop = new LoopEvent();
		Character enemy;
		if(this.getCharacter().getID() == "p1")
			enemy = (Character) entityManager.getEntity(2, "player2");
		else
			enemy = (Character) entityManager.getEntity(2, "player1");
		
		loop.addAction(new IceRainAction(this.getPosition(), enemy, this.getCharacter().getDirection()));
		this.addComponent(loop);
		
	}

}
