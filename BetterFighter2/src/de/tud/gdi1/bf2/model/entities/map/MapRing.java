package de.tud.gdi1.bf2.model.entities.map;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.ui.BetterFighter2;
import eea.engine.component.render.ImageRenderComponent;

public class MapRing extends Map {

	private static Wall north;
	private static Wall south;
	private static Wall east;
	private static Wall west;
	private static ImageRenderComponent background;

	public MapRing(String entityID, int stateid) throws SlickException {
		super(entityID, stateid, north, south, east, west, background);
		setRing();
		if (!BetterFighter2.debug) {
			background = new ImageRenderComponent(new Image("/assets/map_ring.png"));
		}
	}

	public void setRing() throws SlickException {
		north = new Wall("wallNorth", new Vector2f(0, 200), new Vector2f(800, 50));
		south = new Wall("wallSouth", new Vector2f(0, 550), new Vector2f(800, 50));
		east = new Wall("wallEast", new Vector2f(750, 0), new Vector2f(50, 600));
		west = new Wall("wallWest", new Vector2f(0, 0), new Vector2f(50, 600));
	}

}
