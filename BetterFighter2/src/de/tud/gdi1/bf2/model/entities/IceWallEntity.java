package de.tud.gdi1.bf2.model.entities;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.action.DecreaseHPAction;
import de.tud.gdi1.bf2.model.events.CharacterCollisionEvent;
import de.tud.gdi1.bf2.model.events.FramesEvent;
import de.tud.gdi1.bf2.model.events.IceWallCollisionEvent;
import de.tud.gdi1.bf2.model.render.RenderShapeComponent;
import eea.engine.action.basicactions.DestroyEntityAction;
import eea.engine.entity.Entity;
import eea.engine.event.Event;

public class IceWallEntity extends Entity{
	
//	private StateBasedEntityManager entityManager;
//	private int numberOfIceWall = 0;
	private Vector2f currentPosition;
//	private Vector2f size;

	public IceWallEntity(String entityID, Vector2f position, Vector2f size) {
		super(entityID);
		this.currentPosition = position;
//		this.size = size;
		
		addComponent(new RenderShapeComponent(new Rectangle(0, 0, size.getX(), size.getY()), Color.red, 1));
		setSize(size);
		setPosition(currentPosition);
		setVisible(true);
		setScale(0.5f);
		setPassable(false);
		setEvents();
	}
	
	public void setEvents() {

		CharacterCollisionEvent characterCollision = new CharacterCollisionEvent(2);
		this.addComponent(characterCollision);
		
		IceWallCollisionEvent iceWallCollision = new IceWallCollisionEvent(1);
		iceWallCollision.addAction(new DecreaseHPAction(0, 0));
		this.addComponent(iceWallCollision);
		
		Event[] eventsToRemove = {characterCollision, iceWallCollision};
		
		FramesEvent frames = new FramesEvent("icewall frames", 1000, 5, eventsToRemove);
		frames.addAction(new DestroyEntityAction());
		this.addComponent(frames);
	}
	
}
