package de.tud.gdi1.bf2.model.entities.spells;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.action.DecreaseHPAction;
import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.events.CharacterCollisionEvent;
import de.tud.gdi1.bf2.model.events.FramesEvent;
import de.tud.gdi1.bf2.model.events.IceWallCollisionEvent;
import de.tud.gdi1.bf2.model.render.RenderShapeComponent;
import eea.engine.action.basicactions.DestroyEntityAction;
import eea.engine.event.Event;

public class IceWall extends Spell{

	private static int manaCost = 20;
	private int strength = 5;
	
	/**
	 * 
	 * @param entityID
	 * @param usingCharacter
	 * @param size
	 * @param position
	 */
	public IceWall(String entityID, Character usingCharacter, Vector2f size, Vector2f position) {
		super(entityID, usingCharacter, size, position, manaCost);
		
		addComponent(new RenderShapeComponent(new Rectangle(0, 0, size.getX(), size.getY()), Color.blue, 1));
		setEvents();
	}

	@Override
	void setEvents() {

		CharacterCollisionEvent characterCollision = new CharacterCollisionEvent(2);
		characterCollision.addAction(new DecreaseHPAction(strength, 4, this));
		this.addComponent(characterCollision);
		
		IceWallCollisionEvent iceWallCollision = new IceWallCollisionEvent(1);
		iceWallCollision.addAction(new DecreaseHPAction(0, 0));
		this.addComponent(iceWallCollision);
		
		Event[] eventsToRemove = {characterCollision, iceWallCollision};
		
		FramesEvent frames = new FramesEvent("icewall frames", 1000, 5, eventsToRemove);
		frames.addAction(new DestroyEntityAction());
		this.addComponent(frames);
		
	}

}
