package de.tud.gdi1.bf2.model.entities.characters;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import de.tud.gdi1.bf2.model.action.casts.StartFireBreath;
import de.tud.gdi1.bf2.model.action.casts.StartFireExplosion;
import de.tud.gdi1.bf2.model.action.casts.StartFireball;
import de.tud.gdi1.bf2.model.events.MultipleInputsEvent;
import de.tud.gdi1.bf2.model.player.Player;
import de.tud.gdi1.bf2.ui.BetterFighter2;
import eea.engine.event.ANDEvent;
import eea.engine.event.NOTEvent;
import eea.engine.event.basicevents.KeyDownEvent;

public class BudsyCharacter extends Character {

	public BudsyCharacter(String entityID, Player p) throws SlickException {
		super(entityID, p);
		String path = "assets/characters/Budsy/";

		if (!BetterFighter2.debug) {
			Image[] idle = { new Image(path + "idle/0.png"), new Image(path + "idle/1.png"),
					new Image(path + "idle/2.png"), new Image(path + "idle/3.png") };
			Image[] walking = { new Image(path + "walking/0.png"), new Image(path + "walking/1.png"),
					new Image(path + "walking/2.png"), new Image(path + "walking/3.png"),
					new Image(path + "walking/4.png"), new Image(path + "walking/5.png"),
					new Image(path + "walking/6.png"), new Image(path + "walking/7.png") };
			Image[] running = { new Image(path + "running/0.png"), new Image(path + "running/1.png"),
					new Image(path + "running/2.png"), new Image(path + "running/3.png"),
					new Image(path + "running/4.png"), new Image(path + "running/5.png"),
					new Image(path + "running/6.png"), new Image(path + "running/7.png") };
			Image[] throwing = { new Image(path + "throwing_projectile/0.png"),
					new Image(path + "throwing_projectile/1.png"), new Image(path + "throwing_projectile/2.png"),
					new Image(path + "throwing_projectile/3.png"), new Image(path + "throwing_projectile/4.png") };
			Image[] shielding = { new Image(path + "shielding/00.png"), new Image(path + "shielding/01.png"),
					new Image(path + "shielding/02.png"), new Image(path + "shielding/03.png"),
					new Image(path + "shielding/04.png"), new Image(path + "shielding/05.png"),
					new Image(path + "shielding/06.png"), new Image(path + "shielding/07.png"),
					new Image(path + "shielding/08.png"), new Image(path + "shielding/09.png"),
					new Image(path + "shielding/10.png") };
			Image[] jumping = { new Image(path + "jumping/0.png"), new Image(path + "jumping/1.png"),
					new Image(path + "jumping/2.png"), new Image(path + "jumping/3.png") };

			setAnimations(idle, walking, running, throwing, shielding, jumping);
		}

		setSpecialMoves();
	}

	@Override
	protected void setSpecialMoves() {
		int right = keyConfig[0];
		int left = keyConfig[1];
		int up = keyConfig[2];
		int down = keyConfig[3];
		// int run = keyConfig[4];
		int attack = keyConfig[5];
		int jump = keyConfig[6];
		int shield = keyConfig[7];

		// explosion
		MultipleInputsEvent fireExplosion = new MultipleInputsEvent("Fireexplosion", shield, up, jump);
		fireExplosion.addAction(new StartFireExplosion());
		this.addComponent(fireExplosion);

		// firebreath
		MultipleInputsEvent firebreath = new MultipleInputsEvent("Firebreath", shield, down, jump);
		firebreath.addAction(new StartFireBreath());
		addComponent(firebreath);

		// fireball
		ANDEvent shootingLeft = new ANDEvent(new MultipleInputsEvent("Shooting", shield, left, attack),
				new NOTEvent(new KeyDownEvent(right)));
		;
		shootingLeft.addAction(new StartFireball(false));
		addComponent(shootingLeft);
		ANDEvent shootingRight = new ANDEvent(new MultipleInputsEvent("Shooting", shield, right, attack),
				new NOTEvent(new KeyDownEvent(left)));
		;
		shootingRight.addAction(new StartFireball(true));
		addComponent(shootingRight);

	}

}
