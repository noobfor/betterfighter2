package de.tud.gdi1.bf2.model.entities.map;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.render.RenderShapeComponent;
import eea.engine.entity.Entity;

public class Wall extends Entity {

	Vector2f position;
	RenderShapeComponent WallHitbox;
	Vector2f size;

	public Wall(String entityID, Vector2f pos, Vector2f s) {
		super(entityID);
		size = s;
		position = pos.add(new Vector2f(s.getX() / 2, s.getY() / 2));
		WallHitbox = new RenderShapeComponent(new Rectangle(0, 0, size.getX(), size.getY()), Color.blue, 1);
		setWall();
	}

	public void setWall() {
		setPassable(false);
		setPosition(position);
		setSize(size);
		addComponent(WallHitbox);
	}

}
