package de.tud.gdi1.bf2.model.entities;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.entity.Entity;

public class HpAndManaDisplay extends Entity {

	private Character player1;
	private Character player2;

	public HpAndManaDisplay(String entityID, Character firstPlayer, Character secondPlayer) { // i ist der erste oder zweite spieler
		super(entityID);
		this.player1 = firstPlayer;
		this.player2 = secondPlayer;
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics graphicsContext) {

		// for player1
		graphicsContext.setColor(Color.black);
		graphicsContext.fillRect(100, 30, player1.getMaxHealthpoints() + 2, 20);
		graphicsContext.setColor(Color.red);
		graphicsContext.fillRect(101, 31, player1.getHealth(), 18);

		graphicsContext.setColor(Color.black);
		graphicsContext.fillRect(100, 55, player1.getMaxManapoints() + 2, 20);
		graphicsContext.setColor(Color.blue);
		graphicsContext.fillRect(101, 56, player1.getMana(), 18);

		// for player2
		graphicsContext.setColor(Color.black);
		graphicsContext.fillRect(520, 30, player1.getMaxHealthpoints() + 2, 20);
		graphicsContext.setColor(Color.red);
		graphicsContext.fillRect(521, 31, player2.getHealth(), 18);

		graphicsContext.setColor(Color.black);
		graphicsContext.fillRect(520, 55, player2.getMaxManapoints() + 2, 20);
		graphicsContext.setColor(Color.blue);
		graphicsContext.fillRect(521, 56, player2.getMana(), 18);

	}
}
