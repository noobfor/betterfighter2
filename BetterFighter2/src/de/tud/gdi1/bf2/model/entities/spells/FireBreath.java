package de.tud.gdi1.bf2.model.entities.spells;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.action.DecreaseHPAction;
import de.tud.gdi1.bf2.model.action.casts.FireBreathAction;
import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.events.CharacterCollisionEvent;
import de.tud.gdi1.bf2.model.events.IceWallCollisionEvent;
import de.tud.gdi1.bf2.model.render.RenderShapeComponent;
import eea.engine.event.basicevents.LoopEvent;

public class FireBreath extends Spell{

	private static Vector2f size = new Vector2f(65, 40);
	private static int manaCost = 5;
	private int damage = 10;
	private int knockback = 3;
	
	/**
	 * 
	 * @param entityID
	 * @param usingCharacter
	 * @param position
	 */
	public FireBreath(String entityID, Character usingCharacter, Vector2f position) {
		super(entityID, usingCharacter, size, position, manaCost);
		
		addComponent(new RenderShapeComponent(new Rectangle(0, 0, size.getX(), size.getY()), Color.orange, 1));
		setEvents();
	}

	@Override
	void setEvents() {
		
		CharacterCollisionEvent characterCollided = new CharacterCollisionEvent(1);
		characterCollided.addAction(new DecreaseHPAction(damage, knockback, this, this.getCharacter()));
		this.addComponent(characterCollided);
		
		IceWallCollisionEvent iceWallCollided = new IceWallCollisionEvent(1);
		iceWallCollided.addAction(new DecreaseHPAction(0, 0));
		addComponent(iceWallCollided);
		
		LoopEvent loop = new LoopEvent();
		loop.addAction(new FireBreathAction(this.getCharacter()));
		addComponent(loop);
		
	}

	public int getDamage() {
		return damage;
	}
	
	public int getKnockback() {
		return knockback;
	}
}
