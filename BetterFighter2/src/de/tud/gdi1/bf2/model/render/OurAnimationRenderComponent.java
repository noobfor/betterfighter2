package de.tud.gdi1.bf2.model.render;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.ui.BetterFighter2;
import eea.engine.component.RenderComponent;

public class OurAnimationRenderComponent extends RenderComponent {

	private Vector2f size;
	protected Animation animation;

	public OurAnimationRenderComponent(Image[] frames, float speed, Vector2f size, boolean looping) {
		super("OurAnimationRenderComponent");
		this.size = size;
		if (!BetterFighter2.debug) {
			animation = new Animation(frames, 1);
			animation.setSpeed(speed);
			animation.setLooping(looping);
			animation.start();
		}
	}

	public void stopAnimation() {
		animation.stop();
	}

	public void continueAnimation() {
		animation.start();
	}

	@Override
	public Vector2f getSize() {
		return size;
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics graphicsContext) {
		if (!animation.isStopped()) {
			animation.draw(getOwnerEntity().getPosition().getX() - (size.x / 2),
					getOwnerEntity().getPosition().getY() - (size.y / 2), size.x, size.y);
		} else {
			animation.getImage(animation.getFrame()).draw(getOwnerEntity().getPosition().getX() - (size.x / 2),
					getOwnerEntity().getPosition().getY() - (size.y / 2), size.x, size.y);
		}

	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta) {
	}

}
