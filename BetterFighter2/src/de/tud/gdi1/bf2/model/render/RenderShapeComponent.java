package de.tud.gdi1.bf2.model.render;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import eea.engine.component.RenderComponent;

public class RenderShapeComponent extends RenderComponent {

	Shape shape;
	Color color;
	int lineWidth;

	public RenderShapeComponent(Shape s, Color color, int lineWidth) {
		super("RenderShapeComponent");

		this.shape = s;
		this.color = color;
		this.lineWidth = lineWidth;
	}


	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics graphicsContext) {
		Vector2f pos = owner.getPosition();
		this.shape.setCenterX(pos.getX());
		this.shape.setCenterY(pos.getY());
		graphicsContext.setLineWidth(lineWidth);
		graphicsContext.setColor(color);
		graphicsContext.draw(this.shape);

	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta) {
	}


	@Override
	public Vector2f getSize() {
		return null;
	}

}
