package de.tud.gdi1.bf2.model.factory;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.ui.BetterFighter2;
import eea.engine.action.Action;
import eea.engine.component.render.ImageRenderComponent;
import eea.engine.entity.Entity;
import eea.engine.event.Event;
import eea.engine.interfaces.IEntityFactory;

public class ButtonFactory implements IEntityFactory {
	private Entity entity;
	private Action action;
	private String name;
	private Vector2f position;
	private Event event;
	private String assetPath;

	public ButtonFactory(String name, Vector2f position, String assetPath, Event event, Action action) {
		this.name = name;
		this.position = position;
		this.action = action;
		this.assetPath = assetPath;
		this.event = event;
	}

	@Override
	public Entity createEntity() {
		try {
			entity = new Entity(name);
			entity.setPosition(position);
			entity.setScale(0.7f);
			if (!BetterFighter2.debug) {
				entity.addComponent(new ImageRenderComponent(new Image(assetPath)));
			} else {
				if (entity.getID().equals("VSStartButton")) {
					entity.setSize(new Vector2f(116 * 0.7f, 36 * 0.7f));
				} else {
					entity.setSize(new Vector2f(252 * 0.7f, 109 * 0.7f));
				}
			}
			event.addAction(action);
			entity.addComponent(event);
			return entity;

		} catch (SlickException e) { // only part that can rise an exception is line 35 while opening the image
			e.printStackTrace();
			return null;
		}
	}

}
