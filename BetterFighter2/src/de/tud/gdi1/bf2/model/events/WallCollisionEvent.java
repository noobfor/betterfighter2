package de.tud.gdi1.bf2.model.events;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.map.Wall;
import de.tud.gdi1.bf2.model.entities.spells.IceWall;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.basicevents.CollisionEvent;

/**
 * Checks if the owner of this Event is colliding with a Wall
 * 
 * @author Kiet
 *
 */
public class WallCollisionEvent extends CollisionEvent {

	/**
	 * checks if the action(s) associated with this event shall be performed. In
	 * this case, the action(s) are only performed if an collision between the
	 * entity that own this event object and a different entity has taken place.
	 * 
	 * @param gc
	 *            the GameContainer object that handles the game loop, recording of
	 *            the frame rate, and managing the input system
	 * @param sb
	 *            the StateBasedGame that isolates different stages of the game
	 *            (e.g., menu, ingame, highscores etc.) into different states so
	 *            they can be easily managed and maintained.
	 * @param delta
	 *            the time passed in nanoseconds (ns) since the start of the event,
	 *            used to interpolate the current target position
	 * 
	 * @return true if the action(s) associated with this event shall be performed,
	 *         else false
	 */
	@Override
	protected boolean performAction(GameContainer gc, StateBasedGame sb, int delta) {
		// determine the first entity that has collided with the owner of the event
		Entity entity = StateBasedEntityManager.getInstance().collides(sb.getCurrentStateID(), owner);

		if (entity instanceof Wall) {
			return true;
		}
		
		 if (entity instanceof IceWall) {
		    	
		    	if(owner.getPosition().getY() + 15 >= entity.getPosition().getY() && 
		    			owner.getPosition().getY() - 15 <= entity.getPosition().getY()) {
		    	
		    		return true;
		    	}
		    }
		
		return false;
	}

}
