package de.tud.gdi1.bf2.model.events;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.entities.spells.IceDrop;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.basicevents.CollisionEvent;

public class SpellCollisionEvent extends CollisionEvent{
private Character charactercollided;
	
	int duration = 0;
	float startY;
	
	/**
	 * 
	 * @param duration number of hits the collided entity gets
	 */
	public SpellCollisionEvent() {
		this.id = "CharacterCollisionEvent";
	}
	
	
	/**
	   * checks if the action(s) associated with this event shall be performed. In
	   * this case, the action(s) are only performed if an collision between the
	   * entity that own this event object and a different oldCharacter has taken place.
	   * 
	   * @param gc
	   *          the GameContainer object that handles the game loop, recording of
	   *          the frame rate, and managing the input system
	   * @param sb
	   *          the StateBasedGame that isolates different stages of the game
	   *          (e.g., menu, ingame, highscores etc.) into different states so
	   *          they can be easily managed and maintained.
	   * @param delta
	   *          the time passed in nanoseconds (ns) since the start of the event,
	   *          used to interpolate the current target position
	   * 
	   * @return true if the action(s) associated with this event shall be
	   *         performed, else false
	   */
	  @Override
	  protected boolean performAction(GameContainer gc, StateBasedGame sb, int delta) {
	    // determine the first entity that has collided with the owner of the event
	    Entity entity = StateBasedEntityManager.getInstance().collides(sb.getCurrentStateID(), owner);
	    
	    // if the entity is a Player then indicate the
	    // willingness
	    // to perform the action(s)
	    
	    if (owner instanceof IceDrop) {
	    	 if (entity instanceof Character && duration != 0) {   	
	 	    	charactercollided = (Character) entity;
	 	    	
	 	    	duration--;
	 	    	return true;
	 	    	
	 	    }
	 	    // else, nothing is to be done
	 	    return false;
	 	  }
	    
	    
	    if (entity instanceof Character && duration != 0) {
	    	
	    	if(owner.getPosition().getY() + 20 >= entity.getPosition().getY() && 
	    			owner.getPosition().getY() - 20 <= entity.getPosition().getY()) {
		    	
//	    		System.out.println("hit 1");
	    	charactercollided = (Character) entity;
	    	duration--;
	    	return true;
	    	}
	    }
	    // else, nothing is to be done
	    return false;
	  }
	  
	  public Character getCollidedCharacter() {
		  if (charactercollided == null) {
		  }
		  
		    return charactercollided;
		  }
	  
}


