package de.tud.gdi1.bf2.model.events;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import eea.engine.event.Event;

public class MultipleInputsEvent extends Event {

	long timeBetweenPressingKeys = 500;
	long elapsedTime = -1;
	long startTime;
	boolean check_first_event = false;
	boolean check_second_event = false;
	int firstkey;
	int secondkey;
	int thirdkey;

	/**
	 * Check if multiple inputs are being pressed in sequence in a specific time
	 * frame
	 * 
	 * @param id
	 *            the event id
	 * @param firstkey
	 *            the first key to check for
	 * @param secondkey
	 *            the second key to check
	 * @param thirdkey
	 *            the third key to check
	 */
	public MultipleInputsEvent(String id, Integer firstkey, Integer secondkey, Integer thirdkey) {
		super(id);
		this.firstkey = firstkey;
		this.secondkey = secondkey;
		this.thirdkey = thirdkey;
	}

	@Override
	protected boolean performAction(GameContainer gc, StateBasedGame sb, int delta) {
		if (elapsedTime >= 2 * timeBetweenPressingKeys) {
			elapsedTime = -1;
			check_first_event = false;
			check_second_event = false;
			return false;
		}
		
		boolean firstKeyDown = gc.getInput().isKeyDown(firstkey);
		boolean secondKeyDown = gc.getInput().isKeyDown(secondkey);
		boolean thirdKeyDown = gc.getInput().isKeyDown(thirdkey);

		if (elapsedTime >= 0) {
			elapsedTime = System.currentTimeMillis() - startTime;
		}

		if (firstKeyDown && elapsedTime == -1 && !check_first_event) {
			startTime = System.currentTimeMillis() - 1;
			elapsedTime = 0;
			check_first_event = true;
			check_second_event = false;
//			System.out.println("first key pressed 0");
		}

		if (secondKeyDown && elapsedTime <= timeBetweenPressingKeys && check_first_event && !check_second_event) {
//			System.out.println("second key pressed " + elapsedTime);
			check_second_event = true;
		}

		if (check_first_event && check_second_event && thirdKeyDown && elapsedTime <= 2 * timeBetweenPressingKeys) {
//			System.out.println("third key pressed " + elapsedTime);
			check_first_event = false;
			check_second_event = false;
			return true;
		}
		return false;
	}
}
