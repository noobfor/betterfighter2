package de.tud.gdi1.bf2.model.events;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.spells.IceWall;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.basicevents.CollisionEvent;

public class IceWallCollisionEvent extends CollisionEvent{

	
private Entity iceWallCollided;
	
	int duration = 0;
	
	/**
	 * 
	 * @param duration number of hits the collided entity gets
	 */
	public IceWallCollisionEvent(int duration) {
		this.id = "IceWallCollisionEvent";
		this.duration = duration;
	}
	
	
	 @Override
	  protected boolean performAction(GameContainer gc, StateBasedGame sb, int delta) {
	    // determine the first entity that has collided with the owner of the event
	    Entity entity = StateBasedEntityManager.getInstance().collides(sb.getCurrentStateID(), owner);
	    
	    // if the entity is a Player then indicate the
	    // willingness
	    // to perform the action(s)
	    if ((entity instanceof IceWall) && duration != 0) {
	    	
//	    	System.out.println(owner.getPosition().getY());
//	    	System.out.println(entity.getPosition().getY());
	    	
	    	if(owner.getPosition().getY() + 20 >= entity.getPosition().getY() && 
	    			owner.getPosition().getY() - 20 <= entity.getPosition().getY()) {
	    	
	    	iceWallCollided = entity;
	    	duration--;
	    		return true;
	    	}
	    }
	    // else, nothing is to be done
	    return false;
	  }
	 

	  public Entity getCollidedIceWall() {
		  if (iceWallCollided == null) {
			  return null;
		  }
		  else return iceWallCollided;
		  }
	
}
