package de.tud.gdi1.bf2.model.events;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.event.Event;

public class FramesEvent extends Event {

	int i = 0;
	int frames;
	int newAction = -1;
	int removeEvent = -1;
	Event[] eventsToRemove;
	Event newEventtoAdd;
	Character character;

	// TODO: add boolean for consecutive moves
	public FramesEvent(String id, int frames) {
		super(id);
		this.frames = frames;
	}

	public FramesEvent(String id, int frames, Character entity) {
		super(id);
		this.frames = frames;
		this.character = entity;
	}

	public FramesEvent(String id, int frames, int removeEvent, Event[] eventsToRemove) {
		super(id);
		this.frames = frames;
		this.removeEvent = removeEvent;
		this.eventsToRemove = eventsToRemove;
	}
	
	// public FramesEvent(String id, int frames, Character entity, int newAction,
	// Event newEvent) {
	// super(id);
	// this.frames = frames;
	// this.character = entity;
	// this.newAction = newAction;
	// this.newEventtoAdd = newEvent;
	// }

	public int getFrame() {
		return frames;
	}

	@Override
	protected boolean performAction(GameContainer gc, StateBasedGame sb, int delta) {
		if (i == newAction) {
			owner.addComponent(newEventtoAdd);
			owner.removeComponent(this.getId());
			i++;
		}
		
		if (i == removeEvent) {
			
			for(int i = 0; i < eventsToRemove.length; i++) {
				owner.removeComponent(eventsToRemove[i]);	
			}
			
		}

		if (i < frames) {
			i++;
			return false;
		} else {
			i = 0;
			return true;
		}
	}
}
