package de.tud.gdi1.bf2.model.action.casts;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.entities.spells.IceWall;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.Event;

public class IceWallAction implements Action{

	private StateBasedEntityManager entityManager;
	int elapsedTime = 0;
	Event[] componentsToRemove;
	boolean direction;
	int iceWallOffset = 1;
	
	IceWall iceWall1;
	IceWall iceWall2;
	IceWall iceWall3;
	
	
	public IceWallAction(Event[] componentsToRemove, boolean direction) {
		this.componentsToRemove = componentsToRemove;
		this.direction = direction;
		entityManager = StateBasedEntityManager.getInstance();
		
		if(!direction)
			iceWallOffset *= -1;
		
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		// TODO Auto-generated method stub
		Character currentChar = (Character) event.getOwnerEntity();
		Vector2f currentCharPosition = currentChar.getPosition();
		
		if (elapsedTime == 0) {
			iceWall1 = new IceWall(currentChar.getID() + "1", currentChar, new Vector2f(50,70), new Vector2f(currentCharPosition.x + 55 * iceWallOffset, currentCharPosition.getY()));
			entityManager.addEntity(2, iceWall1);
		}
		
		if (elapsedTime == 10) {
			iceWall2 = new IceWall(currentChar.getID() + "2", currentChar, new Vector2f(50,70), new Vector2f(iceWall1.getPosition().getX() + 55  * iceWallOffset, iceWall1.getPosition().getY()));
			entityManager.addEntity(2, iceWall2);
		}
		
		if (elapsedTime == 20) {
			iceWall3 = new IceWall(currentChar.getID() + "3", currentChar, new Vector2f(50,70), new Vector2f(iceWall2.getPosition().getX() + 55 * iceWallOffset, iceWall2.getPosition().getY()));
			entityManager.addEntity(2, iceWall3);
		}
		
		if (elapsedTime == 20) {
			currentChar.setThrowing(false);
		}
		
		
		if (elapsedTime == 1020) {
			for(int i = 0; i < componentsToRemove.length; i++) {
				currentChar.removeComponent(componentsToRemove[i]);
			}
			currentChar.setIceWallCooldown(false);
		}
		
		elapsedTime++;
	}

}
