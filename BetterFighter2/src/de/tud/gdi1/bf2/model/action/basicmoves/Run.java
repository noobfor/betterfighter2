package de.tud.gdi1.bf2.model.action.basicmoves;

import de.tud.gdi1.bf2.model.entities.characters.Character;

public class Run extends BasicDirectionalMovement {

	/**
	 * 
	 * @param direction 0 = left, 1 = right, 2 = up , 3 = down
	 * @param c the character which moves
	 * @param runningspeed the speed to move
	 */
	public Run(int direction, Character c, float runningSpeed, boolean setDirection) {
		super(direction, c, runningSpeed, setDirection);
	}

	@Override
	void setState() {
		currentCharacter.setRunning();
	}

}
