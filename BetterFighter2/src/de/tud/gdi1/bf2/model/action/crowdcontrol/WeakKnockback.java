package de.tud.gdi1.bf2.model.action.crowdcontrol;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;

public class WeakKnockback implements Action{
	
	long elapsedTime;
	int currentFrames = 0;
	long startTime;
	Character currentChar;
	Event[] componentsToRemove;
	Vector2f currentPosition;
	
	public WeakKnockback(Character currentChar, long startTime, Event[] componentsToRemove, Vector2f currentPosition) {
		this.currentChar = currentChar;
		this.startTime = startTime;
		this.componentsToRemove = componentsToRemove;
		this.currentPosition = currentPosition;
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		// TODO Auto-generated method stub

		currentChar.setPosition(currentPosition);
		
		if (currentFrames > 40) {
			
			currentChar.setWeakKnockback(false);
			
			for(int i = 0; i < componentsToRemove.length; i++) {
				currentChar.removeComponent(componentsToRemove[i]);	
			}
		}

		//elapsedTime = System.currentTimeMillis() - startTime;;
		
		currentFrames++;
	
	}

}
