package de.tud.gdi1.bf2.model.action.casts;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.action.DecreaseHPAction;
import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.entities.spells.FireBreath;
import de.tud.gdi1.bf2.model.events.CharacterCollisionEvent;
import de.tud.gdi1.bf2.model.events.IceWallCollisionEvent;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.StateBasedEntityManager;

public class FireBreathAction implements Action {
	
	private StateBasedEntityManager entityManager;
	private Character currentCharacter;
	private int frames = 0;

	public FireBreathAction(Character character) {
		this.currentCharacter = character;
		
		entityManager = StateBasedEntityManager.getInstance();
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {

		FireBreath firebreath = (FireBreath) event.getOwnerEntity();
		int currentStateId = sb.getCurrentStateID();
		
		if(gc.getInput().isKeyDown(currentCharacter.keyConfig[7])){
			entityManager.removeEntity(currentStateId, firebreath);
			currentCharacter.setFireBreathing(false);
		}
		
		if (frames == 15) {

			firebreath.removeComponent("CharacterCollisionEvent");
			firebreath.removeComponent("IceWallCollisionEvent");
			
			CharacterCollisionEvent characterCollided = new CharacterCollisionEvent(1);
			characterCollided.addAction(new DecreaseHPAction(firebreath.getDamage(), firebreath.getKnockback(), firebreath, firebreath.getCharacter()));
			firebreath.addComponent(characterCollided);
			
			IceWallCollisionEvent iceWallCollided = new IceWallCollisionEvent(1);
			
			iceWallCollided.addAction(new DecreaseHPAction(0, 0));
			firebreath.addComponent(iceWallCollided);
			
			if (currentCharacter.getMana() >= firebreath.getManaCost()) {
				currentCharacter.decreaseMana(firebreath.getManaCost());
			} else {
				entityManager.removeEntity(currentStateId, firebreath);
				currentCharacter.setFireBreathing(false);
			}
			frames = 0;
			
		}
		frames++;
	}

}
