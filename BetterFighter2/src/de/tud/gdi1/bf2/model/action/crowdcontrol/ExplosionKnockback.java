package de.tud.gdi1.bf2.model.action.crowdcontrol;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;

public class ExplosionKnockback implements Action{

	private boolean direction;
	
	float duration = 2f;
	float height = 3f;
	
	float time = 0;
	
	float directionOffset = 1;
	
	long elapsedTime;
	Character currentChar;
	Event[] componentsToRemove;
	Vector2f startPosition = new Vector2f();
	Vector2f midPoint = new Vector2f();
	Vector2f endPoint = new Vector2f();
	int frame = 0;
	float speed = 0;
	
	public ExplosionKnockback(Character currentChar, Event[] componentsToRemove, Vector2f currentPosition, boolean direction) {
		this.currentChar = currentChar;
		this.componentsToRemove = componentsToRemove;
		this.startPosition = currentPosition;
		this.direction = direction;
		
		if(direction) {
			}
			else {

				directionOffset *= -1;
				
			}
		
	}
	
	private float knockbackFunction(float t, float startY) {
		return (float) - ( height - Math.pow( t - directionOffset * duration/2, 2) + 0);
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		
//		System.out.println(time);
		
		float y = knockbackFunction(time, startPosition.getY());
		
		//TODO Wall collision hardcoded, maybe change
		//TODO: Wall collision for south wall is missing
		if(currentChar.getPosition().getX() > 723 || currentChar.getPosition().getX() < 76)
			currentChar.setPosition(new Vector2f(currentChar.getPosition().getX(), currentChar.getPosition().getY() + y));
		else currentChar.setPosition(new Vector2f(currentChar.getPosition().getX() + time,
				currentChar.getPosition().getY() + y));
		
		
		if(direction)
		time += 0.05;
		else
			time -= 0.05;
		
		
		if(currentChar.getPosition().getY() >= startPosition.getY()) {
			
			try {
				currentChar.hitstunAnimation(5, currentChar.getDirection());
			} catch (SlickException e) {
				e.printStackTrace();
			}
			
			currentChar.setExplosionKnockback(false);
			
			for(int i = 0; i < componentsToRemove.length; i++) {
				currentChar.removeComponent(componentsToRemove[i]);	
			}
			
		}
		
	}

}
