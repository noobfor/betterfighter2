package de.tud.gdi1.bf2.model.action.casts;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.entities.spells.Fireball;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.StateBasedEntityManager;

public class StartFireball implements Action{

	private StateBasedEntityManager sbm;
	Vector2f fireBallStartingPosition = new Vector2f(40, 0);
	boolean dir;
	
	public StartFireball(boolean dir) {
		this.dir = dir;
		this.sbm = StateBasedEntityManager.getInstance();
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		
		Character character = (Character) event.getOwnerEntity();
		
		if (!character.isShielding() || character.isInAnyCrowdControl()) {
			return;
		}

		character.setDirection(dir);
		character.setThrowing(true);
		
		Fireball p;
		
		if (dir) {
			p = new Fireball(character.getID(), character, character.getPosition().copy().add(fireBallStartingPosition), dir);
		} else {
			p = new Fireball(character.getID(), character, character.getPosition().copy().sub(fireBallStartingPosition), dir);
		}
		
		if (character.getMana() >= p.getManaCost()) {
			character.decreaseMana(p.getManaCost());
			sbm.addEntity(sb.getCurrentStateID(), p);
			character.setThrowing(false);
		} 
		
		else {
			character.setThrowing(false);
		}
		
	}
}
