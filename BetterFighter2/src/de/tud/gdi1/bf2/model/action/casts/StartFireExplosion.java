package de.tud.gdi1.bf2.model.action.casts;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.action.DecreaseHPAction;
import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.events.CharacterCollisionEvent;
import de.tud.gdi1.bf2.model.events.IceWallCollisionEvent;
import de.tud.gdi1.bf2.model.render.RenderShapeComponent;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;
import eea.engine.event.basicevents.LoopEvent;

public class StartFireExplosion implements Action{
	
	private int strength = 15;
	private int knockback = 6;
	private int manaCost = 30;
	
	public StartFireExplosion() {
		
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		
		Character character = (Character) event.getOwnerEntity();
		
		if (!character.isShielding() || character.isInAnyCrowdControl()) {
			return;
		}
		
		
		
		if (character.getMana() >= manaCost) {
			character.setSpecialMove(true);
			
			//add super armor
			character.setShielding(true);
			
			character.decreaseMana(manaCost);		
			
			
			
			Vector2f charSize = character.getSize();
			Vector2f explosionSize = new Vector2f(charSize.x * 2, charSize.y * 2);
			
			character.setSize(explosionSize);
			
			//TODO: change this to animation
			character.addComponent(new RenderShapeComponent(new Rectangle(0, 0, 
					explosionSize.getX(), explosionSize.getY()), Color.red, 1));
			
			CharacterCollisionEvent charaCollision = new CharacterCollisionEvent(1);
			charaCollision.addAction(new DecreaseHPAction(strength, knockback, character));
			character.addComponent(charaCollision);
			
			IceWallCollisionEvent iceWallCollided = new IceWallCollisionEvent(1);
			iceWallCollided.addAction(new DecreaseHPAction(0, 0));
			character.addComponent(iceWallCollided);
			
			//System.out.println("in fireexplosion");
			
			LoopEvent loop = new LoopEvent();
			Event[] eventsToRemove = {loop, charaCollision, iceWallCollided};
			loop.addAction(new FireExplosionAction(charSize, eventsToRemove));
			character.addComponent(loop);
			
		} 
		
		else {
			character.setSpecialMove(false);
		}
		
	}

	
	
}
