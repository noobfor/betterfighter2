package de.tud.gdi1.bf2.model.action.basicmoves;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.entities.map.Wall;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.Event;

public class DashAttackAction implements Action {

	long startTime;
	long elapsedTime = 0;
	Vector2f currentPosition;
	Event[] componentsToremove;

	// can change to using system time if necessary!!!!!!!!
	public DashAttackAction(Event[] componentsToremove, Vector2f currentPosition) {
		this.currentPosition = currentPosition;
		this.componentsToremove = componentsToremove;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		Character currentChar = (Character) event.getOwnerEntity();

		Wall east = (Wall) StateBasedEntityManager.getInstance().getEntity(sb.getCurrentStateID(), "wallEast");
		Wall west = (Wall) StateBasedEntityManager.getInstance().getEntity(sb.getCurrentStateID(), "wallWest");

		float westThreshold = west.getSize().getX() + currentChar.getSize().getX() / 2;
		float eastThreshold = gc.getWidth() - (east.getSize().getX() + currentChar.getSize().getX() / 2);

		if (elapsedTime == 0) {
			if (currentChar.getDirection())
				currentChar
						.setPosition(new Vector2f(Math.min(currentPosition.x + 30, eastThreshold), currentPosition.y));
			else
				currentChar
						.setPosition(new Vector2f(Math.max(currentPosition.x - 30, westThreshold), currentPosition.y));
		}

		// until hitbox is finished
		if (elapsedTime == 30) {
			currentChar.removeComponent(componentsToremove[1]);
			currentChar.removeComponent(componentsToremove[2]);
		}

		// ending lag of the move, until 30 frames he can move again
		if (elapsedTime > 60) {

			currentChar.setpunching(false);
			currentChar.removeComponent(componentsToremove[0]);
		}

		elapsedTime++;
	}

}
