package de.tud.gdi1.bf2.model.action.animation;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.action.Action;
import eea.engine.component.Component;

public class CharacterIdle implements Action {

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		Character character = (Character) event.getOwnerEntity();
		if (character.isDoingAnyAction()) {
			return;
		}
		character.setIdle();
	}

}
