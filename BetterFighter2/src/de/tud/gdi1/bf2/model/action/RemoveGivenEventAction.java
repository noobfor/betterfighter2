package de.tud.gdi1.bf2.model.action;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;

public class RemoveGivenEventAction implements Action{

	private Event eventToRemove;
	
	public RemoveGivenEventAction(Event eventToRemove) {
		this.eventToRemove = eventToRemove;
	}
	
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		
//		System.out.println("remove given event");
		
		event.getOwnerEntity().removeComponent(eventToRemove);
	}

}
