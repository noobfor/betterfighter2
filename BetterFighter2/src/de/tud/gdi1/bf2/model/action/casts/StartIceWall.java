package de.tud.gdi1.bf2.model.action.casts;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;
import eea.engine.event.basicevents.LoopEvent;

public class StartIceWall implements Action {

	private int iceWallManaCost = 20;
	boolean direction;

	public StartIceWall(boolean b) {
		this.direction = b;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		// TODO Auto-generated method stub
		Character character = (Character) event.getOwnerEntity();

		if (!character.isShielding() || character.isInAnyCrowdControl() || character.getIceWallCooldown()) {
			return;
		}

		LoopEvent loop = new LoopEvent();
		Event[] componentsToremove = { loop };
		character.setThrowing(true);
		character.setDirection(direction);

		if (character.getMana() >= iceWallManaCost) {

			character.decreaseMana(iceWallManaCost);

			character.setIceWallCooldown(true);
			loop.addAction(new IceWallAction(componentsToremove, direction));
			character.addComponent(loop);
		}

		else
			character.setThrowing(false);

	}
}
