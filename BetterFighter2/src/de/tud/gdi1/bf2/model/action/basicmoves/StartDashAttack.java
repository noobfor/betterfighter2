package de.tud.gdi1.bf2.model.action.basicmoves;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.action.DecreaseHPAction;
import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.events.CharacterCollisionEvent;
import de.tud.gdi1.bf2.model.events.IceWallCollisionEvent;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;
import eea.engine.event.basicevents.LoopEvent;

public class StartDashAttack implements Action {

	private int strength = 15;
	private int knockback = 3;

	Character user;

	public StartDashAttack(Character user) {
		this.user = user;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {

		// System.out.println("in dashattackstart");

		if (user.isDoingAnyAction() || user.isInAnyCrowdControl()) {
			return;
		}
		user.setpunching(true);

		// if hit then decrease hp from enemy
		CharacterCollisionEvent punchingcharacter = new CharacterCollisionEvent(1);
		punchingcharacter.addAction(new DecreaseHPAction(strength, knockback, user));
		user.addComponent(punchingcharacter);

		IceWallCollisionEvent punchingIceWall = new IceWallCollisionEvent(1);
		punchingIceWall.addAction(new DecreaseHPAction(strength, knockback, user));
		user.addComponent(punchingIceWall);

		LoopEvent loop = new LoopEvent();
		Event[] componentsToremove = { loop, punchingcharacter, punchingIceWall };
		loop.addAction(new DashAttackAction(componentsToremove, user.getPosition()));
		user.addComponent(loop);

	}

}
