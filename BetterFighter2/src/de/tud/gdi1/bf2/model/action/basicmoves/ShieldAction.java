package de.tud.gdi1.bf2.model.action.basicmoves;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;

public class ShieldAction implements Action {

	int frames = 0;
	long startTime;
	long elapsedTime;
	Event[] componentsToRemove;
	Vector2f currentPosition;

	public ShieldAction(long starttime, Event[] componentsToremove, Vector2f currentPosition) {
		this.startTime = starttime;
		this.componentsToRemove = componentsToremove;
		this.currentPosition = currentPosition;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {

		Character currentChar = (Character) event.getOwnerEntity();

		currentChar.setPosition(currentPosition);

		if (frames == 50) {
			for (int i = 0; i < componentsToRemove.length; i++) {
				currentChar.removeComponent(componentsToRemove[i]);
			}
			// System.out.println("setting shield off");
			currentChar.setShielding(false);

		}
		frames++;

	}

}
