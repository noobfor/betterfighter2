package de.tud.gdi1.bf2.model.action;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.entities.spells.FireBreath;
import de.tud.gdi1.bf2.model.entities.spells.Fireball;
import de.tud.gdi1.bf2.model.entities.spells.IceWall;
import de.tud.gdi1.bf2.model.entities.spells.Iceball;
import de.tud.gdi1.bf2.model.entities.spells.Spell;
import de.tud.gdi1.bf2.model.events.CharacterCollisionEvent;
import de.tud.gdi1.bf2.model.events.IceWallCollisionEvent;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.StateBasedEntityManager;

public class DecreaseHPAction implements Action{

	IceWall currentIceWall = null;
	Character currentCharacter;
	Character enemyEntity;
	Spell currentSpell;
	Spell enemySpell;
	int strength;
	int knockback;
	
	public DecreaseHPAction(int strength, int knockback) {
		this.knockback = knockback;
		this.strength = strength;
	}

	/**
	 * 
	 * @param strength attack power of the move
	 * @param knockback what kind of knockback is happening. 1 = weak knockback, 2 = medium knockback, 3 = strong knockback,
	 * 4 = freeze, 5 = lying on the floor
	 * @param user the character which is attacking
	 */
	public DecreaseHPAction(int strength, int knockback, Character user) {
		this.knockback = knockback;
		this.strength = strength;
		this.currentCharacter = user;
	}
	
	/**
	 * 
	 * @param strength attack power of the move
	 * @param knockback  what kind of knockback is happening. 1 = weak knockback, 2 = medium knockback, 3 = strong knockback,
	 * 4 = freeze, 5 = lying on the floor
	 * @param spell the used spell which is attacking
	 * @param user the character which uses the spell
	 */
	public DecreaseHPAction(int strength, int knockback, Spell spell, Character user) {
		this.knockback = knockback;
		this.strength = strength;
		this.currentSpell = spell;
		this.currentCharacter = user;
	}
	
	/**
	 * 
	 * @param strength
	 * @param knockback what kind of knockback is happening. 1 = weak knockback, 2 = medium knockback, 3 = strong knockback,
	 * 4 = freeze, 5 = lying on the floor
	 * @param icewall the given IceWallEntity
	 */
	public DecreaseHPAction(int strength, int knockback, IceWall icewall) {
		this.knockback = knockback;
		this.strength = strength;
		this.currentIceWall = icewall;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		
		//System.out.println("hit");
		
		if(event instanceof CharacterCollisionEvent) {
			
			enemyEntity = (Character) ((CharacterCollisionEvent) event).getCollidedCharacter();
			enemySpell = (Spell) ((CharacterCollisionEvent) event).getCollidedSpell();
			
			//if enemy is a character
			if(enemyEntity != null) {
				
				// normal hit and not icewall hit, if enemy is freezed
				if (enemyEntity.isFreezed() && currentIceWall == null) {
	
					//if (enemyEntity.getPlayer() != currentCharacter.getPlayer()) {
						
						enemyEntity.decreaseHealth(strength);
						
						enemyEntity.setFreezed(false);
						try {
							enemyEntity.hitstunAnimation(5, enemyEntity.getDirection());
						} catch (SlickException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					//}
					
				}
				
				//if hit an incoming spell, reflect it
				else if(enemyEntity.isPunching() && currentSpell != null) {
					
//					System.out.println(currentSpell.getID());

					StateBasedEntityManager.getInstance().removeEntity(sb.getCurrentStateID(), currentSpell);
					
					if(currentSpell instanceof Fireball) {
						Fireball reflectedSpell = new Fireball(enemyEntity.getID(), enemyEntity, currentSpell.getPosition(), enemyEntity.getDirection());
						StateBasedEntityManager.getInstance().addEntity(sb.getCurrentStateID(), reflectedSpell);
						
					}
					else if(currentSpell instanceof Iceball) {
						Iceball reflectedSpell = new Iceball(enemyEntity.getID(), enemyEntity, currentSpell.getPosition(), enemyEntity.getDirection());
//						System.out.println(enemyEntity.getDirection());
						StateBasedEntityManager.getInstance().addEntity(sb.getCurrentStateID(), reflectedSpell);
//						System.out.println(reflectedSpell.getID());
//						System.out.println("entity should be added");
					
					}
					
				}
				
				// normal hit if enemy is not shielding
				else if (!enemyEntity.isShielding() && !enemyEntity.isOntheGround() && !enemyEntity.getSuperArmor()) {
					
					// if icewall move is being used
					if (currentIceWall != null) {
						
						enemyEntity.decreaseHealth(strength);
						try {
							enemyEntity.hitstunAnimation(4, enemyEntity.getDirection());
						} catch (SlickException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					
					// hit with spell
					// TODO: sum this "if" up with the upper "if" with icewall move
					else if ((currentSpell != null) && enemyEntity.getPlayer() != currentCharacter.getPlayer() || 
							enemyEntity.getPlayer() != currentCharacter.getPlayer()) {
						enemyEntity.decreaseHealth(strength);
						
						try {
							enemyEntity.hitstunAnimation(knockback, currentCharacter.getDirection());
						} catch (SlickException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
				
				
				// block if shielded
				else {
				}
			}
			
			//if enemy is a spell
			else if(enemySpell != null) {
				if(!(enemySpell instanceof FireBreath))
					StateBasedEntityManager.getInstance().removeEntity(sb.getCurrentStateID(), enemySpell);
			}
		}
			 // if attacked entity is a icewall
			else if (event instanceof IceWallCollisionEvent) {
				
				IceWall iceWall = (IceWall) ((IceWallCollisionEvent) event).getCollidedIceWall();
				StateBasedEntityManager.getInstance().removeEntity(sb.getCurrentStateID(), iceWall);
				
			}
		
		
		else {
			
		}
		
		//else if()
		
	}
}

