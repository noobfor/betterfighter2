package de.tud.gdi1.bf2.model.action.crowdcontrol;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;

import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;

public class StrongKnockback implements Action{

	private long frames = 0;
	private Character currentChar;
	private Event[] componentsToRemove;
	
	public StrongKnockback(Character character, Event[] componentsToRemove) {
		this.currentChar = character;
		this.componentsToRemove = componentsToRemove;
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		
		if (frames > 15) {
			currentChar.setStrongKnockback(false);
			
			
			for(int i = 0; i < componentsToRemove.length; i++) {
				currentChar.removeComponent(componentsToRemove[i]);	
			}
			
			
			try {
				currentChar.hitstunAnimation(5, currentChar.getDirection());
			} catch (SlickException e) {
				e.printStackTrace();
			}
			
		}

		frames++;
	}
	
}
