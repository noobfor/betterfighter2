package de.tud.gdi1.bf2.model.action.crowdcontrol;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.render.RenderShapeComponent;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;

public class OnTheGroundKnockback implements Action{
		
		long elapsedTime = 0;
		Character currentChar;
		Event[] componentsToRemove;
		Vector2f currentPosition = new Vector2f();
		Vector2f size;
		
		public OnTheGroundKnockback(Character character, Event[] componentsToRemove, Vector2f currentPosition) {
			this.currentChar = character;
			this.componentsToRemove = componentsToRemove;
			this.currentPosition = currentPosition;
		}
		
		@Override
		public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
			
			if (elapsedTime < 150) {
				//currentChar.setPosition(currentPosition);
				currentChar.setOnTheGround(true);
				
				
				//TODO: change animation to lying on the ground
				
				size = new Vector2f(75, 50);
				
				///// just for testing hitboxes
				currentChar.removeComponent("RenderShapeComponent");
				currentChar.addComponent(new RenderShapeComponent(new Rectangle(0, 0, size.getX(), size.getY()), Color.red, 1));
				/////
				
				//TODO: insert lying on the ground image here
				currentChar.setSize(size);
				
				
			}
			
			// invincibility frames after standing up
			if (150 < elapsedTime && elapsedTime < 250) {

				/////// only for testing
				size = new Vector2f(50, 75);
				currentChar.setSize(size);
				currentChar.removeComponent("RenderShapeComponent");
				currentChar.addComponent(new RenderShapeComponent(new Rectangle(0, 0, size.getX(), size.getY()), Color.red, 1));
				///////
				
				
				currentChar.setOnTheGround(false);
				currentChar.setSuperArmor(true);
			}
			
			if (elapsedTime > 250) {
				currentChar.setSuperArmor(false);
				for(int i = 0; i < componentsToRemove.length; i++) {
					currentChar.removeComponent(componentsToRemove[i]);	
				}
			}

			elapsedTime++;
		}

	
	
	
}
