package de.tud.gdi1.bf2.model.action;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.BudsyCharacter;
import de.tud.gdi1.bf2.model.entities.characters.NoobforCharacter;
import de.tud.gdi1.bf2.model.player.Player;
import de.tud.gdi1.bf2.ui.BetterFighter2;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.Entity;

public class SelectCharacterAction implements Action {

	Player player;
	Entity square;
	Vector2f[] entitiesPositions;

	public SelectCharacterAction(Player p, Entity square, Vector2f[] entitiesPositions) throws SlickException {
		this.player = p;
		this.square = square;
		this.entitiesPositions = entitiesPositions;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		float xPos = square.getPosition().getX();
		int selecterChar = -1;

		for (int i = 0; i < entitiesPositions.length; i++) {
			if (xPos == entitiesPositions[i].getX()) {
				selecterChar = i;
			}
		}

		switch (selecterChar) {
		case 0:
			try {
				player.setChar(new BudsyCharacter(player.getName(), player));
			} catch (SlickException e) {
				e.printStackTrace();
			}
			if (!BetterFighter2.debug) {
				System.out.println("Player " + player.getName() + " selected first Character.");
			}
			break;

		case 1:
			try {
				player.setChar(new NoobforCharacter(player.getName(), player));
			} catch (SlickException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (!BetterFighter2.debug) {
				System.out.println("Player " + player.getName() + " selected second Character.");
			}
			break;

		default:
			break;
		}

	}

}
