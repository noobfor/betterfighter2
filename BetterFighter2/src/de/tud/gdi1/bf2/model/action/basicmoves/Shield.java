package de.tud.gdi1.bf2.model.action.basicmoves;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;
import eea.engine.event.basicevents.LoopEvent;

public class Shield implements Action{

	Character character;
	
	public Shield(Character character) {
		this.character = character;
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {

		if (character.isDoingAnyAction() || character.isInAnyCrowdControl()) {
			return;
		}
		character.setShielding(true);
		
		LoopEvent loop = new LoopEvent();
		Event[] componentsToremove = {loop};
		loop.addAction(new ShieldAction(System.currentTimeMillis(), componentsToremove, character.getPosition()));
		character.addComponent(loop);
	}

}
