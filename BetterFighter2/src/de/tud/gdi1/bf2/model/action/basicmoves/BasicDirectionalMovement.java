package de.tud.gdi1.bf2.model.action.basicmoves;

import java.security.InvalidParameterException;

import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.action.Action;
import eea.engine.action.basicactions.Movement;

public abstract class BasicDirectionalMovement extends Movement implements Action {

	float speed;
	int direction;
	Character currentCharacter;
	boolean setDir = true;

	public BasicDirectionalMovement(int direction, Character c, float speed, boolean setDirection) {
		super(speed);
		this.speed = speed;
		this.direction = direction;
		currentCharacter = c;
		setDir = setDirection;
	}

	abstract void setState();

	@Override
	public Vector2f getNextPosition(Vector2f position, float speed, float rotation, int delta) {
		Vector2f pos = new Vector2f(position);
		if (currentCharacter.isDoingAnyAction() || currentCharacter.isInAnyCrowdControl()) {
			return pos;
		}
		setState();

		switch (direction) {
		case 0: // left
			if (currentCharacter.getDirection() && setDir) {
				currentCharacter.setDirection(false);
				currentCharacter.setDirectionChange(true);
			}

			pos.x -= speed * delta;
			break;
		case 1: // right
			if (!currentCharacter.getDirection() && setDir) {
				currentCharacter.setDirection(true);
				currentCharacter.setDirectionChange(true);
			}

			pos.x += speed * delta;
			break;
		case 2: // up
			pos.y -= speed * delta;
			break;
		case 3: // down
			pos.y += speed * delta;
			break;
		default:
			throw new InvalidParameterException("Invalid direction: " + direction);
		}

		return pos;
	}

}
