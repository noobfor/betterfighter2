package de.tud.gdi1.bf2.model.action.casts;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.render.RenderShapeComponent;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;

public class FireExplosionAction implements Action {

	int frames = 0;
	Vector2f oldSize;
	Event[] eventsToRemove;
	
	Character currentChar;
	
	public FireExplosionAction(Vector2f oldSize, Event[] eventsToRemove) {
		this.oldSize = oldSize;
		this.eventsToRemove = eventsToRemove;
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		
		currentChar = (Character) event.getOwnerEntity();
		//TODO: check interactions with other moves
		//currentChar.setShielding(false);
		
		//at 10 frames
		if (frames == 10) {

			currentChar.setSize(oldSize);
			currentChar.addComponent(new RenderShapeComponent(new Rectangle(0, 0, 
					oldSize.getX(), oldSize.getY()), Color.red, 1));
			
			currentChar.setSpecialMove(false);
			
			for(int i = 0; i < eventsToRemove.length; i++) {
				currentChar.removeComponent(eventsToRemove[i]);
			}
		}
		frames++;
	}

}
