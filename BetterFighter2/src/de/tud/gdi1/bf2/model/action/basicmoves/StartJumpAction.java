package de.tud.gdi1.bf2.model.action.basicmoves;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.basicevents.LoopEvent;

public class StartJumpAction implements Action {

	int direction;
	JumpAction jumpAct;

	public StartJumpAction(int jumpDirection) {
		direction = jumpDirection;
	}

	public JumpAction getJumpAction() {
		return jumpAct;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		Character character = (Character) event.getOwnerEntity();
		if (character.isDoingAnyAction() || character.isInAnyCrowdControl()) {
			return;
		}
		character.setJumping(true); // this is if character.isJumping() returns false
		LoopEvent le = new LoopEvent();
		jumpAct = new JumpAction(character, direction, le);
		le.addAction(jumpAct);
		character.addComponent(le);

	}

}
