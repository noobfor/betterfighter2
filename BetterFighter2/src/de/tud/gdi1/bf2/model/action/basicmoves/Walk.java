package de.tud.gdi1.bf2.model.action.basicmoves;

import de.tud.gdi1.bf2.model.entities.characters.Character;

public class Walk extends BasicDirectionalMovement {

	/**
	 * 
	 * @param direction 0 = left, 1 = right, 2 = up , 3 = down
	 * @param c the character which moves
	 * @param speed the speed to move
	 * @param wallCollided 
	 */
	public Walk(int direction, Character c, float speed, boolean setDirection) {
		super(direction, c, speed, setDirection);
	}

	@Override
	void setState() {
		currentCharacter.setWalking();
	}

}
