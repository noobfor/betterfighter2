package de.tud.gdi1.bf2.model.action.crowdcontrol;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;

public class FreezeAction implements Action{
	
	long elapsedTime = 0;
	Character currentChar;
	Event[] componentsToRemove;
	Vector2f currentPosition;
	
	public FreezeAction(Character character, Event[] componentsToRemove, Vector2f currentPosition) {
		this.currentChar = character;
		this.componentsToRemove = componentsToRemove;
		this.currentPosition = currentPosition;
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		// TODO Auto-generated method stub

		currentChar.setPosition(currentPosition);
		
		if (!currentChar.isFreezed()) {
			for(int i = 0; i < componentsToRemove.length; i++) {
				currentChar.removeComponent(componentsToRemove[i]);	
			}
		}
		
		if (elapsedTime > 800) {
			for(int i = 0; i < componentsToRemove.length; i++) {
				currentChar.removeComponent(componentsToRemove[i]);	
			}
			currentChar.setFreezed(false);;
		}

		elapsedTime++;
	}

}