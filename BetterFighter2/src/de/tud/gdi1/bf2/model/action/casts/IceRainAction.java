package de.tud.gdi1.bf2.model.action.casts;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.entities.spells.IceDrop;
import eea.engine.action.Action;
import eea.engine.component.Component;

public class IceRainAction implements Action{

	
	Vector2f startPosition = new Vector2f();
	Vector2f midPoint = new Vector2f();
	Vector2f endPoint = new Vector2f();
	float t = 0;
	int frames = 0;
	
	
	Character usingCharacter;
	Character enemyEntity;
	boolean following = false;
	boolean direction;
	
	public IceRainAction(Vector2f startPosition, Character enemyEntity, boolean direction) {
		this.startPosition = startPosition;
		this.enemyEntity = enemyEntity;
		this.direction = direction;
		if(direction) {
		midPoint.x = startPosition.getX() + 50;
		endPoint.x = startPosition.getX() + 70;
		
		}
		else {
			midPoint.x = startPosition.getX() - 50;
			endPoint.x = startPosition.getX() - 70;
		}

		midPoint.y = startPosition.getY() - 250;
		endPoint.y = startPosition.getY() - 180;
	}
	
	private float quadraticBezierCurve(float start, float mid, float end, float t) {
		return ((1-t) * ((1-t) * start + t * mid) + t * ((1-t) * mid + t * end));
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		IceDrop iceDrop = (IceDrop) event.getOwnerEntity();
		Vector2f currentPosition = iceDrop.getPosition();	
		
		
		if(!direction && !following && currentPosition.x <= endPoint.x && currentPosition.y >= endPoint.y) {
			iceDrop.getCharacter().setIceRain(false);
			following = true;
			startPosition = endPoint;
			t = 0;
		}
		
		else if (direction && !following && currentPosition.x >= endPoint.x && currentPosition.y >= endPoint.y) {
			iceDrop.getCharacter().setIceRain(false);
			following = true;
			startPosition = endPoint;
			t = 0;
		}
		
		
		if(!following) {
			float newX = quadraticBezierCurve(startPosition.x, midPoint.x, endPoint.x, t);
			float newY = quadraticBezierCurve(startPosition.y, midPoint.y, endPoint.y, t);
			iceDrop.setPosition(new Vector2f(newX, newY));
			//System.out.println(t);
			t += 0.01;
		}
		
		if(following) {
			endPoint = enemyEntity.getPosition();
			
			float newX = quadraticBezierCurve(startPosition.x, midPoint.x, endPoint.x, t);
			float newY = quadraticBezierCurve(startPosition.y, midPoint.y, endPoint.y, t);
			Vector2f newPosition = new Vector2f(newX, newY);
			iceDrop.setPosition(newPosition);
			
			t += 0.01;
			
		}
		
	}

}
