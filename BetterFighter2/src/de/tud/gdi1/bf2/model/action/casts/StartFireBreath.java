package de.tud.gdi1.bf2.model.action.casts;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.entities.spells.FireBreath;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.StateBasedEntityManager;

public class StartFireBreath implements Action {

	private int manaCost = 5;
	private StateBasedEntityManager entityManager;

	Vector2f fireBreathStartingPosition = new Vector2f(60, 0);

	public StartFireBreath() {
		entityManager = StateBasedEntityManager.getInstance();
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		Character character = (Character) event.getOwnerEntity();

		if (!character.isShielding() && !character.isFireBreathing() || character.isInAnyCrowdControl()) {
			return;
		}

		boolean direction = character.getDirection();
		character.setFireBreathing(true);

//		System.out.println(direction);
		FireBreath fireBreath;
		if (direction)
			fireBreath = new FireBreath(character.getID(), character,
					character.getPosition().copy().add(fireBreathStartingPosition));
		else
			fireBreath = new FireBreath(character.getID(), character,
					character.getPosition().copy().sub(fireBreathStartingPosition));

		if (character.getMana() >= manaCost) {
			character.decreaseMana(manaCost);

			entityManager.addEntity(sb.getCurrentStateID(), fireBreath);

			// character.setFireBreathing(false);

		}

		else {
			character.setFireBreathing(false);
		}

	}

}
