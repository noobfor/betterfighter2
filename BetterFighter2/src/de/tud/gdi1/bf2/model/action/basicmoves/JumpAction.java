package de.tud.gdi1.bf2.model.action.basicmoves;

import java.security.InvalidParameterException;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.entities.map.Wall;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.basicevents.LoopEvent;

public class JumpAction implements Action {
	// the direction of the jump, 9 different in total, from -1 to 7
	int direction;

	// the height and lenght of the jump
	float jumpHeight = 80f;
	// time needed to complete the jump in miliseconds
	float jumpTime = 1000;

	float jumpLenght = 2 * jumpHeight;

	float constant = jumpLenght / jumpTime;

	float verticalJumpDistance = 50;

	// starting position before the jump starts
	Vector2f startingPosition;

	long animationCastingTime = 200;

	// velocity of the jump

	// boolean to indicate if the jump has been executed
	private boolean done = false;
	private Character currentChar;

	long elapsedTime = 0;
	LoopEvent toBeRemoved;

	public JumpAction(Character character, int direction2, LoopEvent toRemove) {
		currentChar = character;
		this.direction = direction2;
		toBeRemoved = toRemove;
	}

	public void setElapsedTime(long t) {
		this.elapsedTime = t;
	}

	/**
	 * Calculates the y-coordinate of a jump for the given x.
	 * 
	 * @param x
	 *            - represents the difference to the starting position x-coordinate
	 * @return float for the y-coordinate of the given x
	 */
	public float jumpFunction(float x, float verticalOffset) {

		float result = (float) (x * (4 * jumpHeight * (jumpTime - x) + jumpTime * verticalOffset)
				/ Math.pow(jumpTime, 2));
		// System.out.println("x = " + x + ", f(x) = " + result);
		return result;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		// System.out.println(direction);

		Wall north = (Wall) StateBasedEntityManager.getInstance().getEntity(sb.getCurrentStateID(), "wallNorth");
		Wall south = (Wall) StateBasedEntityManager.getInstance().getEntity(sb.getCurrentStateID(), "wallSouth");
		Wall east = (Wall) StateBasedEntityManager.getInstance().getEntity(sb.getCurrentStateID(), "wallEast");
		Wall west = (Wall) StateBasedEntityManager.getInstance().getEntity(sb.getCurrentStateID(), "wallWest");

		float westThreshold = west.getSize().getX() + currentChar.getSize().getX() / 2;
		float eastThreshold = gc.getWidth() - (east.getSize().getX() + currentChar.getSize().getX() / 2);
		float northThreshold = north.getPosition().getY() + north.getSize().getY() / 2
				+ currentChar.getSize().getY() / 2;
		float southThreshold = gc.getHeight() - (south.getSize().getY() + currentChar.getSize().getY() / 2);

		// System.out.println(elapsedTime);
		Vector2f nextPos;
		if (elapsedTime == 0) {

			startingPosition = currentChar.getPosition().copy();

		} else if (done) {

			currentChar.setJumping(false);
			event.getOwnerEntity().removeComponent(toBeRemoved);

		} else if (elapsedTime >= animationCastingTime && (elapsedTime <= (animationCastingTime + jumpTime) || !done)) {

			long tmp = elapsedTime - animationCastingTime;
			float nextYPositionSame = jumpFunction(tmp, 0);
			float nextYPositionUp = jumpFunction(tmp, verticalJumpDistance);
			float nextYPositionDown = jumpFunction(tmp, -verticalJumpDistance);

			if (nextYPositionSame < 0 && direction < 2) {
				done = true;
			} else if (nextYPositionUp < 0 && (direction == 2 || direction == 4 || direction == 6)
					&& elapsedTime >= jumpTime + animationCastingTime) {
				done = true;
			} else if (nextYPositionDown < -verticalJumpDistance
					&& (direction == 3 || direction == 5 || direction == 7)) {
				done = true;
			}
			switch (direction) {
			case -1:
				currentChar.setPosition(startingPosition.copy().add(new Vector2f(0, -Math.max(nextYPositionSame, 0))));
				break;
			case 0: // left
				nextPos = startingPosition.copy()
						.add(new Vector2f(Math.max(-tmp * constant, -jumpLenght), -Math.max(nextYPositionSame, 0)));
				currentChar.setPosition(new Vector2f(Math.max(westThreshold + 1, nextPos.getX()), nextPos.getY()));
				break;
			case 1: // right
				nextPos = startingPosition.copy()
						.add(new Vector2f(Math.min(tmp * constant, jumpLenght), -Math.max(nextYPositionSame, 0)));
				currentChar.setPosition(new Vector2f(Math.min(eastThreshold - 1, nextPos.getX()), nextPos.getY()));

				break;
			case 2: // leftup
				if (startingPosition.getY() - verticalJumpDistance > northThreshold) { // jump will not land above
																						// threshold
					if (tmp < jumpTime / 2) { // jumping up
						nextPos = startingPosition.copy().add(
								new Vector2f(Math.max(-tmp * constant, -jumpLenght), -Math.max(nextYPositionUp, 0)));
					} else { // falling
						nextPos = startingPosition.copy().add(new Vector2f(Math.max(-tmp * constant, -jumpLenght),
								-Math.max(nextYPositionUp, verticalJumpDistance)));
					}
					currentChar.setPosition(new Vector2f(Math.max(westThreshold + 1, nextPos.getX()), nextPos.getY()));
				} else { // jump lands above threshold

					nextPos = startingPosition.copy()
							.add(new Vector2f(Math.max(-tmp * constant, -jumpLenght), -Math.max(nextYPositionUp, 0)));
					if (tmp < jumpTime / 2) { // jumping up
						// the y position can be max jumpHeight*1.5f while jumping up
						currentChar.setPosition(new Vector2f(Math.max(westThreshold + 1, nextPos.getX()),
								Math.max(northThreshold + 1 - jumpHeight * 1.5f, nextPos.getY())));
					} else { // falling
						// the y position is the minimum (smaller means rendered higher) between the
						// next calculed position and the norththreshold +1
						currentChar.setPosition(new Vector2f(Math.max(westThreshold + 1, nextPos.getX()),
								Math.min(northThreshold + 1, nextPos.getY())));
					}
				}

				break;
			case 3: // leftdown
				nextPos = startingPosition.copy().add(new Vector2f(Math.max(-tmp * constant, -jumpLenght),
						-Math.max(nextYPositionDown, -verticalJumpDistance)));
				currentChar.setPosition(new Vector2f(Math.max(westThreshold + 1, nextPos.getX()),
						Math.min(southThreshold - 1, nextPos.getY())));
				break;
			case 4: // rightup
				if (startingPosition.getY() - 100 > northThreshold) { // jump will not land above threshold
					if (tmp < jumpTime / 2) { // jumping up
						nextPos = startingPosition.copy()
								.add(new Vector2f(Math.min(tmp * constant, jumpLenght), -Math.max(nextYPositionUp, 0)));
					} else { // falling

						nextPos = startingPosition.copy().add(new Vector2f(Math.min(tmp * constant, jumpLenght),
								-Math.max(nextYPositionUp, verticalJumpDistance)));
					}
					currentChar.setPosition(new Vector2f(Math.min(eastThreshold - 1, nextPos.getX()), nextPos.getY()));
				} else { // jump lands above threshold

					nextPos = startingPosition.copy()
							.add(new Vector2f(Math.min(tmp * constant, jumpLenght), -Math.max(nextYPositionUp, 0)));
					if (tmp < jumpTime / 2) { // jumping up
						// the y position can be max jumpHeight*1.5f while jumping up
						currentChar.setPosition(new Vector2f(Math.min(eastThreshold - 1, nextPos.getX()),
								Math.max(northThreshold + 1 - jumpHeight * 1.5f, nextPos.getY())));
					} else { // falling
						// the y position is the minimum (smaller means rendered higher) between the
						// next calculed position and the norththreshold +1
						currentChar.setPosition(new Vector2f(Math.min(eastThreshold - 1, nextPos.getX()),
								Math.min(northThreshold + 1, nextPos.getY())));
					}
				}

				break;
			case 5: // rightdown
				nextPos = startingPosition.copy()
						.add(new Vector2f(Math.min(tmp * constant, jumpLenght), -Math.max(nextYPositionDown, -verticalJumpDistance)));
				currentChar.setPosition(new Vector2f(Math.min(eastThreshold - 1, nextPos.getX()),
						Math.min(southThreshold - 1, nextPos.getY())));

				break;
			case 6: // up
				if (startingPosition.getY() - 100 > northThreshold) { // jump will not land above threshold
					if (tmp < jumpTime / 2) { // jumping up
						nextPos = startingPosition.copy().add(new Vector2f(0, -Math.max(nextYPositionUp, 0)));
					} else { // falling

						nextPos = startingPosition.copy()
								.add(new Vector2f(0, -Math.max(nextYPositionUp, verticalJumpDistance)));
					}
					currentChar.setPosition(nextPos);
				} else { // jump lands above threshold

					nextPos = startingPosition.copy().add(new Vector2f(0, -Math.max(nextYPositionUp, 0)));
					if (tmp < jumpTime / 2) { // jumping up
						// the y position can be max jumpHeight*1.5f while jumping up
						currentChar.setPosition(new Vector2f(nextPos.getX(),
								Math.max(northThreshold + 1 - jumpHeight * 1.5f, nextPos.getY())));
					} else { // falling
						// the y position is the minimum (smaller means rendered higher) between the
						// next calculed position and the norththreshold +1
						currentChar.setPosition(
								new Vector2f(nextPos.getX(), Math.min(northThreshold + 1, nextPos.getY())));
					}
				}

				break;
			case 7: // down
				nextPos = startingPosition.copy()
						.add(new Vector2f(0, -Math.max(nextYPositionDown, -verticalJumpDistance)));
				currentChar.setPosition(new Vector2f(nextPos.getX(), Math.min(southThreshold - 1, nextPos.getY())));

				break;
			default:
				throw new InvalidParameterException("This jumping direction is not supported.");
			}
		}
		elapsedTime = elapsedTime + delta;

	}
}
