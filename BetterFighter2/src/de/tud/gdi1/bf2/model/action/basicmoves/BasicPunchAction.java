package de.tud.gdi1.bf2.model.action.basicmoves;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.event.Event;

public class BasicPunchAction implements Action{

	long elapsedTime = 0;
	Vector2f currentPosition;
	Event[] componentsToremove;
	
	
	// can change to using system time if necessary!!!!!!!!
	public BasicPunchAction(Event[] componentsToremove, Vector2f currentPosition) {
		this.currentPosition = currentPosition;
		this.componentsToremove = componentsToremove;
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		Character currentChar = (Character) event.getOwnerEntity();
		
		if(currentChar.getDirection())
		currentChar.setPosition(new Vector2f(currentPosition.x + 5, currentPosition.y));
		else
			currentChar.setPosition(new Vector2f(currentPosition.x - 5, currentPosition.y));
		
		// until hitbox is finished
		if (elapsedTime == 25) {
			currentChar.removeComponent(componentsToremove[1]);
			currentChar.removeComponent(componentsToremove[2]);
		}
		
		
		// ending lag of the move, until 30 frames he can move again
		if (elapsedTime > 30) {
			
			currentChar.setpunching(false);
			currentChar.removeComponent(componentsToremove[0]);
		}
		
		elapsedTime++;
	}

}
