package de.tud.gdi1.bf2.model.action.casts;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.bf2.model.entities.characters.Character;
import de.tud.gdi1.bf2.model.entities.spells.Iceball;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.StateBasedEntityManager;

public class StartIceball implements Action{

	private StateBasedEntityManager sbm;
	Vector2f fireBallStartingPosition = new Vector2f(40, 0);
	boolean dir;
	
	public StartIceball(boolean dir) {
		this.dir = dir;
		this.sbm = StateBasedEntityManager.getInstance();
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {

		Character character = (Character) event.getOwnerEntity();
		character.setDirection(dir);
		
		if (!character.isShielding() || character.isInAnyCrowdControl()) {
			return;
		}
		
		character.setThrowing(true);
		
		Iceball p;
		
		if (dir) {
			p = new Iceball(character.getID(), character, character.getPosition().copy().add(fireBallStartingPosition), dir);
		} else {
			p = new Iceball(character.getID(), character, character.getPosition().copy().sub(fireBallStartingPosition), dir);
		}
		
		if (character.getMana() >= p.getManaCost()) {
			
			character.decreaseMana(p.getManaCost());
			
			sbm.addEntity(sb.getCurrentStateID(), p);
			character.setThrowing(false);
			
		} 
		
		else {
			character.setThrowing(false);
		}
		
	}
}
