package de.tud.gdi1.bf2.utils;

import java.util.Comparator;

import eea.engine.entity.Entity;

public class EntityPositionComparator implements Comparator<Entity>{

	@Override
	public int compare(Entity firstEntity, Entity secondEntity) {
		if(firstEntity.getPosition().y < secondEntity.getPosition().y)
			return -1;
		if (firstEntity.getPosition().y > secondEntity.getPosition().y)
			return 1;
		
		return 0;
	}

}
